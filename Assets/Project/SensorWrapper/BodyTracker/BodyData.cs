﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ryabomar {
    
    // contains id, tracked state and joints data (3d position & orientation). 
    public class BodyData {

        public enum HandState {
            UNKNOWN, OPEN, CLOSED, NOT_TRACKER, LASSO
        }

        public ulong id { get; }
        public bool tracked { get; }
        public Joint[] joints { get; } 

        public HandState leftHandState  { get; } = HandState.UNKNOWN;
        public HandState rightHandState { get; } = HandState.UNKNOWN;


        // constructor without joint data
        public BodyData(ulong id, bool isTracked = false)
            : this(id, isTracked, new Joint[Enum.GetNames(typeof(Joint.Type)).Length - 1]) // exclude Joint.Type.NONE
        {
            for(int i = 0; i < joints.Length; i++) {
                joints[i].orientation = Quaternion.identity;
            }
        }


        // constructor with joints data
        public BodyData(ulong id, bool isTracked, Joint[] joints) { 
            this.id = id; 
            tracked = isTracked;
            this.joints = joints;

            if(joints.Length < Enum.GetNames(typeof(Joint.Type)).Length - 1) {
                throw new ArgumentException("wrong number of joints");
            }
        }


        public BodyData(BodyData other){
            id = other.id;
            tracked = other.tracked;

            joints = new Joint[Enum.GetNames(typeof(Joint.Type)).Length - 1];

            for(int i = 0; i < joints.Length; i++) {
                joints[i] = other.joints[i];
            }


            leftHandState  = other.leftHandState;  
            rightHandState = other.rightHandState; 
        }

    }
}
