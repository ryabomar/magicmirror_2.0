﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar {

    //static class JointEnumExtensions {
    //    // using azure's parent-child relationship
    //    // idx == joint; _PARENTS[idx] == parent
    //    private static JointEnum[] _PARENTS = {
    //            /* PELVIS 		  -> */  JointEnum.NONE,
    //            /* SPINE_NAVAL 	  -> */  JointEnum.PELVIS,
    //            /* SPINE_CHEST 	  -> */  JointEnum.SPINE_NAVAL,
    //            /* NECK 		  -> */  JointEnum.SPINE_CHEST,
    //            /* CLAVICLE_LEFT  -> */  JointEnum.SPINE_CHEST,
    //            /* SHOULDER_LEFT  -> */  JointEnum.CLAVICLE_LEFT,
    //            /* ELBOW_LEFT 	  -> */  JointEnum.SHOULDER_LEFT,
    //            /* WRIST_LEFT 	  -> */  JointEnum.ELBOW_LEFT,
    //            /* HAND_LEFT 	  -> */  JointEnum.WRIST_LEFT,
    //            /* HANDTIP_LEFT   -> */  JointEnum.HAND_LEFT,
    //            /* THUMB_LEFT 	  -> */  JointEnum.WRIST_LEFT,
    //            /* CLAVICLE_RIGHT -> */  JointEnum.SPINE_CHEST,
    //            /* SHOULDER_RIGHT -> */  JointEnum.CLAVICLE_RIGHT,
    //            /* ELBOW_RIGHT 	  -> */  JointEnum.SHOULDER_RIGHT,
    //            /* WRIST_RIGHT 	  -> */  JointEnum.ELBOW_RIGHT,
    //            /* HAND_RIGHT 	  -> */  JointEnum.WRIST_RIGHT,
    //            /* HANDTIP_RIGHT  -> */  JointEnum.HAND_RIGHT,
    //            /* THUMB_RIGHT 	  -> */  JointEnum.WRIST_RIGHT,
    //            /* HIP_LEFT 	  -> */  JointEnum.PELVIS,
    //            /* KNEE_LEFT 	  -> */  JointEnum.HIP_LEFT,
    //            /* ANKLE_LEFT 	  -> */  JointEnum.KNEE_LEFT,
    //            /* FOOT_LEFT 	  -> */  JointEnum.ANKLE_LEFT,
    //            /* HIP_RIGHT 	  -> */  JointEnum.PELVIS,
    //            /* KNEE_RIGHT 	  -> */  JointEnum.HIP_RIGHT,
    //            /* ANKLE_RIGHT 	  -> */  JointEnum.KNEE_RIGHT,
    //            /* FOOT_RIGHT 	  -> */  JointEnum.ANKLE_RIGHT,
    //            /* HEAD           -> */  JointEnum.NECK,
    //            /* NOSE 		  -> */  JointEnum.HEAD,
    //            /* EYE_LEFT 	  -> */  JointEnum.HEAD,
    //            /* EAR_LEFT 	  -> */  JointEnum.HEAD,
    //            /* EYE_RIGHT 	  -> */  JointEnum.HEAD,
    //            /* EAR_RIGHT 	  -> */  JointEnum.HEAD,
    //        };


    //    public static JointEnum GetParent(this JointEnum joint) {
    //        if(joint == JointEnum.NONE) { 
    //            return JointEnum.NONE;
    //            //throw new ArgumentOutOfRangeException("attempt to get parent of JointEnum.NONE");
    //        }

    //        return _PARENTS[(int)joint];
    //    }

    //    public static bool HasParent(this JointEnum joint) {
    //        return joint.GetParent() != JointEnum.NONE;
    //    }


    //    public static bool HasChildren(this JointEnum joint) {
    //        //TODO:
    //        throw new NotImplementedException("JointEnum.HasChildren()");
    //    }
    //}

    //// using azure's joint names
    //public enum JointEnum {
    //    NONE = -1,
    //    PELVIS = 0,
    //    SPINE_NAVAL,
    //    SPINE_CHEST,
    //    NECK,
    //    CLAVICLE_LEFT,
    //    SHOULDER_LEFT,
    //    ELBOW_LEFT,
    //    WRIST_LEFT,
    //    HAND_LEFT,
    //    HANDTIP_LEFT,
    //    THUMB_LEFT,
    //    CLAVICLE_RIGHT,
    //    SHOULDER_RIGHT,
    //    ELBOW_RIGHT,
    //    WRIST_RIGHT,
    //    HAND_RIGHT,
    //    HANDTIP_RIGHT,
    //    THUMB_RIGHT,
    //    HIP_LEFT,
    //    KNEE_LEFT,
    //    ANKLE_LEFT,
    //    FOOT_LEFT,
    //    HIP_RIGHT,
    //    KNEE_RIGHT,
    //    ANKLE_RIGHT,
    //    FOOT_RIGHT,
    //    HEAD,
    //    NOSE,
    //    EYE_LEFT,
    //    EAR_LEFT,
    //    EYE_RIGHT,
    //    EAR_RIGHT,
    //}


}//namespace ryabomar 