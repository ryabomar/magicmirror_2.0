﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ryabomar {

    [System.Serializable] public class BodyTrackingEvent : UnityEvent<BodyData> {}

    /// <summary>
    /// Interface for body tracking
    /// </summary>
    public interface IBodyTracker {

        BodyTrackingEvent BodyAppeared_Event    { get; set; }
        BodyTrackingEvent BodyDisappeared_Event { get; set; }
        BodyTrackingEvent BodyUpdated_Event     { get; set; }

        List<BodyData> GetLastFrameBodies();
    }
} //! namespace ryabomar
