﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ryabomar {

    [Serializable] 
    public struct Joint { 

        public Vector3       position;
        public Quaternion    orientation;
        public TrackingState trackingState;
        public Type          type;
        public bool          isSupported;

        [Serializable] 
        public enum Type {
            NONE        = -1,
            PELVIS      = 0,
            SPINE_NAVAL,
            SPINE_CHEST,
            NECK,
            CLAVICLE_LEFT,
            SHOULDER_LEFT,
            ELBOW_LEFT,
            WRIST_LEFT,
            HAND_LEFT,
            HANDTIP_LEFT,
            THUMB_LEFT,
            CLAVICLE_RIGHT,
            SHOULDER_RIGHT,
            ELBOW_RIGHT,
            WRIST_RIGHT,
            HAND_RIGHT,
            HANDTIP_RIGHT,
            THUMB_RIGHT,
            HIP_LEFT,
            KNEE_LEFT,
            ANKLE_LEFT,
            FOOT_LEFT,
            HIP_RIGHT,
            KNEE_RIGHT,
            ANKLE_RIGHT,
            FOOT_RIGHT,
            HEAD,
            NOSE,
            EYE_LEFT,
            EAR_LEFT,
            EYE_RIGHT,
            EAR_RIGHT,
        } //! enum Type

        public enum TrackingState {
            TRACKED, NOT_TRACKED, INFERRED
        }

    } //! class Joint


    public static class JointTypeExtensions {
        // using azure's parent-child relationship
        // idx == joint; _PARENTS[idx] == parent
        private static Joint.Type[] _PARENTS = {
                /* PELVIS 		  -> */  Joint.Type.NONE,
                /* SPINE_NAVAL 	  -> */  Joint.Type.PELVIS,
                /* SPINE_CHEST 	  -> */  Joint.Type.SPINE_NAVAL,
                /* NECK 		  -> */  Joint.Type.SPINE_CHEST,
                /* CLAVICLE_LEFT  -> */  Joint.Type.SPINE_CHEST,
                /* SHOULDER_LEFT  -> */  Joint.Type.CLAVICLE_LEFT,
                /* ELBOW_LEFT 	  -> */  Joint.Type.SHOULDER_LEFT,
                /* WRIST_LEFT 	  -> */  Joint.Type.ELBOW_LEFT,
                /* HAND_LEFT 	  -> */  Joint.Type.WRIST_LEFT,
                /* HANDTIP_LEFT   -> */  Joint.Type.HAND_LEFT,
                /* THUMB_LEFT 	  -> */  Joint.Type.WRIST_LEFT,
                /* CLAVICLE_RIGHT -> */  Joint.Type.SPINE_CHEST,
                /* SHOULDER_RIGHT -> */  Joint.Type.CLAVICLE_RIGHT,
                /* ELBOW_RIGHT 	  -> */  Joint.Type.SHOULDER_RIGHT,
                /* WRIST_RIGHT 	  -> */  Joint.Type.ELBOW_RIGHT,
                /* HAND_RIGHT 	  -> */  Joint.Type.WRIST_RIGHT,
                /* HANDTIP_RIGHT  -> */  Joint.Type.HAND_RIGHT,
                /* THUMB_RIGHT 	  -> */  Joint.Type.WRIST_RIGHT,
                /* HIP_LEFT 	  -> */  Joint.Type.PELVIS,
                /* KNEE_LEFT 	  -> */  Joint.Type.HIP_LEFT,
                /* ANKLE_LEFT 	  -> */  Joint.Type.KNEE_LEFT,
                /* FOOT_LEFT 	  -> */  Joint.Type.ANKLE_LEFT,
                /* HIP_RIGHT 	  -> */  Joint.Type.PELVIS,
                /* KNEE_RIGHT 	  -> */  Joint.Type.HIP_RIGHT,
                /* ANKLE_RIGHT 	  -> */  Joint.Type.KNEE_RIGHT,
                /* FOOT_RIGHT 	  -> */  Joint.Type.ANKLE_RIGHT,
                /* HEAD           -> */  Joint.Type.NECK,
                /* NOSE 		  -> */  Joint.Type.HEAD,
                /* EYE_LEFT 	  -> */  Joint.Type.HEAD,
                /* EAR_LEFT 	  -> */  Joint.Type.HEAD,
                /* EYE_RIGHT 	  -> */  Joint.Type.HEAD,
                /* EAR_RIGHT 	  -> */  Joint.Type.HEAD,
            };


        public static Joint.Type GetParent(this Joint.Type joint) {
            if (joint == Joint.Type.NONE) {
                return Joint.Type.NONE;
            }

            return _PARENTS[(int)joint];
        }

        public static bool HasParent(this Joint.Type joint) {
            return joint.GetParent() != Joint.Type.NONE;
        }


        //public static bool HasChildren(this Joint.Type joint) {
        //    //TODO:
        //    throw new NotImplementedException("JointEnum.HasChildren()");
        //}
    }

} //! namespace ryabomar
