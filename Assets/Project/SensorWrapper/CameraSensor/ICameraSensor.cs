﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar {

    /// <summary>
    /// Interface for camera sensor
    /// </summary>
    public interface ICameraSensor {
        Vector2Int colorResolution      { get; }
        Vector2Int depthResolution      { get; }


        Texture color_Texture           { get; }
        Texture bodyIndex_Texture       { get; }
        Texture pointsPos_Texture       { get; }
        Texture pointsColors_Texture   { get; }      
        Texture depthData_Texture       { get; }
    }
}
