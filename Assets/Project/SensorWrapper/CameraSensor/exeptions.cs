﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar {

    /// <summary>
    /// 
    /// </summary>
    public class SensorNotFound : Exception {
        public SensorNotFound() : base() { }
        public SensorNotFound(string msg) : base(msg) { }
    }

    public class SensorInitializationFailed : Exception {
        public SensorInitializationFailed(string msg) : base(msg) { }
    }


} //! namespace ryabomar
