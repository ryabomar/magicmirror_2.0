﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ryabomar {

    /// <summary>
    /// handle for OffAxisCamera
    /// </summary>
    [CreateAssetMenu(fileName = "OffAxisCamerasHandle", menuName = "MagicMirror/OffAxisCamerasHandle")]
    public class OffAxisCamerasHandle : ScriptableObject {
        /// <summary>
        /// see OffAxisCamera
        /// </summary>
        /// <param name="enable"></param>
        public void SetEnableOffAxis(bool enable) {
            foreach(OffAxisCamera offAxisCamera in FindObjectsOfType<OffAxisCamera>()) {
                offAxisCamera.SetEnableOffAxis(enable);
            }

            PlayerPrefs.SetInt("MagicMirror_isEnabled", Convert.ToInt32(enable));
        }

    } //! OffAxisCamerasHandle

} //!namespace ryabomar