﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ryabomar {

    /// <summary>
    /// Makes camera use Off-Axis projection
    /// </summary>
    [RequireComponent(typeof(Camera))]
    public class OffAxisCamera : MonoBehaviour {
        [Header("References")]
        [SerializeField] UserSwitcherHandle userSwitcherHandle;
        [SerializeField] OffAxisProjectionFrameHandle offAxisProjectionFrameHandle;

        bool _isOffAxis = false;


        Vector3 _initPosition;
        //Vector3 _initScale;
        Quaternion _initRotation;

        Matrix4x4 _initProjection;
        Matrix4x4 _initView;
        
        Camera _camera;

        void Awake() {
            _camera = GetComponent<Camera>();

            if(offAxisProjectionFrameHandle == null) {
                throw new System.Exception("no offAxisProjectionFrameAssigned");
            }
        }


        void Start() {
            _SaveInit();
            userSwitcherHandle.activeBodySwitched.AddListener(OnActiveBodySwitched);
        }


        /// <summary> delegate </summary>
        /// <param name="bodyData"></param>
        public void OnActiveBodyUpdated(BodyData bodyData) {
            if(_isOffAxis) { 
                _Move(bodyData);
                _CalcOffAxisMatricies();
            } 
        }


        /// <summary> delegate </summary>
        /// <param name="bodyData"></param>
        public void OnActiveBodySwitched(BodyData bodyData) {
            OnActiveBodyUpdated(bodyData);
        }


        /// <summary> swtich offaxis/onaxis </summary>
        /// <param name="value"></param>
        public void SetEnableOffAxis(bool enable) {
            if(enable) {
                userSwitcherHandle.activeBodyUpdated.AddListener(OnActiveBodyUpdated);
            } else {
                userSwitcherHandle.activeBodyUpdated.RemoveListener(OnActiveBodyUpdated);
                _RestoreInit();
            }

            _isOffAxis = enable;
        }


        void _RestoreInit(){
            transform.position = _initPosition;
            transform.rotation = _initRotation;
            _camera.projectionMatrix    = _initProjection;
            _camera.worldToCameraMatrix = _initView;
        }


        void _SaveInit() {
            _initPosition = transform.position;
            _initRotation = transform.rotation;
            _initProjection = _camera.projectionMatrix;
            _initView       = _camera.worldToCameraMatrix;
        }


        void _Move(BodyData bodyData) {
            Vector3 headPosition = bodyData.joints[(int)Joint.Type.HEAD].position;

            // head position in local coordinates of projection frame
            Vector3 headLocalPositionInProjectionFrame = offAxisProjectionFrameHandle.transform.InverseTransformPoint(headPosition);
            headLocalPositionInProjectionFrame.z = -headLocalPositionInProjectionFrame.z; // "mirroring" it by inversing Z axis sign

            Vector3 cameraPosition = offAxisProjectionFrameHandle.transform.TransformPoint(headLocalPositionInProjectionFrame);
            transform.position = cameraPosition;
        }


        void _CalcOffAxisMatricies() {
            Vector3 eyePosition = transform.position;

            Vector3 vA = offAxisProjectionFrameHandle.pA - eyePosition;
            Vector3 vB = offAxisProjectionFrameHandle.pB - eyePosition;
            Vector3 vC = offAxisProjectionFrameHandle.pC - eyePosition;
            
            //distance from eye to projection screen plane
            float dist = -Vector3.Dot(vA, offAxisProjectionFrameHandle.vN);

            float near, far, l, r, b, t;
            near = 0.001f;  //Camera.main.nearClipPlane;
            far  = 100.0f; //Camera.main.farClipPlane;

            l = Vector3.Dot(offAxisProjectionFrameHandle.vR, vA) * (near / dist);
            r = Vector3.Dot(offAxisProjectionFrameHandle.vR, vB) * (near / dist);
            b = Vector3.Dot(offAxisProjectionFrameHandle.vU, vA) * (near / dist);
            t = Vector3.Dot(offAxisProjectionFrameHandle.vU, vC) * (near / dist);

            Matrix4x4 projection = Matrix4x4.Frustum(l, r, b, t, near, far);

            Matrix4x4 transition = Matrix4x4.Translate(-eyePosition);
            Matrix4x4 rotation   = Matrix4x4.Rotate(Quaternion.Inverse(transform.rotation));

            _camera.worldToCameraMatrix = offAxisProjectionFrameHandle.matrix * rotation * transition;
            _camera.projectionMatrix    = projection;
        }



    } //! class OffAxisCamera 

} //! namespace ryabomar {