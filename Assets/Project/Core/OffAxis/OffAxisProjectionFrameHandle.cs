﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ryabomar {
    /// <summary>
    /// handle for OffAxisProjectionFrame
    /// </summary>
    [CreateAssetMenu(fileName = "OffAxisProjectionFrameHandle", menuName = "MagicMirror/OffAxisProjectionFrameHandle")]
    public class OffAxisProjectionFrameHandle : ScriptableObject {
        /// <summary>to draw lines between corners or not</summary>
        public bool isVisible = true;

        /// <summary>border color</summary>
        //public Color color = Color.white;

        // global positions of the frame corners
        /// <summary>corner A global position</summary>
        public Vector3 pA => _frame == null ? Vector3.zero : _frame.pA;

        /// <summary>corner B global position</summary>
        public Vector3 pB => _frame == null ? Vector3.zero : _frame.pB;

        /// <summary>corner C global position</summary>
        public Vector3 pC => _frame == null ? Vector3.zero : _frame.pC;

        /// <summary>corner D global position</summary>
        public Vector3 pD => _frame == null ? Vector3.zero : _frame.pD;

        // frame basis
        /// <summary>Basis vector U</summary>
        public Vector3 vU => _frame == null ? Vector3.zero : _frame.vU;

        /// <summary>Basis vector R</summary>
        public Vector3 vR => _frame == null ? Vector3.zero : _frame.vR;

        /// <summary>Basis vector N</summary>
        public Vector3 vN => _frame == null ? Vector3.zero : _frame.vN;

        /// <summary>Calculated matrix</summary>
        public Matrix4x4 matrix => _frame == null ? Matrix4x4.identity : _frame.matrix;

        /// transform of projection frame
        public Transform transform => _frame == null ? null : _frame.transform;


        private OffAxisProjectionFrame _frame { 
            get { // will search until found and then will use cached value
                if(_frameCached == null) {
                    _frameCached = FindObjectOfType<OffAxisProjectionFrame>();
                }
                return _frameCached;
            }
        }

        private OffAxisProjectionFrame _frameCached = null;
    }
}
