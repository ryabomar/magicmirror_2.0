﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar {

    /// <summary>
    /// handle for point cloud controller
    /// </summary>
    [CreateAssetMenu(fileName = "PointCloudHandle", menuName = "MagicMirror/PointCloudHandle")]
    public class PointCloudHandle : ScriptableObject {

        PointCloudController _pointCloud => FindObjectOfType<PointCloudController>();

        public void SetHighlightBodies(bool value){
            _pointCloud?.SetHighlightBodies(value);
            PlayerPrefs.SetInt("PointCloud_highlightBodies", Convert.ToInt32(value));
        }


        public void SetCutBackground(bool value){
            _pointCloud?.SetCutBackground(value);
            PlayerPrefs.SetInt("PointCloud_cutBackground", Convert.ToInt32(value));
        }


        public void SetPointSize(float value) {
            _pointCloud?.SetPointSize(value);

            PlayerPrefs.SetFloat("PointCloud_pointSize", value);
        }
    }

} //! namespace ryabomar