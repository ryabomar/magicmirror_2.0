﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

namespace ryabomar { 

    /// <summary>
    /// 3D visualization of sensor camera data
    /// </summary>
    [RequireComponent(typeof(VisualEffect))]
    public class PointCloudController : MonoBehaviour {

        [SerializeField] SensorSwitcherHandle sensorSwitcherHandle;
        [SerializeField] UserSettingsManager settingsManager;

        VisualEffect _vfxGraph;

        void Awake() {
            _vfxGraph = GetComponent<VisualEffect>();
        }


        void Start() { 
            if(sensorSwitcherHandle.activeSensor != null) {
                _OnSensorChanged(sensorSwitcherHandle.activeSensor);
            }

            sensorSwitcherHandle.sensorChanged.AddListener(_OnSensorChanged);

            if(settingsManager.HasProperty(BoolSettingsProperty.POINTCLOUD__CUT_BACKGROUND)){
                SetCutBackground(settingsManager.GetPropertyValue(BoolSettingsProperty.POINTCLOUD__CUT_BACKGROUND));
            }

            
            if(settingsManager.HasProperty(BoolSettingsProperty.POINTCLOUD__HIGHLIGHT_BODIES)){
                SetHighlightBodies(settingsManager.GetPropertyValue(BoolSettingsProperty.POINTCLOUD__HIGHLIGHT_BODIES));
            }


            
            if(settingsManager.HasProperty(FloatSettingsProperty.POINTCLOUD__POINT_SIZE)){
                SetPointSize(settingsManager.GetPropertyValue(FloatSettingsProperty.POINTCLOUD__POINT_SIZE));
            }
        }



        void _OnSensorChanged(SensorSwitcher.SensorEntry sensor) {
            var cameraSensor = sensor.cameraSensor;

            if(cameraSensor != null) {
                _vfxGraph.SetUInt("nPointsHorizontal", (uint)cameraSensor.depthResolution.x);
                _vfxGraph.SetUInt("nPointsVertical",   (uint)cameraSensor.depthResolution.y);

                _vfxGraph.SetInt("colorCameraResolutionX", cameraSensor.colorResolution.x);
                _vfxGraph.SetInt("colorCameraResolutionY", cameraSensor.colorResolution.y);

                _vfxGraph.SetInt("irCameraResolutionX", cameraSensor.depthResolution.x);
                _vfxGraph.SetInt("irCameraResolutionY", cameraSensor.depthResolution.y);

                _vfxGraph.SetTexture("ColorCameraView_Texture2D",      cameraSensor.color_Texture);
                _vfxGraph.SetTexture("PointsBakedPositions_Texture2D", cameraSensor.pointsPos_Texture);
                _vfxGraph.SetTexture("PointsColors_Texture2D",        cameraSensor.pointsColors_Texture);
                _vfxGraph.SetTexture("bodyIndexBaked_Texture2D",       cameraSensor.bodyIndex_Texture);
            }
        }


        /// <summary>
        /// turn on/off colorful highlight  of bodies
        /// </summary>
        /// <param name="value"></param>
        public void SetHighlightBodies(bool value){
            _vfxGraph.SetInt("bodiesHighlight", value ? 1 : 0);
            settingsManager.SetProperty(BoolSettingsProperty.POINTCLOUD__HIGHLIGHT_BODIES, value);
        }


        /// <summary>
        /// turn on/off cutting background around bodies
        /// </summary>
        /// <param name="value"></param>
        public void SetCutBackground(bool value){
            _vfxGraph.SetInt("cutBackground", value ? 1 : 0);
            settingsManager.SetProperty(BoolSettingsProperty.POINTCLOUD__CUT_BACKGROUND, value);
        }



        /// <summary>
        /// Set particles size
        /// </summary>
        /// <param name="value"></param>
        public void SetPointSize(float value) {
            _vfxGraph.SetFloat("pointSize", value);
            settingsManager.SetProperty(FloatSettingsProperty.POINTCLOUD__POINT_SIZE, value);
        }
    }
} //! namespace ryabomar