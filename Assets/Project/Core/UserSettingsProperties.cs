﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace ryabomar {
    
    [Serializable] public enum IntSettingsProperty {
        
    }

    [Serializable] public enum FloatSettingsProperty {
        POINTCLOUD__POINT_SIZE, 
        PROJECTION_FRAME_POS_X,
        PROJECTION_FRAME_POS_Y,
        PROJECTION_FRAME_POS_Z,
        PROJECTION_FRAME_EULER_X,
        PROJECTION_FRAME_EULER_Y,
        PROJECTION_FRAME_EULER_Z,
    }


    [Serializable] public enum StringSettingsProperty {
        SENSORSWITCHER__LAST_SENSOR,
        DRESSING__LAST_USER_BODY_TYPE,
    }

    [Serializable] public enum BoolSettingsProperty {
        POINTCLOUD__CUT_BACKGROUND,
        POINTCLOUD__HIGHLIGHT_BODIES,
        DEBUG__ENABLED,
        DEBUG__WIREFRAME_BODIES_IS_VISIBLE,
        DEBUG__SHOW_LOGGER,
        DEBUG__USER_BODY_VISIBLE,
        DRESSING__USER_BODY_TYPE,
    }


    [Serializable] public class IntSettingsEvent    : UnityEvent<IntSettingsProperty, int>   {}
    [Serializable] public class FloatSettingsEvent  : UnityEvent<FloatSettingsProperty, float> {}
    [Serializable] public class StringSettingsEvent : UnityEvent<StringSettingsProperty, string>{}
    [Serializable] public class BoolSettingsEvent   : UnityEvent<BoolSettingsProperty, bool>  {}



    [Serializable] public class IntSettingsEntry {
        public IntSettingsProperty propertyEnum;
        public IntSettingsEvent onChanged = new IntSettingsEvent();
        public int defaultValue;
        public int value {
            get { return _value; } 
            set { _value = value; onChanged.Invoke(propertyEnum, _value); }
        }

        int _value;
    }


    [Serializable] public class FloatSettingsEntry {
        public FloatSettingsProperty propertyEnum;
        public FloatSettingsEvent onChanged = new FloatSettingsEvent();
        public float defaultValue;
        public float value {
            get { return _value; } 
            set { _value = value; onChanged.Invoke(propertyEnum, _value); }
        }

        float _value;
    }


    [Serializable] public class StringSettingsEntry {
        public StringSettingsProperty propertyEnum;
        public StringSettingsEvent onChanged = new StringSettingsEvent();
        public string defaultValue;
        public string value {
            get { return _value; } 
            set { _value = value; onChanged.Invoke(propertyEnum, _value); }
        }

        string _value;
    }



    [Serializable] public class BoolSettingsEntry {
        public BoolSettingsProperty propertyEnum;
        public BoolSettingsEvent onChanged = new BoolSettingsEvent();
        public bool defaultValue;
        public bool value {
            get { return _value; } 
            set { _value = value; onChanged.Invoke(propertyEnum, _value); }
        }

        bool _value;
    }



}
