﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using TMPro; 

namespace ryabomar {
    
    public class BodyIndicator : MonoBehaviour, IBodyVisualization {

        [SerializeField] TextMeshProUGUI textMeshPro;

        public ulong bodyId => _id;

        ulong _id;

        public void Init(BodyData bodyData) {
            _id = bodyData.id;
            textMeshPro.SetText("#" + bodyId.ToString());
            UpdateData(bodyData);
        }


        public void UpdateData(BodyData bodydata) {
            Vector3 headPosition = bodydata.joints[(int)Joint.Type.HEAD].position;
            headPosition.y += 0.2f;
            transform.position = headPosition;
        }


        public void Destroy(BodyData bodydata) {
            Destroy(gameObject);
        }


        public void ShowId(bool value){
            textMeshPro.gameObject.SetActive(value);
        }

        public void ShowActiveIndicator(bool value){
            
        }
    }

} //! namespace ryabomar