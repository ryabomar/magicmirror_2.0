﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ryabomar {
    
    public class BodyIndicatorSpawner : MonoBehaviour {
        [SerializeField] GameObject bodyIndicatorPrefab;
        [SerializeField] SensorSwitcherHandle sensorSwitcherHandle;

        Dictionary<ulong, BodyIndicator> _bodyIndicators = new Dictionary<ulong, BodyIndicator>();

        public void Start(){
            sensorSwitcherHandle.newBodyAppeared.AddListener(SpawnBodyIndicator);
        }


        public void SpawnBodyIndicator(BodyData bodyData){
            GameObject newBodyIndicator = GameObject.Instantiate(bodyIndicatorPrefab, this.transform);
            newBodyIndicator.name = bodyData.id.ToString();
            
            BodyIndicator bodyIndicator = newBodyIndicator.GetComponent<BodyIndicator>();

            if(bodyIndicator != null) {
                bodyIndicator.Init(bodyData);
                sensorSwitcherHandle.AddBodyListeners(bodyData.id, bodyIndicator.UpdateData, bodyIndicator.Destroy);
            }

        }
    }

} //! namespace ryabomar