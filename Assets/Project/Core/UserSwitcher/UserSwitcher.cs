﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ryabomar {

    /// <summary>
    /// controls which of a user (body) is currently active
    /// TODO: manual switching
    /// </summary>
    public class UserSwitcher : MonoBehaviour {

        [Header("References")]
        [SerializeField] SensorSwitcherHandle sensorSwitcherHandle;
        [SerializeField] DiamondIndicator diamondIndicator;

        [Header("Events")]
        public BodyTrackingEvent activeBodySwitched  = new BodyTrackingEvent(); // will send null as BodyData if active body wes unset
        public BodyTrackingEvent activeBodyUpdated   = new BodyTrackingEvent();


        public ulong activeBodyId {get; private set;} = 0;

        List<ulong> knownIds = new List<ulong>();


        void Start() {
            sensorSwitcherHandle.newBodyAppeared.AddListener((bodyData) => {
                    knownIds.Add(bodyData.id);
                    if(activeBodyId == 0){
                        SetAvtiveBody(bodyData.id);
                    }
                });

            sensorSwitcherHandle.bodyUpdated.AddListener((bodyData) => { 
                    if(bodyData.id == activeBodyId)
                        activeBodyUpdated.Invoke(bodyData);
                });

            sensorSwitcherHandle.bodyDisappeared.AddListener((bodyData)=>{
                    knownIds.Remove(bodyData.id);
                    if(bodyData.id == activeBodyId){
                        SetRandomBodyActive();
                    }
                });

            sensorSwitcherHandle.sensorChanged.AddListener((_) => {
                SetRandomBodyActive();
            });


            activeBodyUpdated.AddListener(diamondIndicator.OnActiveBodyUpdated);
            activeBodySwitched.AddListener(diamondIndicator.OnActiveBodySwitched);

        }


        /// <summary>
        /// switch active body to the body with given id
        /// </summary>
        /// <param name="id">body id or 0 to unset</param>
        public void SetAvtiveBody(ulong id){

            // subscribe new body
            activeBodyId = id;

            // inform listeners
            activeBodySwitched.Invoke(sensorSwitcherHandle.FindBodyDataById(id));

            if(activeBodyId == 0) {
                Debug.Log("[UserSwitcher] active body unset");    
            } else {
                Debug.Log("[UserSwitcher] set active body to #" + activeBodyId);
            }
        }


        /// <summary>
        /// unset active user
        /// </summary>
        public void UnsetActiveBody() {
            SetAvtiveBody(0);
        }



        /// <summary>
        /// switch active to random user; never choses current user
        /// </summary>
        public void SetRandomBodyActive(){
            if(knownIds.Count == 0) {
                UnsetActiveBody();
            }

            if(knownIds.Count > 1) {
                ulong newActiveBodyId = activeBodyId;
                while(activeBodyId == newActiveBodyId){
                    newActiveBodyId = knownIds[Random.Range(0, knownIds.Count)];
                }
                SetAvtiveBody(newActiveBodyId);
            }
        }      

    } //! class UserSwitcher 

} //! namespace ryabomar