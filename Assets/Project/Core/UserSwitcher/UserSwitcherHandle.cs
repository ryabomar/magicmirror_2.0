﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar {

    /// <summary>
    /// handle for UserSwitcher; see UserSwitcher
    /// </summary>
    [CreateAssetMenu(fileName = "UserSwitcherHandle", menuName = "MagicMirror/UserSwitcherHandle")]
    public class UserSwitcherHandle : ScriptableObject {
        public BodyTrackingEvent activeBodySwitched  => _userSwitcher?.activeBodySwitched;
        public BodyTrackingEvent activeBodyUpdated   => _userSwitcher?.activeBodyUpdated;
        public ulong activeBodyId => _GetActiveBodyId();

        UserSwitcher _userSwitcher => FindObjectOfType<UserSwitcher>();


        public void SetAvtiveBody(ulong id) {
            _userSwitcher?.SetAvtiveBody(id);
        }

        public void UnsetActiveBody() {
            SetAvtiveBody(0);
        }


        ulong _GetActiveBodyId() {
            if(_userSwitcher != null) {
                return _userSwitcher.activeBodyId;
            } else {
                return 0;
            }
        }

        public void SetRandomBodyActive(){
            _userSwitcher?.SetRandomBodyActive();
        }
    } //! class UserSwitcherHandle
} //! namespace ryabomar