﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ryabomar {
    
    [Serializable] public class SensorChangedEvent : UnityEvent<SensorSwitcher.SensorEntry> { } 

    /// <summary>
    /// performs sensor switching 
    /// </summary>
    public class SensorSwitcher : MonoBehaviour {

        [SerializeField] UserSettingsManager settingsManager;

        /// <summary> representation of sensor with access to it's camera and body tracking interfaces</summary>
        [Serializable] public class SensorEntry {
            public ICameraSensor cameraSensor;
            public IBodyTracker  bodyTracker;
            public GameObject    gameObject;

            public string        name => gameObject.name;
        }


        /// <summary>internal class to tie pair of events related to a specific body</summary>
        private class BodyEntry {
            public BodyTrackingEvent updated    = new BodyTrackingEvent();
            public BodyTrackingEvent disapeared = new BodyTrackingEvent();
        }


        /// <summary> been invoked whenever sensor switched </summary>
        public SensorChangedEvent sensorChanged  = new SensorChangedEvent();
        
        
        /// <summary>been invoked when the new body had been detected</summary>
        public BodyTrackingEvent newBodyAppeared = new BodyTrackingEvent();

        /// <summary> been invoked when any body had been no longer tracked </summary>
        public BodyTrackingEvent bodyDisappeared = new BodyTrackingEvent(); // any body disappeared

        /// <summary> been invoked when any body had been updated </summary>
        public BodyTrackingEvent bodyUpdated     = new BodyTrackingEvent(); // any body updated


        /// <summary> currently active sensor; may be null if no one active </summary>
        public SensorEntry activeSensor { get => _activeSensor; }

        /// <summary> list on names of known sensors </summary>
        public List<string> knownSensors { get => _FindSensors().ConvertAll(entry => entry.name); } 


        List<SensorEntry> _sensors   = null;
        SensorEntry _activeSensor = null;


        Dictionary<ulong, BodyEntry> _perBodyEvents = new Dictionary<ulong, BodyEntry>(); // individual events per body

        Dictionary<Joint.Type, Vector3> _jointOffsets = new Dictionary<Joint.Type, Vector3>(); 

        /// <summary> switches to last selected sensor or first sensor on start </summary>
        public void Start() {
            _sensors = _FindSensors();

            //StartCoroutine()
            if(settingsManager!= null 
                && settingsManager.HasProperty(StringSettingsProperty.SENSORSWITCHER__LAST_SENSOR))
                {
                
                string sensorName = settingsManager.GetPropertyValue(StringSettingsProperty.SENSORSWITCHER__LAST_SENSOR);

                int idx = _sensors.FindIndex(entry => entry.name == sensorName);
                if(idx != -1){
                    SwitchTo(sensorName);
                } else {
                    _SwitchToFirstAvailableSensor();
                }

            } else {
                _SwitchToFirstAvailableSensor();
            }
        }


        /// <summary> Switch to another sensor </summary>
        /// <param name="name">sensor name</param>
        public void SwitchTo(string name) {
            Debug.Log("[SensorSwitcher] switching to " + name);

            SensorEntry sensorEntry = _sensors.Find(_ => _.name == name);
            if(sensorEntry != null) {

                // deactivate current first
                if(_activeSensor!= null) { 
                    _activeSensor.bodyTracker.BodyAppeared_Event.RemoveListener(_OnBodyAppeared);
                    _activeSensor.bodyTracker.BodyDisappeared_Event.RemoveListener(_OnBodyDisappeared);
                    _activeSensor.bodyTracker.BodyUpdated_Event.RemoveListener(_OnBodyUpdated);

                    // inform of destroy of old bodies
                    foreach(BodyData bodyData in _activeSensor.bodyTracker.GetLastFrameBodies()){
                        bodyDisappeared.Invoke(bodyData);
                    }

                    _activeSensor.gameObject.SetActive(false);
                }


                // activate requested sensor
                _activeSensor = sensorEntry;

                _activeSensor.bodyTracker.BodyAppeared_Event.AddListener(_OnBodyAppeared);
                _activeSensor.bodyTracker.BodyDisappeared_Event.AddListener(_OnBodyDisappeared);
                _activeSensor.bodyTracker.BodyUpdated_Event.AddListener(_OnBodyUpdated);

                _activeSensor.gameObject.SetActive(true);

                sensorChanged.Invoke(_activeSensor);

                // inform of new bodies
                // foreach(BodyData bodyData in _activeSensor.bodyTracker.GetLastFrameBodies()){
                //     newBodyAppeared.Invoke(bodyData);
                // }

                settingsManager?.SetProperty(StringSettingsProperty.SENSORSWITCHER__LAST_SENSOR, _activeSensor.name);
            } else {
                throw new System.ArgumentException("unknown sensor: " + name);
            }
        }


        /// <summary> subscribe on body events </summary>
        /// <param name="id">body id</param>
        /// <param name="onUpdated">delegate</param>
        /// <param name="onDisappeared">delegate</param>
        public void AddBodyListeners(ulong id, UnityAction<BodyData> onUpdated, UnityAction<BodyData> onDisappeared) {
            if(_perBodyEvents.TryGetValue(id, out BodyEntry entry)){
                entry.updated.AddListener(onUpdated);
                entry.disapeared.AddListener(onDisappeared);
            }
        }

        
        /// <summary> unsubscribe on body events; opposite of AddBodyListeners</summary>
        /// <param name="id">body id</param>
        /// <param name="onUpdated">delegate</param>
        /// <param name="onDisappeared">delegate</param>
        public void RemoveBodyListeners(ulong id, UnityAction<BodyData> onUpdated, UnityAction<BodyData> onDisappeared) {
            if(_perBodyEvents.TryGetValue(id, out BodyEntry entry)){
                entry.updated.RemoveListener(onUpdated);
                entry.disapeared.RemoveListener(onDisappeared);
            }
        }


        /// <summary> Find BodyData by given id in last frame bodies </summary>
        /// <param name="id"></param>
        /// <returns>BodyData or null if not found or no sensor is active</returns>
        public BodyData FindBodyDataById(ulong id) {
            if(activeSensor != null) {
                return activeSensor.bodyTracker
                            .GetLastFrameBodies()
                            .Find(bodyData => bodyData.id == id);
            } else {
                return null;
            }
        }

        public void SetJointOffset(Joint.Type type, Vector3 offset){
            _jointOffsets[type] = offset;
        }


        void _ApplyOffsets(ref BodyData bodyData){
            foreach(var entry in _jointOffsets){
                bodyData.joints[(int)entry.Key].position += entry.Value;
            }
        }

        List<SensorEntry> _FindSensors(){
            List<SensorEntry> list = new List<SensorEntry>();

            foreach(Transform child in transform) {
                SensorEntry sensorEntry = new SensorEntry();
                
                sensorEntry.gameObject = child.gameObject;
                sensorEntry.cameraSensor = child.GetComponent<ICameraSensor>();
                sensorEntry.bodyTracker  = child.GetComponent<IBodyTracker>();

                if(sensorEntry.bodyTracker != null && sensorEntry.cameraSensor != null){
                    list.Add(sensorEntry);
                }
            }

            return list;
        }



        void _OnBodyAppeared(BodyData bodyData){
            _ApplyOffsets(ref bodyData);

            _perBodyEvents[bodyData.id] = new BodyEntry();
            newBodyAppeared.Invoke(bodyData);
        }


        void _OnBodyUpdated(BodyData bodyData){
            _ApplyOffsets(ref bodyData);

            bodyUpdated.Invoke(bodyData);

            if(_perBodyEvents.TryGetValue(bodyData.id, out BodyEntry entry)){
                // individual event
                entry.updated.Invoke(bodyData);
            }
        }
        

        void _OnBodyDisappeared(BodyData bodyData){
            _ApplyOffsets(ref bodyData);

            bodyDisappeared.Invoke(bodyData);
            
            if(_perBodyEvents.TryGetValue(bodyData.id, out BodyEntry entry)){
                // individual event
                entry.disapeared.Invoke(bodyData);
                _perBodyEvents.Remove(bodyData.id);
            }
        }


        void _SwitchToFirstAvailableSensor(){
           if(_sensors.Count > 0){
                SwitchTo(_sensors[0].name);
            }
        }
    }
} //! namespace ryabomar 