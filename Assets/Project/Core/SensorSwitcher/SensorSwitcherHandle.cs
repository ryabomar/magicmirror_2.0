﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ryabomar {

    /// <summary> SensorSwitcher handle; See SensorSwitcher </summary>
    [CreateAssetMenu(fileName = "SensorSwitcherHandle", menuName = "MagicMirror/SensorSwitcherHandle")]
    public class SensorSwitcherHandle : ScriptableObject {

        public SensorChangedEvent   sensorChanged   => _switcher?.sensorChanged; 

        public BodyTrackingEvent    newBodyAppeared => _switcher?.newBodyAppeared;
        public BodyTrackingEvent    bodyDisappeared => _switcher?.bodyDisappeared;
        public BodyTrackingEvent    bodyUpdated     => _switcher?.bodyUpdated;
    

        public SensorSwitcher.SensorEntry  activeSensor  => _switcher?.activeSensor;

        public List<string> knownSensors => _switcher?.knownSensors; 

        private SensorSwitcher _switcher => FindObjectOfType<SensorSwitcher>();


        public void SwitchTo(string name) {
            var switcher = _switcher;
            if(switcher != null) {
                switcher.SwitchTo(name);
            }
        }


        public List<BodyData> GetLastFrameBodies() {
            if(activeSensor != null) {
                return activeSensor.bodyTracker.GetLastFrameBodies();
            } else {
                return new List<BodyData>();
            }
        }


        public void AddBodyListeners(ulong id, UnityAction<BodyData> onUpdated, UnityAction<BodyData> onDisappeared){
            var switcher = _switcher;
            if(switcher != null) {
                switcher.AddBodyListeners(id, onUpdated, onDisappeared);
            }
        }


        public void RemoveBodyListeners(ulong id, UnityAction<BodyData> onUpdated, UnityAction<BodyData> onDisappeared) {
            var switcher = _switcher;
            if(switcher != null) {
                switcher.RemoveBodyListeners(id, onUpdated, onDisappeared);
            }
        }

        public BodyData FindBodyDataById(ulong id) {
            return _switcher?.FindBodyDataById(id);
        }


        public void SetJointOffset(Joint.Type type, Vector3 offset){
            _switcher?.SetJointOffset(type, offset);
        }
    } //! class SensorSwitcherHandler 

} //! namespace ryabomar

