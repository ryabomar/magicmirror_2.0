﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ryabomar {

    /// <summary>
    /// single place to get and set settings application wide
    /// 
    /// can save int, float, bool, and string but first have to add enum property (see UserSettingProperties)
    ///
    /// </summary>
    [CreateAssetMenu(fileName = "UserSettingsManager", menuName = "MagicMirror/UserSettingsManager")]
    public class UserSettingsManager : ScriptableObject {
        public List<IntSettingsEntry>    intSettings    = new List<IntSettingsEntry>();
        public List<FloatSettingsEntry>  floatSettings  = new List<FloatSettingsEntry>();
        public List<StringSettingsEntry> stringSettings = new List<StringSettingsEntry>();
        public List<BoolSettingsEntry>   boolSettings   = new List<BoolSettingsEntry>();

        bool _initialized = false;

        public void Init(){
            if(_initialized) return;
            _LoadPropertiesFromPlayerPrefs();
            _SubscribeOnEvents();
            _initialized = true;
        }


        /// <summary>
        /// save properties using PlayerPrefs (to registry on Windows)
        /// </summary>
        public void SaveProperties(){
            foreach(var entry in intSettings){
                _SaveIntToPlayerPrefs(entry.propertyEnum, entry.defaultValue);
            }

            foreach(var entry in floatSettings){
                _SaveFloatToPlayerPrefs(entry.propertyEnum, entry.defaultValue);
            }

            foreach(var entry in stringSettings){
                _SaveStringToPlayerPrefs(entry.propertyEnum, entry.defaultValue);
            }

            foreach(var entry in boolSettings){
                _SaveBoolToPlayerPrefs(entry.propertyEnum, entry.defaultValue);
            }

            PlayerPrefs.Save();
        }


        public int GetPropertyValue(IntSettingsProperty property){
            Init();
            return intSettings.Find((entry) => entry.propertyEnum == property).value;
        }


        public float GetPropertyValue(FloatSettingsProperty property){
            Init();
            return floatSettings.Find((entry) => entry.propertyEnum == property).value;
        }


        public string GetPropertyValue(StringSettingsProperty property){
            Init();
            return stringSettings.Find((entry) => entry.propertyEnum == property).value;
        }


        public bool GetPropertyValue(BoolSettingsProperty property){
            Init();
            return boolSettings.Find((entry) => entry.propertyEnum == property).value;
        }



        public bool HasProperty(IntSettingsProperty property){
            Init();
            int idx = intSettings.FindIndex((entry) => entry.propertyEnum == property);
            return idx >= 0;
        }


        public bool HasProperty(FloatSettingsProperty property){
            Init();
            int idx = floatSettings.FindIndex((entry) => entry.propertyEnum == property);
            return idx >= 0;
        }


        public bool HasProperty(StringSettingsProperty property){
            Init();
            int idx =  stringSettings.FindIndex((entry) => entry.propertyEnum == property);
            return idx >= 0;
        }


        public bool HasProperty(BoolSettingsProperty property){
            Init();
            int idx =  boolSettings.FindIndex((entry) => entry.propertyEnum == property);
            return idx >= 0;
        }


        public void SetProperty(IntSettingsProperty property, int value){
            Init();
            int idx = intSettings.FindIndex((entry) => entry.propertyEnum == property);
            if(idx >= 0) {
                intSettings[idx].value = value;
            } else {
                IntSettingsEntry entry = new IntSettingsEntry{
                    propertyEnum = property,
                    defaultValue = value,
                    value = value
                };
                entry.onChanged.AddListener(_SaveIntToPlayerPrefs);
                intSettings.Add(entry);
            }
        }


        public void SetProperty(FloatSettingsProperty property, float value){
            Init();
            int idx = floatSettings.FindIndex((entry) => entry.propertyEnum == property);
            if(idx >= 0) {
                floatSettings[idx].value = value;
            } else {
                FloatSettingsEntry entry = new FloatSettingsEntry{
                    propertyEnum = property,
                    defaultValue = value,
                    value = value
                };
                entry.onChanged.AddListener(_SaveFloatToPlayerPrefs);
                floatSettings.Add(entry);
            }
        }


        public void SetProperty(StringSettingsProperty property, string value){
            Init();
            int idx =  stringSettings.FindIndex((entry) => entry.propertyEnum == property);
            if(idx >= 0) {
                stringSettings[idx].value = value;
            } else {
                StringSettingsEntry entry = new StringSettingsEntry{
                    propertyEnum = property,
                    defaultValue = value,
                    value = value
                };
                entry.onChanged.AddListener(_SaveStringToPlayerPrefs);
                stringSettings.Add(entry);
            }
        }


        public void SetProperty(BoolSettingsProperty property, bool value){
            Init();
            int idx =  boolSettings.FindIndex((entry) => entry.propertyEnum == property);
            if(idx >= 0) {
                boolSettings[idx].value = value;
            } else {
                BoolSettingsEntry entry = new BoolSettingsEntry{
                    propertyEnum = property,
                    defaultValue = value,
                    value = value
                };
                entry.onChanged.AddListener(_SaveBoolToPlayerPrefs);
                boolSettings.Add(entry);
            }
        }



        void _LoadPropertiesFromPlayerPrefs(){
            for(int i = 0; i < intSettings.Count; i++){ // load ints
                intSettings[i].value = PlayerPrefs.GetInt(intSettings[i].propertyEnum.ToString(), intSettings[i].value);
            }

            for(int i = 0; i < floatSettings.Count; i++){ // load floats
                floatSettings[i].value = PlayerPrefs.GetFloat(floatSettings[i].propertyEnum.ToString(), floatSettings[i].value);
            }

            for(int i = 0; i < stringSettings.Count; i++){ // load strings
                stringSettings[i].value = PlayerPrefs.GetString(stringSettings[i].propertyEnum.ToString(), stringSettings[i].value);
            }

            for(int i = 0; i < boolSettings.Count; i++){ // load bools
                string stringValue = PlayerPrefs.GetString(boolSettings[i].propertyEnum.ToString(), boolSettings[i].value ? "true" : "false");
                boolSettings[i].value = stringValue.ToLower() == "true";
            }

            // // check missing entries
            // foreach(IntSettingsProperty property in Enum.GetValues(typeof(IntSettingsProperty))){
            //     if(!HasProperty(property)){
            //         throw new Exception("property doesn't have entry in settings manager: " + property.ToString());
            //     }
            // }

            // foreach(FloatSettingsProperty property in Enum.GetValues(typeof(FloatSettingsProperty))){
            //     if(!HasProperty(property)){
            //         throw new Exception("property doesn't have entry in settings manager: " + property.ToString());
            //     }
            // }

            // foreach(StringSettingsProperty property in Enum.GetValues(typeof(StringSettingsProperty))){
            //     if(!HasProperty(property)){
            //         throw new Exception("property doesn't have entry in settings manager: " + property.ToString());
            //     }
            // }

            // foreach(BoolSettingsProperty property in Enum.GetValues(typeof(BoolSettingsProperty))){
            //     if(!HasProperty(property)){
            //         throw new Exception("property doesn't have entry in settings manager: " + property.ToString());
            //     }
            // }
        }



        void _SubscribeOnEvents(){
            foreach(var entry in intSettings){
                entry.onChanged.AddListener(_SaveIntToPlayerPrefs);
            }

            foreach(var entry in floatSettings){
                entry.onChanged.AddListener(_SaveFloatToPlayerPrefs);
            }

            foreach(var entry in stringSettings){
                entry.onChanged.AddListener(_SaveStringToPlayerPrefs);
            }

            foreach(var entry in boolSettings){
                entry.onChanged.AddListener(_SaveBoolToPlayerPrefs);
            }
        }


        void _SaveIntToPlayerPrefs(IntSettingsProperty propertyEnum, int value){
            PlayerPrefs.SetInt(propertyEnum.ToString(), value);
            PlayerPrefs.Save();
        }


        void _SaveFloatToPlayerPrefs(FloatSettingsProperty propertyEnum, float value){
            PlayerPrefs.SetFloat(propertyEnum.ToString(), value);
            PlayerPrefs.Save();
        }


        void _SaveStringToPlayerPrefs(StringSettingsProperty propertyEnum, string value){
            PlayerPrefs.SetString(propertyEnum.ToString(), value);
            PlayerPrefs.Save();
        }


        void _SaveBoolToPlayerPrefs(BoolSettingsProperty propertyEnum, bool value){
            PlayerPrefs.SetString(propertyEnum.ToString(), value ? "true" : "false");
            PlayerPrefs.Save();
        }




    }

} //! namespace ryabomar