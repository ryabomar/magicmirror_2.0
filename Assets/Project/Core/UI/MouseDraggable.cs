﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace ryabomar.UI {


    /// <summary>
    /// attach this component to make ui element draggable by mouse
    /// 
    /// for window-like behaviour attack this script to header and set elementToDrag to window
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class MouseDraggable : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
    {

        [SerializeField] bool enableDrag       = true;

        [Tooltip("keep empty to make draggable itself")]
        [SerializeField] RectTransform elementToDrag;  // <- this object will be moved


        Vector2 _lastMousePos;
        RectTransform _rectTransform;

        void Awake(){
            // will move self rect transform if not specified else
            _rectTransform = elementToDrag == null ? GetComponent<RectTransform>() : elementToDrag;
        }


        public void OnBeginDrag(PointerEventData pointerEventData) {
            _lastMousePos = pointerEventData.position;
        }


        public void OnEndDrag(PointerEventData pointerEventData) {}


        public void OnDrag(PointerEventData pointerEventData) {
            if(enableDrag) {
                _Move(pointerEventData.position - _lastMousePos);
            }

            _lastMousePos = pointerEventData.position;
        }


        void _Move(Vector3 delta) {
            _rectTransform.position = _rectTransform.position + new Vector3(delta.x, delta.y, _rectTransform.position.z);
        }
    }

} //! namespace ryabomar.UI