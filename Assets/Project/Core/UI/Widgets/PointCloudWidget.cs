﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ryabomar.UI {

public class PointCloudWidget : MonoBehaviour
{
    [SerializeField] PointCloudHandle pointCloudHandle;
    [SerializeField] UserSettingsManager settingsManager;

    [SerializeField] Slider pointSizeSlider;
    [SerializeField] Toggle hightlightBodiesToggle;
    [SerializeField] Toggle cutBackgroundToggle;

    void Start(){
        if(settingsManager.HasProperty(FloatSettingsProperty.POINTCLOUD__POINT_SIZE)){
            pointSizeSlider.SetValueWithoutNotify(settingsManager.GetPropertyValue(FloatSettingsProperty.POINTCLOUD__POINT_SIZE));
        }


        if(settingsManager.HasProperty(BoolSettingsProperty.POINTCLOUD__HIGHLIGHT_BODIES)){
            hightlightBodiesToggle.SetIsOnWithoutNotify(settingsManager.GetPropertyValue(BoolSettingsProperty.POINTCLOUD__HIGHLIGHT_BODIES));
        }


        if(settingsManager.HasProperty(BoolSettingsProperty.POINTCLOUD__CUT_BACKGROUND)){
            cutBackgroundToggle.SetIsOnWithoutNotify(settingsManager.GetPropertyValue(BoolSettingsProperty.POINTCLOUD__CUT_BACKGROUND));
        }
    }

    public void SetHighlightBodies(bool value){
        pointCloudHandle.SetHighlightBodies(value);
    }

    public void SetCutBackground(bool value){
        pointCloudHandle.SetCutBackground(value);
    }

    public void SetPointSize(float value) {
        pointCloudHandle.SetPointSize(value);
    }

}

} // namespace ryabomar.UI