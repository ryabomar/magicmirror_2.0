﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace ryabomar.UI {
    
    public class SensorSwitcherWidget : MonoBehaviour {

        [SerializeField] UserSettingsManager userSettingsManager;

        [SerializeField] RawImage colorCameraRawImage;
        [SerializeField] RawImage DepthRawImage;
        [SerializeField] RawImage BakedPositionsRawImage;
        [SerializeField] RawImage BakedUVsRawImage;
        [SerializeField] RawImage BodyIndeciesRawImage;

        [SerializeField] Dropdown dropdownSelector;

        [SerializeField] SensorSwitcherHandle sensorSwitcherHandle;

        private List<string> _sensorsNames;

        bool _initialized = false; // 

        public void DropdownValueChanged(int i) {
            if(_initialized){
                sensorSwitcherHandle.SwitchTo(_sensorsNames[i]);
            }
        }


        void Start() {
            _sensorsNames = sensorSwitcherHandle.knownSensors;
            _SetupDropdown(_sensorsNames);

            

            if(sensorSwitcherHandle.activeSensor != null) { // manually invoke if already initialized
                _SensorChanged(sensorSwitcherHandle.activeSensor);
                
                int optionIdx = dropdownSelector.options.FindIndex((option) => option.text == sensorSwitcherHandle.activeSensor.name);
                // dropdownSelector.SetValueWithoutNotify(optionIdx); // BUG in unity; doesn't chande selected option
                dropdownSelector.value = dropdownSelector.options.FindIndex((option) => option.text == sensorSwitcherHandle.activeSensor.name);


            } else {
                if(userSettingsManager != null && 
                    userSettingsManager.HasProperty(StringSettingsProperty.SENSORSWITCHER__LAST_SENSOR)
                    ){
                        Debug.Log("load last sensor name");
                        int optionIdx = dropdownSelector.options.FindIndex(
                                (option) => {
                                    return option.text == userSettingsManager.GetPropertyValue(StringSettingsProperty.SENSORSWITCHER__LAST_SENSOR);
                                }
                            );
                    
                        // dropdownSelector.SetValueWithoutNotify(optionIdx);
                        dropdownSelector.value = optionIdx;
                }
            }
            sensorSwitcherHandle.sensorChanged.AddListener(_SensorChanged);
            _initialized = true;
        }


        void _SensorChanged(SensorSwitcher.SensorEntry sensor){
            _SetupTextures(sensor.cameraSensor);
        }


        void _SetupTextures(ICameraSensor cameraSensor){
            if(cameraSensor != null) {
                colorCameraRawImage.texture     = cameraSensor.color_Texture;
                DepthRawImage.texture           = cameraSensor.depthData_Texture;
                BakedPositionsRawImage.texture  = cameraSensor.pointsPos_Texture;
                BakedUVsRawImage.texture        = cameraSensor.pointsColors_Texture;
                BodyIndeciesRawImage.texture    = cameraSensor.bodyIndex_Texture;
            }
        }

        void _SetupDropdown(List<string> sensorsNames){
            if(sensorsNames.Count > 0) {
                dropdownSelector.AddOptions(sensorsNames);
                //dropdownSelector.SetValueWithoutNotify(0);
            }
        }


    } //! class SensorSwitcherWidget
} //! namespace ryabomar.UI 