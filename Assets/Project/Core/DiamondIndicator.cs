﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar {

    /// <summary>
    /// green diamond above active users head; (like in Sims)
    /// </summary>
    //[RequireComponent(typeof(MeshRenderer))]
    public class DiamondIndicator : MonoBehaviour {
        Vector3 _initPosition;

        void Awake() {
            _initPosition = transform.position;
        }

        /// <summary> delegate </summary>
        public void OnActiveBodyUpdated(BodyData bodyData) {
            _MoveTo(bodyData.joints[(int)Joint.Type.SPINE_CHEST].position, new Vector3(0, 0.5f, 0));
        }


        /// <summary> delegate </summary>
        public void OnActiveBodySwitched(BodyData bodyData) {
            if(bodyData == null || bodyData.id == 0) {
                _Reset();
                _SetVisible(false);
            } else {
                _SetVisible(true);
                OnActiveBodyUpdated(bodyData);
            }
        }


        void _Reset(){
            transform.position = _initPosition;
        }


        void _SetVisible(bool value){
            //GetComponent<MeshRenderer>().enabled = value;
            foreach(Transform child in transform){
                child.gameObject.SetActive(value);
            }
        }


        void _MoveTo(Vector3 position, Vector3 offset){
            transform.position = position + offset;
        }

    } //! class ActiveBodyIndicator

} //! namespace ryabomar