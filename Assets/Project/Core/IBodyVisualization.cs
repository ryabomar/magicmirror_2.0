﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar { 

    /// <summary> interface for visualization of BodyData </summary>
    public interface IBodyVisualization {
        ulong bodyId { get; }
        void UpdateData(BodyData bodydata);
        void Destroy(BodyData bodydata);
    }

 
}// !namespace ryabomar 