﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ryabomar {
    /// <summary>
    /// handle for WireframeBodySpawner
    /// </summary>
    [CreateAssetMenu(fileName = "WireframeBodySpawnerHandle", menuName = "MagicMirror/WireframeBodySpawnerHandle")]
    public class WireframeBodySpawnerHandle : ScriptableObject {

        WireframeBodyVisualizationSpawner _wireframeBodySpawner => FindObjectOfType<WireframeBodyVisualizationSpawner>();

        public void SetWireframeBodiesVisible(bool value){
            _wireframeBodySpawner?.SetWireframeBodiesVisible(value);
        }


        public bool IsWireframeBodiesVisible() {
            if(_wireframeBodySpawner != null){
                return _wireframeBodySpawner.IsWireframeBodiesVisible();
            }
            return false;
        }
    }

} //! namespace ryabomar
