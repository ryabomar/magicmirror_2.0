﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ryabomar {

    /// <summary>
    /// part of WireframeBodyVisualization
    ///     draws colorful lines
    /// </summary>
    public class WireframeJoint : MonoBehaviour {

        public WireframeJoint parentJointObject;
        public Joint.TrackingState trackingState = Joint.TrackingState.NOT_TRACKED;

        static Color TRACKED_COLOR     = Color.green;
        static Color NOT_TRACKED_COLOR = Color.black;
        static Color INFERRED_COLOR    = Color.red;
        LineRenderer _lineRenderer;

        void Awake(){
            _lineRenderer = GetComponent<LineRenderer>();
            if( _lineRenderer == null ){
                _lineRenderer = gameObject.AddComponent<LineRenderer>();
            }

            _lineRenderer.material = new Material(Shader.Find("Universal Render Pipeline/2D/Sprite-Lit-Default"));
            _lineRenderer.material.color = _GetLineColor();
            _lineRenderer.positionCount = 2;
            _lineRenderer.startWidth = 0.01f;
            _lineRenderer.endWidth = 0.01f;
        }



        void Update() {
            if(parentJointObject != null) {
                _lineRenderer.startColor = _GetLineColor();
                _lineRenderer.endColor   = _GetLineColor();
                _lineRenderer.SetPosition(0, transform.position);
                _lineRenderer.SetPosition(1, parentJointObject.transform.position);
            }
        }

        Color _GetLineColor() {
            if(trackingState == Joint.TrackingState.TRACKED) {
                return TRACKED_COLOR;
            } else if(trackingState == Joint.TrackingState.NOT_TRACKED) {
                return NOT_TRACKED_COLOR;
            } else {
                return INFERRED_COLOR;
            }
        }
    }
} //! namespace ryabomar 
