﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar {

    /// <summary>
    /// controls spawning of WireframeBodyVisualizations
    /// </summary>
    public class WireframeBodyVisualizationSpawner : MonoBehaviour {

        [SerializeField] GameObject         jointPrefab;
        [SerializeField] GameObject         wireframeAvatarsRoot;
        [SerializeField] SensorSwitcherHandle sensorSwitcher;

        [SerializeField] UserSettingsManager settingsManager;

        List<WireframeBodyVisualization> _spawned = new List<WireframeBodyVisualization>();

        bool _isWireframeBodiesVisible = false;


        void Awake() {
            if(wireframeAvatarsRoot == null) {
                wireframeAvatarsRoot = new GameObject("-=[ WIREFRAME AVATARS ]=-");
                wireframeAvatarsRoot.transform.parent = this.transform;
            }
        }


        void Start() {
            sensorSwitcher.bodyDisappeared.AddListener(_OnBodyDestroyed);
            sensorSwitcher.newBodyAppeared.AddListener(SpawnAvatar);

            if(settingsManager.HasProperty(BoolSettingsProperty.DEBUG__WIREFRAME_BODIES_IS_VISIBLE)){
                SetWireframeBodiesVisible(settingsManager.GetPropertyValue(BoolSettingsProperty.DEBUG__WIREFRAME_BODIES_IS_VISIBLE));
            } else {
                SetWireframeBodiesVisible(_isWireframeBodiesVisible);
            }
        }


        /// <summary>
        /// delegate
        /// </summary>
        public void SpawnAvatar(BodyData bodyData) {
            GameObject newObj = new GameObject(bodyData.id.ToString());
            var wireframeAvatar = newObj.AddComponent<WireframeBodyVisualization>();
            newObj.transform.parent = wireframeAvatarsRoot.transform;

            wireframeAvatar.Init(jointPrefab, bodyData);

            sensorSwitcher.AddBodyListeners(bodyData.id, wireframeAvatar.UpdateData, wireframeAvatar.Destroy);
            sensorSwitcher.bodyDisappeared.AddListener(wireframeAvatar.Destroy);

            newObj.SetActive(_isWireframeBodiesVisible);
            _spawned.Add(wireframeAvatar);
        }


        public void SetWireframeBodiesVisible(bool value){
            foreach(var obj in _spawned){
                obj.gameObject.SetActive(value);
            }

            _isWireframeBodiesVisible = value;

            settingsManager.SetProperty(BoolSettingsProperty.DEBUG__WIREFRAME_BODIES_IS_VISIBLE, value);
        }


        public bool IsWireframeBodiesVisible() { return _isWireframeBodiesVisible; }


        void _OnBodyDestroyed(BodyData bodyData){
            int idx = _spawned.FindIndex((wireframe) => wireframe.bodyId == bodyData.id);
            if(idx >= 0){
                _spawned.RemoveAt(idx);
            }
        }

    } //! class WireframeBodyVisualizationSpawner

} //! namespace ryabomar
