﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar { 

    /// <summary>
    /// visualize user body with joints and lines (looks like wireframe) 
    /// // mostly for debug purpose
    /// </summary>
    public class WireframeBodyVisualization : MonoBehaviour, IBodyVisualization {
        /// <summary> body id </summary>
        public ulong bodyId => _id;

        ulong _id;
        Dictionary<Joint.Type, WireframeJoint> _jointComponents = new Dictionary<Joint.Type, WireframeJoint>();


        /// <summary>
        /// delegate
        /// </summary>
        public void Destroy(BodyData bodyData) {
            // foreach(var jointComponent in _jointComponents.Values) {
            //     Destroy(jointComponent.gameObject);
            // }
            if(bodyId == bodyData.id){
                Destroy(gameObject);
            }
        }


        /// <summary>
        /// delegate
        /// </summary>
        public void UpdateData(BodyData bodydata) {
            foreach (Joint joint in bodydata.joints) {
                if(joint.isSupported) {
                    var jointComponent = _jointComponents[joint.type];
                    jointComponent.trackingState = joint.trackingState;

                    GameObject jointObject = jointComponent.gameObject;
                    jointObject.transform.position = joint.position;
                    jointObject.transform.rotation = joint.orientation;
                }
            }
        }


        /// <summary>
        /// initialize 
        /// </summary>
        /// <param name="jointPrefab"></param>
        /// <param name="bodyData"></param>
        public void Init(GameObject jointPrefab, BodyData bodyData) {
            _id = bodyData.id;

            // spawn joints
            foreach(Joint.Type jointType in Enum.GetValues(typeof(Joint.Type))) {
                if(jointType != Joint.Type.NONE) {
                    GameObject jointObject = Instantiate(jointPrefab, this.transform);
                    jointObject.name = jointType.ToString();
                    jointObject.layer = LayerMask.NameToLayer("debug");

                    _jointComponents[jointType] = jointObject.AddComponent<WireframeJoint>();

                    if(!bodyData.joints[(int)jointType].isSupported) {
                        jointObject.SetActive(false); // Disable unsupported joints 
                        // TODO: instead of disabling somehow show it's not supported
                    }
                }
            }

            // link child -> parent
            foreach(var entry in _jointComponents) {
                Joint.Type jointType = entry.Key;
                WireframeJoint jointComponent = _jointComponents[jointType];

                // find supported parent joint type
                Joint.Type supportedParentJointType = jointType.GetParent();
                while (supportedParentJointType != Joint.Type.NONE
                    && !bodyData.joints[(int)supportedParentJointType].isSupported) 
                    {
                    supportedParentJointType = supportedParentJointType.GetParent();
                }

                if (bodyData.joints[(int)jointType].isSupported && jointType.HasParent()) {
                    jointComponent.parentJointObject = _jointComponents[supportedParentJointType];
                } else {
                    jointComponent.parentJointObject = jointComponent; // set itself as a parent to render 0-length line
                }
            }
        }




        
    }

} // ! namespace ryabomar