﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ryabomar.UI { 
    /// <summary>
    /// simple logger to display messages
    /// </summary>
    public class Logger : MonoBehaviour {
        [SerializeField] UserSettingsManager settingsManager;

        [SerializeField] GameObject content;
        [SerializeField] ScrollRect scrollView;

        [Header("Message prefabs")]
            [SerializeField] GameObject errorMessagePrefab;
            [SerializeField] GameObject assertMessagePrefab;
            [SerializeField] GameObject warningMessagePrefab;
            [SerializeField] GameObject logMessagePrefab;
            [SerializeField] GameObject exceptionMessagePrefab;

        [SerializeField][Range(10, 2000)] int maxMessages = 40;
        [SerializeField] bool scrollToBottom = true;

        RectTransform contentRectTransform;


        Dictionary<LogType, GameObject> prefabs = new Dictionary<LogType, GameObject>();

        LinkedList<LoggerMessage> messages = new LinkedList<LoggerMessage>();
        

        /// <summary>
        /// if set true Logger will scroll to last message on every new message
        /// </summary>
        /// <param name="value"></param>
        public void SetScrollToBottom(bool value) {
            scrollToBottom = value;
        }


        GameObject _GetPrefab(LogType type) {
            switch(type){
                case LogType.Error     : return errorMessagePrefab;
                case LogType.Assert    : return assertMessagePrefab;
                case LogType.Warning   : return warningMessagePrefab;
                case LogType.Log       : return logMessagePrefab;
                case LogType.Exception : return exceptionMessagePrefab;
                default: return null;
            }
        }


        void Awake() {
            errorMessagePrefab.SetActive(false);
            assertMessagePrefab.SetActive(false);
            warningMessagePrefab.SetActive(false);
            logMessagePrefab.SetActive(false);
            exceptionMessagePrefab.SetActive(false);

            contentRectTransform = content.GetComponent<RectTransform>();
        }


        void Start(){
            if(settingsManager.HasProperty(BoolSettingsProperty.DEBUG__SHOW_LOGGER)){
                gameObject.SetActive(settingsManager.GetPropertyValue(BoolSettingsProperty.DEBUG__SHOW_LOGGER));
            }
        }


        void OnLogMessageReceived(string msg, string stackTrace, LogType type) {
            // spawn message
            GameObject obj = Instantiate(_GetPrefab(type), content.transform);
            LoggerMessage message = obj.GetComponent<LoggerMessage>();
            message.text = msg;
            obj.SetActive(true);
            messages.AddLast(message);

            // remove old messages
            while(messages.Count > maxMessages){
                LoggerMessage messageToRemove = messages.First.Value;
                messages.RemoveFirst();

                Destroy(messageToRemove.gameObject);
            }

            if(scrollToBottom){
                LayoutRebuilder.ForceRebuildLayoutImmediate(contentRectTransform);
                scrollView.verticalNormalizedPosition = 0f;
            }
        }


        void OnApplicationQuit(){
            Application.logMessageReceived -= OnLogMessageReceived;
            settingsManager.SetProperty(BoolSettingsProperty.DEBUG__SHOW_LOGGER, gameObject.activeSelf);
        }


        void OnEnable(){
            Application.logMessageReceived += OnLogMessageReceived;
            settingsManager.SetProperty(BoolSettingsProperty.DEBUG__SHOW_LOGGER, true);
        }


        void OnDisable(){
            Application.logMessageReceived -= OnLogMessageReceived;
            settingsManager.SetProperty(BoolSettingsProperty.DEBUG__SHOW_LOGGER, false);
        }
    }

} //! namespace ryabomar.debug