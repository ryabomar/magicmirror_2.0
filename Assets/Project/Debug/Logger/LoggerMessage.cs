﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ryabomar.UI {
    /// <summary>
    /// single message for Logger
    /// </summary>
    public class LoggerMessage : MonoBehaviour {
        
        [SerializeField] Text textElement;

        public string text { 
            get { return textElement.text; }
            set { textElement.text = value; }
        }
        
    }

} //! namespace ryabomar.UI