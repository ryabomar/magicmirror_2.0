﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ryabomar.UI {

    public class DebugDressingWidget : MonoBehaviour {

        [SerializeField] DressingManagerHandle dressingManagerHandle;

        [SerializeField] Image indicator_HEADGEAR;
        [SerializeField] Image indicator_FEETWEAR;
        [SerializeField] Image indicator_GLOVES;
        [SerializeField] Image indicator_UNDERWEAR_BOTTOM;
        [SerializeField] Image indicator_UNDERWEAR_TOP;
        [SerializeField] Image indicator_TOPWEAR_BOTTOM;
        [SerializeField] Image indicator_TOPWEAR_TOP;


        void Start(){
            DisplayOutfitSlots(Outfit.Category.NONE);
            dressingManagerHandle.activeUserOutfitSetChanged?.AddListener(DisplayOutfitSlots);
        }

        public void DisplayOutfitSlots(Outfit.Category category){
            indicator_HEADGEAR.gameObject.SetActive(category.HasFlag(Outfit.Category.HEADGEAR));
            indicator_FEETWEAR.gameObject.SetActive(category.HasFlag(Outfit.Category.FEETWEAR));
            indicator_GLOVES.gameObject.SetActive(category.HasFlag(Outfit.Category.GLOVES));
            indicator_UNDERWEAR_BOTTOM.gameObject.SetActive(category.HasFlag(Outfit.Category.UNDERWEAR_BOTTOM));
            indicator_UNDERWEAR_TOP.gameObject.SetActive(category.HasFlag(Outfit.Category.UNDERWEAR_TOP));
            indicator_TOPWEAR_BOTTOM.gameObject.SetActive(category.HasFlag(Outfit.Category.TOPWEAR_BOTTOM));
            indicator_TOPWEAR_TOP.gameObject.SetActive(category.HasFlag(Outfit.Category.TOPWEAR_TOP));
        }
    }

} //! namespace ryabomar.UI