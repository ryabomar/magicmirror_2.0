﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace ryabomar.UI {
    /// <summary>
    /// widget with control of debug functionality
    /// </summary>
    public class DebugSettingsWidget : MonoBehaviour {
        [SerializeField] UserSettingsManager settingsManager;
        [SerializeField] SensorSwitcherHandle sensorSwitcherHandle;
        [SerializeField] DressingManagerHandle dressingManagerHandle;
        [SerializeField] WireframeBodySpawnerHandle wireframeBodySpawnerHandle;
        [SerializeField] Toggle isVireframeBodiesVisible_toggle;

        [SerializeField] GameObject logger;
        [SerializeField] Toggle logger_toggle;
        [SerializeField] Toggle bodyIsVisible_toggle;

        [SerializeField] Text fpsIndicator;

        [SerializeField] Slider bodyScaleSlider;
        [SerializeField] Slider hipVerticalOffsetSlider;

        float _lastTimeMesure;
        
        void Start() {
            isVireframeBodiesVisible_toggle.SetIsOnWithoutNotify(wireframeBodySpawnerHandle.IsWireframeBodiesVisible());
            isVireframeBodiesVisible_toggle.onValueChanged.AddListener(SetWireframeBodiesVisible);

            logger_toggle.SetIsOnWithoutNotify(logger.activeSelf);
            logger_toggle.onValueChanged.AddListener(logger.SetActive);

            bodyIsVisible_toggle.SetIsOnWithoutNotify(settingsManager.GetPropertyValue(BoolSettingsProperty.DEBUG__USER_BODY_VISIBLE));


            _lastTimeMesure = Time.realtimeSinceStartup;

            { // body scale slider
                float scale = bodyScaleSlider.value;
                dressingManagerHandle.SetMannequinScale(new Vector3(scale, scale, scale));
                bodyScaleSlider.onValueChanged.AddListener((value) => {
                    dressingManagerHandle.SetMannequinScale(new Vector3(value, value, value));
                });
            }


            { // hip joint offset slider
                float hipVerticalOffset = hipVerticalOffsetSlider.value;
                sensorSwitcherHandle.SetJointOffset(Joint.Type.PELVIS, new Vector3(0, hipVerticalOffset, 0));

                hipVerticalOffsetSlider.onValueChanged.AddListener((value) => {
                    sensorSwitcherHandle.SetJointOffset(Joint.Type.PELVIS, new Vector3(0, value, 0));
                });
            }
        }

        public void SetWireframeBodiesVisible(bool value){
            wireframeBodySpawnerHandle.SetWireframeBodiesVisible(value);
        }


        void Update(){
            _UpdateFPS();
        }


       
        void _UpdateFPS(){
            float mesureInterval = 0.5f; // 0.5 sec

            if(Time.realtimeSinceStartup - _lastTimeMesure >= mesureInterval){
                _lastTimeMesure = Time.realtimeSinceStartup;
                fpsIndicator.text = (1.0f / Time.smoothDeltaTime).ToString("00.0");
            }
        }

    }

}// !namespace ryabomar
