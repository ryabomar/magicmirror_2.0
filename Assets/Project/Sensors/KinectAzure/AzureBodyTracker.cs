﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



using Azure = Microsoft.Azure.Kinect;
using Microsoft.Azure.Kinect.BodyTracking;

namespace ryabomar.Azure {

    public class AzureBodyTracker : MonoBehaviour, IBodyTracker {

        public BodyTrackingEvent BodyAppeared_Event    { get; set; } = new BodyTrackingEvent();
        public BodyTrackingEvent BodyDisappeared_Event { get; set; } = new BodyTrackingEvent();
        public BodyTrackingEvent BodyUpdated_Event     { get; set; } = new BodyTrackingEvent();

        Dictionary<ulong, BodyData> _bodyDatas = new Dictionary<ulong, BodyData>();

        BodyTrackingThread _bodyTrackingThread;
        
        HashSet<ulong> trackedBodies = new HashSet<ulong>();
 
        public List<BodyData> GetLastFrameBodies(){
            return new List<BodyData>(_bodyDatas.Values);
        }

        public bool GetBodiIndexData(ref byte[] data){
            if(_bodyTrackingThread != null){
                return _bodyTrackingThread.GetBodiIndexData(ref data);
            }

            return false;
        }


        void OnEnable(){
            _bodyTrackingThread = new BodyTrackingThread(GetComponent<AzureDevice>().GetDevice());
        }


        void OnDisable(){
            _bodyTrackingThread?.Dispose();
            _bodyTrackingThread = null;
        }

        
        void Update(){
            if(_bodyTrackingThread == null) return;

            
            BodyData[] lastData = null;
            if (_bodyTrackingThread.GetData(ref lastData)){

                HashSet<ulong> updatedBodiesIds = new HashSet<ulong>();

                foreach (BodyData bodyData in lastData)
                {
                    bool isNewBody = !_bodyDatas.ContainsKey(bodyData.id);

                    _bodyDatas[bodyData.id] = bodyData;

                    if (isNewBody)
                    {
                        BodyAppeared_Event.Invoke(bodyData);
                    }

                    updatedBodiesIds.Add(bodyData.id);
                    BodyUpdated_Event.Invoke(bodyData);
                }

                List<ulong> dissapearedBodiesIds = new List<ulong>();
                foreach (var entry in _bodyDatas)
                {
                    ulong id = entry.Key;

                    if (!updatedBodiesIds.Contains(id))
                    {
                        dissapearedBodiesIds.Add(id);
                    }
                }

                foreach (ulong id in dissapearedBodiesIds)
                {
                    BodyDisappeared_Event.Invoke(_bodyDatas[id]);
                    _bodyDatas.Remove(id);
                }
            }
        }

    }
} //! namespace ryabomar