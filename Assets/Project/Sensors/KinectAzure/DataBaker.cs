﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AzureShort3  = Microsoft.Azure.Kinect.Sensor.Short3;

namespace ryabomar.Azure {
    public class DataBaker: IDisposable {
        public Texture bakedPositions      { get => _renderTexture; }

        RenderTexture _renderTexture;
        ComputeBuffer _pointsPositionsBuffer;
        ComputeShader _computeShader;
        int _pointsPositionsBaker_kernel;


        public DataBaker(int width, int height){
            _renderTexture = new RenderTexture(width, height, 0, RenderTextureFormat.ARGBFloat);
            _renderTexture.enableRandomWrite = true;
            _renderTexture.Create();

            _pointsPositionsBuffer = new ComputeBuffer(width * height, sizeof(short) * 3);

            _InitShader(new Vector2Int(width, height));
        }


        public void Bake(AzureShort3[] data){

        }


        public void Dispose(){
            _renderTexture = null;
            
            _pointsPositionsBuffer?.Release();
            _pointsPositionsBuffer = null;
            
            _computeShader = null; 
            _pointsPositionsBaker_kernel = -1;
        }


        public void BakePointsPositions(AzureShort3[] points) {
            _pointsPositionsBuffer?.SetData(points);
            _computeShader?.Dispatch(_pointsPositionsBaker_kernel, 64, 1, 1);
        }

        void _InitShader(Vector2Int depthCameraResolution) {
            string shaderName = "KinectV2/DataBaker_ComputeShader2";
            try {
                _computeShader = Resources.Load<ComputeShader>(shaderName);
            } catch (Exception e) {
                Dispose();
                throw new Exception("Could not load resource: " + shaderName, e);
            }

            _pointsPositionsBaker_kernel = _computeShader.FindKernel("BakePositions2");

            _computeShader.SetBuffer(_pointsPositionsBaker_kernel, "pointsPositions_buffer", _pointsPositionsBuffer);

            _computeShader.SetTexture(_pointsPositionsBaker_kernel, "pointsPositions_Texture", _renderTexture, 0);

            _computeShader.SetInt("nPointsHorizontal", depthCameraResolution.x);
            _computeShader.SetInt("nPointsVertical", depthCameraResolution.y);
        }
    }
} //! namespace ryabomar.Azure {