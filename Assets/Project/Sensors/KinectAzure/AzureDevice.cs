﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Microsoft.Azure.Kinect.Sensor;
using Microsoft.Azure.Kinect.BodyTracking;

namespace ryabomar.Azure {
    public class AzureDevice : MonoBehaviour {
        Device _device = null;

        public Device GetDevice() {
            if(Device.GetInstalledCount() == 0){
                Debug.Log("[Kinect.Azure] no connected device found");
                return null;
            }

            if(_device == null && Device.GetInstalledCount() != 0) {
                _InitDevice();
            }

            return _device;
        }


        void _InitDevice(){
            _device = Device.Open(0);
            _device.StartCameras(new DeviceConfiguration(){
                CameraFPS       = FPS.FPS30,
                ColorFormat     = ImageFormat.ColorBGRA32,
                ColorResolution = ColorResolution.R720p,
                WiredSyncMode   = WiredSyncMode.Standalone,
                DepthMode       = DepthMode.NFOV_Unbinned,
                SynchronizedImagesOnly = true,
            });           
            Debug.Log("[Kinect.Azure] device is on");
        }



        public void OnEnable(){
            //GetDevice();
        }


        public void OnDisable(){
            // _device.StopCameras();
            // _device.Dispose();
            // _device = null;
        }

        public void OnApplicationQuit(){
            _device?.StopCameras();
            _device?.Dispose();
            _device = null;
        }
    }

} //! namespace ryabomar.Azure