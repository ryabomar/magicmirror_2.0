﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AzureImage   = Microsoft.Azure.Kinect.Sensor.Image;
using AzureBGRA    = Microsoft.Azure.Kinect.Sensor.BGRA;
using AzureShort3  = Microsoft.Azure.Kinect.Sensor.Short3;
using AzureBody    = Microsoft.Azure.Kinect.BodyTracking.Body;
using AzureJointId = Microsoft.Azure.Kinect.BodyTracking.JointId;
using AzureSkeleton = Microsoft.Azure.Kinect.BodyTracking.Skeleton;
using AzureVector3      = System.Numerics.Vector3;
using AzureQuaternion   = System.Numerics.Quaternion;

//using Microsoft.Azure.Kinect.Sensor;

namespace ryabomar.Azure {

    public static class Utils {

        public static void Swap<T>(ref T lhr, ref T rhr){
                var tmp = lhr;
                lhr = rhr;
                rhr = tmp;
        }

        // internal static void _BakeColor(AzureImage azureImage, Texture2D texture){
        //     AzureBGRA[] data = azureImage.GetPixels<AzureBGRA>().ToArray();

        //     unsafe {
        //         fixed(AzureBGRA* ptr = data) {
        //             IntPtr intPtr = (IntPtr)ptr;
        //             texture.LoadRawTextureData(intPtr, sizeof(AzureBGRA) * data.Length);
        //         }
        //     }
        //     texture.Apply();
        // }


        // internal static void _BakePositions(AzureImage coordinatesImage, Texture2D positionTexture){
        //     // Image positionImage = _transformation.DepthImageToPointCloud(capture.Depth);
        //     AzureShort3[] positions = coordinatesImage.GetPixels<AzureShort3>().ToArray();

        //     Vector4[] positionsData = new Vector4[positions.Length];

        //     for (int i = 0; i < positions.Length; i++){

        //         positionsData[i] = new Vector4(
        //             positions[i].X * 0.001f,
        //             positions[i].Y * 0.001f * (-1),
        //             positions[i].Z * 0.001f,
        //             0.0f
        //         );
                
        //     }

        //     unsafe {
        //         fixed(Vector4* ptr = positionsData) {
        //             IntPtr intPtr = (IntPtr)ptr;
        //             positionTexture.LoadRawTextureData(intPtr, sizeof(Vector4) * positionsData.Length);
        //         }
        //     }
        //     positionTexture.Apply();
        // }


        // internal static void _BakeUVs(AzureImage azureImage, Texture2D texture){
            
        //     AzureShort3[] uvs = azureImage.GetPixels<AzureShort3>().ToArray();

        //     Vector2[] uvData = new Vector2[uvs.Length];

        //     for (int i = 0; i < uvs.Length; i++){

        //         uvData[i] = new Vector4(
        //             uvs[i].X,
        //             uvs[i].Y
        //         );
                
        //     }


        //     unsafe {
        //         fixed(Vector2* ptr = uvData) {
        //             IntPtr intPtr = (IntPtr)ptr;
        //             texture.LoadRawTextureData(intPtr, sizeof(Vector2) * uvData.Length);
        //         }
        //     }
        //     texture.Apply();
        // }


        // internal static void _BakeBodyIndecies(AzureImage azureImage, Texture2D texture){
        //     byte[] data = azureImage.GetPixels<byte>().ToArray();
        //     texture.LoadRawTextureData(data);
        //     texture.Apply();
        // }


        // internal static void _BakeDepth(AzureImage azureImage, Texture2D texture){
        //     ushort[] data = azureImage.GetPixels<ushort>().ToArray();

        //     unsafe {
        //         fixed(ushort* ptr = data) {
        //             IntPtr intPtr = (IntPtr)ptr;
        //             texture.LoadRawTextureData(intPtr, sizeof(ushort) * data.Length);
        //         }
        //     }
        //     texture.Apply();
        // }

        

        internal static BodyData BodyDataFromAzureBody(AzureBody azureBody){
            Joint[] joints = new Joint[Enum.GetNames(typeof(Joint.Type)).Length - 1]; // exclude Joint.Type.NONE

            for (int jointId = 0; jointId < AzureSkeleton.JointCount; jointId++){
                Joint.Type jointType = azureJointIdToJointType(jointId);

                joints[(int)jointType].position    = AzureVector3ToUnityVector3(azureBody.Skeleton.GetJoint(jointId).Position / 1000.0f);     
                joints[(int)jointType].position.y *= -1;
                joints[(int)jointType].orientation = AzureQuaternionToUnityQuaternion(azureBody.Skeleton.GetJoint(jointId).Quaternion);

                joints[(int)jointType].trackingState = Joint.TrackingState.TRACKED;

                joints[(int)jointType].type = jointType;
                joints[(int)jointType].isSupported = IsJointSupported(jointType);
            }

            return new BodyData(azureBody.Id, true, joints);
        }


        static bool IsJointSupported(Joint.Type jointType) {
            return true; // all supported
        }



        static Joint.Type azureJointIdToJointType(int azureJointId){
            Joint.Type[] LOOKUP_TABLE = {
                /*  0	PELVIS				*/ Joint.Type.PELVIS,
                /*  1	SPINE_NAVAL			*/ Joint.Type.SPINE_NAVAL,
                /*  2	SPINE_CHEST			*/ Joint.Type.SPINE_CHEST,
                /*  3	NECK				*/ Joint.Type.NECK,
                /*  4	CLAVICLE_LEFT		*/ Joint.Type.CLAVICLE_LEFT,
                /*  5	SHOULDER_LEFT		*/ Joint.Type.SHOULDER_LEFT,
                /*  6	ELBOW_LEFT			*/ Joint.Type.ELBOW_LEFT,
                /*  7	WRIST_LEFT			*/ Joint.Type.WRIST_LEFT,
                /*  8	HAND_LEFT			*/ Joint.Type.HAND_LEFT,
                /*  9	HANDTIP_LEFT		*/ Joint.Type.HANDTIP_LEFT,
                /*  10	THUMB_LEFT			*/ Joint.Type.THUMB_LEFT,
                /*  11	CLAVICLE_RIGHT		*/ Joint.Type.CLAVICLE_RIGHT,
                /*  12	SHOULDER_RIGHT		*/ Joint.Type.SHOULDER_RIGHT,
                /*  13	ELBOW_RIGHT			*/ Joint.Type.ELBOW_RIGHT,
                /*  14	WRIST_RIGHT			*/ Joint.Type.WRIST_RIGHT,
                /*  15	HAND_RIGHT			*/ Joint.Type.HAND_RIGHT,
                /*  16	HANDTIP_RIGHT		*/ Joint.Type.HANDTIP_RIGHT,
                /*  17	THUMB_RIGHT			*/ Joint.Type.THUMB_RIGHT,
                /*  18	HIP_LEFT			*/ Joint.Type.HIP_LEFT,
                /*  19	KNEE_LEFT			*/ Joint.Type.KNEE_LEFT,
                /*  20	ANKLE_LEFT			*/ Joint.Type.ANKLE_LEFT,
                /*  21	FOOT_LEFT			*/ Joint.Type.FOOT_LEFT,
                /*  22	HIP_RIGHT			*/ Joint.Type.HIP_RIGHT,
                /*  23	KNEE_RIGHT			*/ Joint.Type.KNEE_RIGHT,
                /*  24	ANKLE_RIGHT			*/ Joint.Type.ANKLE_RIGHT,
                /*  25	FOOT_RIGHT			*/ Joint.Type.FOOT_RIGHT,
                /*  26	HEAD				*/ Joint.Type.HEAD,
                /*  27	NOSE				*/ Joint.Type.NOSE,
                /*  28	EYE_LEFT			*/ Joint.Type.EYE_LEFT,
                /*  29	EAR_LEFT			*/ Joint.Type.EAR_LEFT,
                /*  30	EYE_RIGHT			*/ Joint.Type.EYE_RIGHT,
                /*  31	EAR_RIGHT			*/ Joint.Type.EAR_RIGHT,
            };

            return LOOKUP_TABLE[azureJointId];
        }


        static Vector3 AzureVector3ToUnityVector3(AzureVector3 value) {
            return new Vector3(value.X, value.Y, value.Z);
        }

        static Quaternion AzureQuaternionToUnityQuaternion(AzureQuaternion value) {
            return new Quaternion(value.X, value.Y, value.Z, value.W);
        }

    }

} //! namespace ryabomar.Azure {
