﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Threading;
using System.Threading.Tasks;

using Microsoft.Azure.Kinect.BodyTracking;
using Microsoft.Azure.Kinect.Sensor;

namespace ryabomar.Azure {

    public class BodyTrackingThread : IDisposable {
        
        public bool isRunning { get; private set; } = false;
        
        object _lock = new object();

        object _bodyIndexDataLock = new object();
        byte[] _bodyIndexData;
        bool _isBodyIndexLasted = false;

        CancellationTokenSource _cancellationSource;
        CancellationToken       _token;

        BodyData[] bodyDatas;

        bool _isLatest = false;


        public BodyTrackingThread(Device device) {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.quitting += OnEditorClose;
            #endif

            _cancellationSource = new CancellationTokenSource();
            _token = _cancellationSource.Token;
            
            if(device == null) {
                Debug.LogError("[Kinect.Azure] no device found");
            } else {
                StartBackgroundThread(device, _token);
            }
        }


        public bool GetBodiIndexData(ref byte[] data){
            lock (_bodyIndexDataLock){
                // swap
                Utils.Swap(ref data, ref _bodyIndexData);
                
                bool result = _isBodyIndexLasted;
                _isBodyIndexLasted = false;

                return result;
            }
        }


        public void SetBodiIndexData(ref byte[] data){
            Utils.Swap(ref data, ref _bodyIndexData);
            _isBodyIndexLasted = true;
        }


        TimeSpan timeout = TimeSpan.FromMilliseconds(50);
        public bool GetData(ref BodyData[] data) {

            bool result = _isLatest;
            bool lockTaken = false;
            try {
                Monitor.TryEnter(_lock, timeout, ref lockTaken);
                if (lockTaken) {
                    // The critical section.
                    Utils.Swap(ref data, ref bodyDatas);
                    result = _isLatest;
                    _isLatest = false;
                } 
            } finally {
                if (lockTaken) {
                    Monitor.Exit(_lock);
                }
            }

            return result;

            // lock (_lock){
            //     // swap
            //     Utils.Swap(ref data, ref bodyDatas);
                
            //     bool result = _isLatest;
            //     _isLatest = false;

            //     return result;
            // }
        }


        public void SetData(ref BodyData[] newData) {
            lock (_lock){
                // swap
                // var temp = data;
                // data = bodyDatas;
                // bodyDatas = temp;
                Utils.Swap(ref newData, ref bodyDatas);

                _isLatest = true;
            }
        }


        public void Dispose() {
            #if UNITY_EDITOR
                UnityEditor.EditorApplication.quitting -= OnEditorClose;
            #endif

            if(_cancellationSource != null){
                _cancellationSource?.Cancel();

                _cancellationSource?.Dispose();
                _cancellationSource = null;
                Debug.Log("[Kinect.Azure] background thread stopped");
            }
            
        }


        void OnEditorClose(){
            Dispose();
        }


        void StartBackgroundThread(Device device, CancellationToken token){
            
            Task.Run(() => {
                try {
                    /*

                        using (Device device = Device.Open(id)){
                            Debug.Log("[Kinect.Azure] BodyTracker: openning device #" + id);
                            device.StartCameras(new DeviceConfiguration()
                            {
                                CameraFPS = FPS.FPS30,
                                ColorResolution = ColorResolution.Off,
                                WiredSyncMode = WiredSyncMode.Standalone,
                                DepthMode = DepthMode.NFOV_Unbinned,
                            });
                        }
                    */

                    var calibration = device.GetCalibration();

                    TrackerConfiguration trackerConf = new TrackerConfiguration() {
                        ProcessingMode    = TrackerProcessingMode.Cpu, // Cpu Cuda 
                        SensorOrientation = SensorOrientation.Default
                    };

                    
                    using (Tracker tracker = Tracker.Create(calibration, trackerConf)){ 
                        Debug.Log("[Kinect.Azure] BodyTracker created");

                        // loop
                        while (!token.IsCancellationRequested)
                        {
                            using (Capture sensorCapture = device.GetCapture()) {
                                tracker.EnqueueCapture(sensorCapture);
                            }


                            using (Frame frame = tracker.PopResult(TimeSpan.Zero, throwOnTimeout: false)){
                                //Debug.Log("in body frame");
                                if (frame == null)
                                {
                                    Debug.Log("[Kinect.Azure] BodyTracker: timeout");
                                }
                                else
                                {   

                                    // BODY DATA
                                    isRunning = true;

                                    BodyData[] _tmpBodyDatas = new BodyData[frame.NumberOfBodies];

                                    //Debug.Log("[Kinect.Azure] BodyTracker: got " + _tmpBodyDatas.Length + " bodies");

                                    for (uint i = 0; i < frame.NumberOfBodies; i++)
                                    {
                                        _tmpBodyDatas[i] = Utils.BodyDataFromAzureBody(frame.GetBody(i));
                                    }

                                    // Update data variable that is being read in the UI thread.
                                    SetData(ref _tmpBodyDatas);


                                    // BODY INDECIES
                                    byte[] bodyIdxMap = frame.BodyIndexMap.Memory.ToArray();
                                    SetBodiIndexData(ref bodyIdxMap);
                                }

                            }


                        }
                    }
                } catch (Exception e) {
                    Debug.LogError("[Kinect.Azure] body tracker: exception" + e);
                //    token.ThrowIfCancellationRequested();
                } //! catch
            });

            Debug.Log("[Kinect.Azure] background thread started");
        }
    }

} //! namespace ryabomar.Azure