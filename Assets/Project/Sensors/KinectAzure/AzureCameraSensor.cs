﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Threading.Tasks;
using Azure = Microsoft.Azure.Kinect;
using AzureImage = Microsoft.Azure.Kinect.Sensor.Image;
using Microsoft.Azure.Kinect.Sensor;
using AzureBGRA = Microsoft.Azure.Kinect.Sensor.BGRA;

namespace ryabomar.Azure {


    public class AzureCameraSensor : MonoBehaviour, ICameraSensor {
        public Vector2Int colorResolution    { get => new Vector2Int(color_Texture.width, color_Texture.height); }
        public Vector2Int depthResolution    { get => new Vector2Int(depthData_Texture.width, depthData_Texture.height); }

        public Texture color_Texture         { get => _colorCameraTexture;      }
        public Texture pointsPos_Texture     { get => _pointsPositions_Texture;  }
        // public Texture pointsColorUV_Texture { get => _pointsUVs_Texture;        }

        

        public Texture pointsColors_Texture { get => _pointColor_Texture;        }
        public Texture bodyIndex_Texture     { get => _bodyIndecies_Texture;     }
        public Texture depthData_Texture     { get => _depthData_Texture;        }

        Texture2D _colorCameraTexture;
        Texture2D _pointsPositions_Texture ;
        Texture2D _pointsUVs_Texture       ;
        Texture2D _bodyIndecies_Texture    ;
        Texture2D _depthData_Texture       ;

        Texture2D _pointColor_Texture;

        Device _device;
        //Transformation _transformation;

        CancellationTokenSource _cancellation;
        CancellationToken _token;

        object _lock = new object();

        AzureDevice _azureDevice;
        AzureBodyTracker _bodyTracker;

      

        class DataPackage {
            public BGRA[] color;
            public ushort[] depth;
            public Vector4[] positions;

            public BGRA[] pointColor;
            
            //uv

            // bodyIdxs
        }

        DataPackage _dataPackage;


        bool _isLatest = false;

        void Awake() {
            _azureDevice = GetComponent<AzureDevice>();
            _bodyTracker = GetComponent<AzureBodyTracker>();

            //_device = _azureDevice.GetDevice();

            _colorCameraTexture      = new Texture2D(1920, 1080, TextureFormat.BGRA32, false);
            _pointsPositions_Texture = new Texture2D(512,  512,  TextureFormat.RGBAFloat, false);
            _pointsUVs_Texture       = new Texture2D(512,  512,  TextureFormat.RGFloat, false);
            _bodyIndecies_Texture    = new Texture2D(512,  512,  TextureFormat.R8, false);
            _depthData_Texture       = new Texture2D(512,  512,  TextureFormat.R16, false);
            _pointColor_Texture      = new Texture2D(512,  512,  TextureFormat.BGRA32, false);
        }

        void Start(){
            _bodyTracker = GetComponent<AzureBodyTracker>();
        }


        void OnEnable(){
            _device = _azureDevice.GetDevice();

            if(_device != null){
                _InitTextures(
                    _device.GetCalibration().DepthCameraCalibration.ResolutionWidth, 
                    _device.GetCalibration().DepthCameraCalibration.ResolutionHeight,
                    _device.GetCalibration().ColorCameraCalibration.ResolutionWidth,
                    _device.GetCalibration().ColorCameraCalibration.ResolutionHeight
                ); 

                _cancellation = new CancellationTokenSource();
                _token = _cancellation.Token;



                _StartSensorThread(_token, _device);
                Debug.Log("[Kinect_Azure:Sensor] enabled");
            }


            // if(Device.GetInstalledCount() == 0){
            //     Debug.Log("[Kinect_Azure:Sensor] no divece found");
            // } else {
            //     _device = Device.Open(0);
            //     _device.StartCameras(new DeviceConfiguration{
            //                                 ColorFormat     = ImageFormat.ColorBGRA32,
            //                                 ColorResolution = ColorResolution.R720p,
            //                                 DepthMode       = DepthMode.NFOV_Unbinned,
            //                                 CameraFPS       = FPS.FPS30,
            //                                 SynchronizedImagesOnly = true,
            //                             });

            //     _transformation = _device.GetCalibration().CreateTransformation();

    
            //     _InitTextures(
            //                 _device.GetCalibration().DepthCameraCalibration.ResolutionWidth, 
            //                 _device.GetCalibration().DepthCameraCalibration.ResolutionHeight,
            //                 _device.GetCalibration().ColorCameraCalibration.ResolutionWidth,
            //                 _device.GetCalibration().ColorCameraCalibration.ResolutionHeight
            //             ); 

    
            //     _cancellation = new CancellationTokenSource();
            //     _token = _cancellation.Token;


            //     _StartSensor(_token);
            //     Debug.Log("[Kinect_Azure:Sensor] enabled");
            // }
        }



        void OnDisable(){
            _StopSensorThread();
            // _device?.Dispose();
            Debug.Log("[Kinect_Azure:Sensor] disabled");
        }


        void _InitTextures(int depthWidth, int depthHeight, int colorWidth, int colorHeight) {
            _colorCameraTexture      = new Texture2D(colorWidth, colorHeight, TextureFormat.BGRA32,     false);
            _pointsPositions_Texture = new Texture2D(depthWidth, depthHeight, TextureFormat.RGBAFloat,  false);
            _pointsUVs_Texture       = new Texture2D(depthWidth, depthHeight, TextureFormat.RGFloat,    false);
            _bodyIndecies_Texture    = new Texture2D(depthWidth, depthHeight, TextureFormat.R8,         false);
            _depthData_Texture       = new Texture2D(depthWidth, depthHeight, TextureFormat.R16,        false);
            _pointColor_Texture      = new Texture2D(depthWidth, depthHeight, TextureFormat.BGRA32,     false);
        }


        void Update(){

            if(_device == null) {
                return;
            }
            


            DataPackage dataPackage = new DataPackage();
            if(_GetData(ref dataPackage)){
                unsafe {
                    // color
                    fixed(AzureBGRA* ptr = dataPackage.color) {
                        IntPtr intPtr = (IntPtr)ptr;
                        _colorCameraTexture.LoadRawTextureData(intPtr, sizeof(AzureBGRA) * dataPackage.color.Length);
                    }

                    // depth
                    fixed(ushort* ptr = dataPackage.depth) {
                        IntPtr intPtr = (IntPtr)ptr;
                        _depthData_Texture.LoadRawTextureData(intPtr, sizeof(ushort) * dataPackage.depth.Length);
                    }

                    // positions
                                
                    fixed(Vector4* ptr = dataPackage.positions) {
                        IntPtr intPtr = (IntPtr)ptr;
                        _pointsPositions_Texture.LoadRawTextureData(intPtr, sizeof(Vector4) * dataPackage.positions.Length);
                    }
                    
                    
                    // uv

                    // body indecies

                    // points colors
                    fixed(AzureBGRA* ptr = dataPackage.pointColor) {
                        IntPtr intPtr = (IntPtr)ptr;
                        _pointColor_Texture.LoadRawTextureData(intPtr, sizeof(AzureBGRA) * dataPackage.pointColor.Length);
                    }
                    
                }

                _colorCameraTexture.Apply();
                _pointsPositions_Texture.Apply();
                _depthData_Texture.Apply();
                _pointColor_Texture.Apply();


                // body indecies
                byte[] bodyIndexData = null;
                if(_bodyTracker.GetBodiIndexData(ref bodyIndexData)){
                    _bodyIndecies_Texture.LoadRawTextureData(bodyIndexData);
                    _bodyIndecies_Texture.Apply();
                }
            }
        }


        bool _GetData(ref DataPackage dataPackage) {
            lock(_lock){
                Utils.Swap(ref dataPackage, ref _dataPackage);

                bool res = _isLatest;
                _isLatest = false;

                return res;
            }
        }



        void _SetData(ref DataPackage dataPackage){
            lock(_lock){
                Utils.Swap(ref dataPackage, ref _dataPackage);

                _isLatest = true;
            }
        }


        void _StopSensorThread(){
            _cancellation?.Cancel();
            _cancellation?.Dispose();
            _cancellation = null;
        }


        void _StartSensorThread(CancellationToken token, Device device) {
            Transformation transformation = _device.GetCalibration().CreateTransformation();

            Task.Run(() => {
                try {
                    // start loop
                    while(true){
                       
                        token.ThrowIfCancellationRequested();
                        using (Capture capture = device.GetCapture()){
                            //Debug.Log("1");
                            DataPackage dataPackage = new DataPackage();
                            

                            //color
                            dataPackage.color = capture.Color.GetPixels<BGRA>().ToArray();

                            // depth
                            //dataPackage.depth = transformation.ColorImageToDepthCamera(capture).Memory.ToArray();
                            dataPackage.depth = capture.Depth.GetPixels<ushort>().ToArray();

                            
                            //Debug.Log("2");
                            // positions
                            Short3[]  positionsBuff = transformation.DepthImageToPointCloud(capture.Depth).GetPixels<Short3>().ToArray();
                            dataPackage.positions = new Vector4[positionsBuff.Length];
                            

                            for (int i = 0; i < positionsBuff.Length; i++){
                                dataPackage.positions[i] = new Vector4(
                                    // mm -> meters
                                    positionsBuff[i].X * 0.001f,
                                    positionsBuff[i].Y * 0.001f * (-1), 
                                    positionsBuff[i].Z * 0.001f,
                                    0.0f
                                );
                            }

                            // uv
                            // TODO


                            // body index
                            // TODO

                            // points colors
                            dataPackage.pointColor = transformation.ColorImageToDepthCamera(capture).GetPixels<BGRA>().ToArray();


                            _SetData(ref dataPackage);
                            // byte[] byteArray = 
                            // _colorCameraTexture.LoadRawTextureData(byteArray);
                            // Debug.Log("3");
                            // _colorCameraTexture.Apply();
                            // Debug.Log("4");
                            //BGRA[] data = capture.Color.GetPixels<AzureBGRA>().ToArray();
                            //Debug.Log(data.Length);

                            //Utils._BakeColor    (_transformation.ColorImageToDepthCamera(capture),          _colorCameraTexture);
                            //Utils._BakePositions(_transformation.DepthImageToPointCloud(capture.Depth),     _pointsPositions_Texture);
                            //Utils._BakeUVs      (_transformation.DepthImageToColorCamera(capture),          _pointsUVs_Texture);
                            ////Utils._BakeBodyIndecies() // TODO
                            //Utils._BakeDepth    (capture.Depth,                                             _depthData_Texture);


                            
                        }
                    }
                    
                } catch (Exception e) {
                    Debug.Log("[Kinect.Azure] camera: exception: " + e.Message);
                    token.ThrowIfCancellationRequested();
                }
            });

        }



        void OnApplicationQuit(){
            OnDisable();
            _cancellation?.Dispose();
        }
        
    } //! class AzureCameraSensor

} //! namespace ryabomar.Azure
