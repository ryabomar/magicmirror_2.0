﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar.FakeDevice {

    /// <summary>
    /// outputs static textures (for debug purpose)
    /// </summary>
    public class FakeCameraSensor : MonoBehaviour, ICameraSensor {
        public Texture2D staticColorTexture;
        public Texture2D staticDepthTexture;
        public Texture2D staticPositionsTexture;
        public Texture2D staticUvTexture;
        public Texture2D staticBodyIndeciesTexture;

        public Vector2Int colorResolution { get => new Vector2Int(staticColorTexture.width, staticColorTexture.height); }
        public Vector2Int depthResolution { get => new Vector2Int(staticDepthTexture.width, staticDepthTexture.height); }


        public Texture color_Texture           { get => staticColorTexture; }
        public Texture bodyIndex_Texture       { get => staticDepthTexture; }
        public Texture pointsPos_Texture       { get => staticPositionsTexture; }
        public Texture pointsColors_Texture   { get => staticUvTexture; }      
        public Texture depthData_Texture       { get => staticBodyIndeciesTexture; }

    } //! class FakeCameraSensor

} //! namespace ryabomar.FakeDevice
