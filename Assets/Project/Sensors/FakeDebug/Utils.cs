﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar.FakeDevice {
    public class Utils {

        static public BodyData MakeBodyDataFromGameObject(ulong id, GameObject root, bool disableJointObjects = false){

            Joint[] joints = new Joint[Enum.GetNames(typeof(Joint.Type)).Length - 1];
                
            foreach(Joint.Type jointType in Enum.GetValues(typeof(Joint.Type))){
                if(jointType == Joint.Type.NONE) continue;

                Transform tr = root.transform.Find(jointType.ToString());
                if(tr != null) {
                    joints[(int)jointType].position = tr.position;
                    joints[(int)jointType].orientation = tr.rotation;
                    joints[(int)jointType].isSupported = true;

                    tr.gameObject.SetActive(!disableJointObjects);

                    //Debug.Log("joint " + jointType.ToString() + " at " + tr.position);
                } else {
                    Debug.Log("Not found: " + jointType.ToString());
                    return null;
                }
            }

            return new BodyData(id, true, joints);
        }

        // static public BodyData MakeTPoseBodyData(ulong id) {
        //     return MakeTPoseBodyData(id, Vector3.zero);
        // }


        // static public BodyData MakeTPoseBodyData(ulong id, Vector3 rootPosition) {
        //     /*
        //        __°__    
        //          |      
        //         | |     
        //         | |
        //     */

        //     Joint[] joints = new Joint[Enum.GetNames(typeof(Joint.Type)).Length - 1]; // exclude Joint.Type.NONE
            
        //     joints[(int)Joint.Type.PELVIS].position         = rootPosition + new Vector3(0,0,1.1f);
        //     joints[(int)Joint.Type.SPINE_NAVAL].position    = rootPosition + new Vector3(0,0,1.34f);
        //     joints[(int)Joint.Type.SPINE_CHEST].position    = rootPosition + new Vector3(0,0,1.50f);
        //     joints[(int)Joint.Type.NECK].position           = rootPosition + new Vector3(0,0,1.65f);
        //     joints[(int)Joint.Type.CLAVICLE_LEFT].position  = rootPosition + new Vector3(0.15f,0,1.6f);
        //     joints[(int)Joint.Type.SHOULDER_LEFT].position  = rootPosition + new Vector3(0.25f,0,1.6f);
        //     joints[(int)Joint.Type.ELBOW_LEFT].position     = rootPosition + new Vector3(0.5f,0,1.6f);
        //     joints[(int)Joint.Type.WRIST_LEFT].position     = rootPosition + new Vector3(0.75f,0,1.6f);
        //     joints[(int)Joint.Type.HAND_LEFT].position      = rootPosition + new Vector3(0.8f,0,1.6f);
        //     joints[(int)Joint.Type.HANDTIP_LEFT].position   = rootPosition + new Vector3(0.98f,0,1.6f);
        //     joints[(int)Joint.Type.THUMB_LEFT].position     = rootPosition + new Vector3(0.8f,-0.1f,1.6f);
        //     joints[(int)Joint.Type.CLAVICLE_RIGHT].position = rootPosition + new Vector3(-0.15f,0,1.6f);
        //     joints[(int)Joint.Type.SHOULDER_RIGHT].position = rootPosition + new Vector3(-0.25f,0,1.6f);
        //     joints[(int)Joint.Type.ELBOW_RIGHT].position    = rootPosition + new Vector3(-0.5f,0,1.6f);
        //     joints[(int)Joint.Type.WRIST_RIGHT].position    = rootPosition + new Vector3(-0.75f,0,1.6f);
        //     joints[(int)Joint.Type.HAND_RIGHT].position     = rootPosition + new Vector3(-0.8f,0,1.6f);
        //     joints[(int)Joint.Type.HANDTIP_RIGHT].position  = rootPosition + new Vector3(-0.98f,0,1.6f);
        //     joints[(int)Joint.Type.THUMB_RIGHT].position    = rootPosition + new Vector3(-0.8f,0.1f,1.6f);
        //     joints[(int)Joint.Type.HIP_LEFT].position       = rootPosition + new Vector3(0.2f,0,1.1f);
        //     joints[(int)Joint.Type.KNEE_LEFT].position      = rootPosition + new Vector3(0.2f,0,0.55f);
        //     joints[(int)Joint.Type.ANKLE_LEFT].position     = rootPosition + new Vector3(0.2f,0,0.1f);
        //     joints[(int)Joint.Type.FOOT_LEFT].position      = rootPosition + new Vector3(0.2f,0.2f,0);
        //     joints[(int)Joint.Type.HIP_RIGHT].position      = rootPosition + new Vector3(-0.2f,0,1.1f);
        //     joints[(int)Joint.Type.KNEE_RIGHT].position     = rootPosition + new Vector3(-0.2f,0,0.55f);
        //     joints[(int)Joint.Type.ANKLE_RIGHT].position    = rootPosition + new Vector3(-0.2f,0,0.1f);
        //     joints[(int)Joint.Type.FOOT_RIGHT].position     = rootPosition + new Vector3(-0.2f,0.2f,0);
        //     joints[(int)Joint.Type.HEAD].position           = rootPosition + new Vector3(0,0,1.9f);
        //     joints[(int)Joint.Type.NOSE].position           = rootPosition + new Vector3(0,-0.1f,1.8f);
        //     joints[(int)Joint.Type.EYE_LEFT].position       = rootPosition + new Vector3(0.1f,-0.1f,1.85f);
        //     joints[(int)Joint.Type.EAR_LEFT].position       = rootPosition + new Vector3(0.2f,-0.1f,1.85f);
        //     joints[(int)Joint.Type.EYE_RIGHT].position      = rootPosition + new Vector3(-0.1f,-0.1f,1.85f);
        //     joints[(int)Joint.Type.EAR_RIGHT].position      = rootPosition + new Vector3(-0.2f,-0.1f,1.85f);


        //     foreach (Joint.Type type in Enum.GetValues(typeof(Joint.Type))) {
        //         joints[(int)type].isSupported = true;
        //         joints[(int)type].type = type;
        //         joints[(int)type].orientation = Quaternion.identity;
        //     }


        //     BodyData bodyData = new BodyData(id);

        //     return bodyData;
        // }
    }
} //! namespace ryabomar.FakeDevice