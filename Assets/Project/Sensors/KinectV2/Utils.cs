﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Kinect     = Windows.Kinect;
using KinectFace = Microsoft.Kinect.Face;

namespace ryabomar.KinectV2 {
     

    internal static class Utils {

        internal static HashSet<Joint.Type> UNSUPPORTED_JOINTS = new HashSet<Joint.Type> {
            // body
            Joint.Type.CLAVICLE_LEFT,
            Joint.Type.CLAVICLE_RIGHT,
            Joint.Type.HAND_LEFT,
            Joint.Type.HAND_RIGHT,

            // face
            Joint.Type.NOSE,
            Joint.Type.EYE_LEFT,
            Joint.Type.EYE_RIGHT,
            Joint.Type.EAR_LEFT,
            Joint.Type.EAR_RIGHT
        };

        internal static bool IsJointSupported(Joint.Type jointType) {
            return !UNSUPPORTED_JOINTS.Contains(jointType);
        }


        // extension method to convert KincetV2 data
        public static Quaternion ToQuaternion(this Kinect.JointOrientation joint) {
            return new Quaternion(joint.Orientation.X, joint.Orientation.Y, joint.Orientation.Z, joint.Orientation.W);
        }


        // extension method to convert KincetV2 data
        public static Vector3 ToVector3(this Kinect.Joint joint) {
            return new Vector3(joint.Position.X, joint.Position.Y, joint.Position.Z);
        }


        public static BodyData ConstructBodyData(Kinect.Body kinectBody, Quaternion headOrientation) {
            Joint[] joints = new Joint[Enum.GetNames(typeof(Joint.Type)).Length - 1]; // exclude Joint.Type.NONE

            // convert joints
            foreach (Kinect.JointType kinectV2JointType in Enum.GetValues(typeof(Kinect.JointType))) {
                Joint.Type jointType = kinectV2JointType.ToJointType();
                joints[(int)jointType].position    = kinectBody.Joints[kinectV2JointType].ToVector3();
                joints[(int)jointType].orientation = kinectBody.JointOrientations[kinectV2JointType].ToQuaternion();

                joints[(int)jointType].trackingState = kinectBody.Joints[kinectV2JointType].TrackingState.ToJointTrackedState();

                joints[(int)jointType].type = jointType;
                joints[(int)jointType].isSupported = IsJointSupported(jointType);
            }

            joints[(int)Joint.Type.HEAD].orientation = headOrientation;


            // fix unsupported joints orientation
            foreach (Joint.Type jointType in UNSUPPORTED_JOINTS) {
                joints[(int)jointType].orientation = Quaternion.identity;
            }

            return new BodyData(kinectBody.TrackingId, kinectBody.IsTracked, joints);
        }


        


        // extension method to convert KincetV2 data
        //public static BodyData ToBody(this Kinect.Body kinectBody) {
        //    Joint[] joints = new Joint[Enum.GetNames(typeof(Joint.Type)).Length - 1]; // exclude Joint.Type.NONE

        //    // convert joints
        //    foreach (Kinect.JointType kinectV2JointType in Enum.GetValues(typeof(Kinect.JointType))) {
        //        Joint.Type jointType = kinectV2JointType.ToJointType();
        //        joints[(int)jointType].position    = kinectBody.Joints[kinectV2JointType].ToVector3();
        //        joints[(int)jointType].orientation = kinectBody.JointOrientations[kinectV2JointType].ToQuaternion();
        //    }

        //    // fix unsupported joints orientation
        //    foreach(Joint.Type jointType in UNSUPPORTED_JOINTS) {
        //        joints[(int)jointType].orientation = Quaternion.identity;
        //    }

        //    return new BodyData(kinectBody.TrackingId, kinectBody.IsTracked, joints);
        //}


        // extension method to convert KincetV2 data
        public static Joint.Type ToJointType(this Kinect.JointType kinectJointType) {
            Joint.Type[] LOOKUP_TABLE = {
                /*	 0  SPINBASE		-> */  Joint.Type.PELVIS,
                /*	 1  SPINEMID		-> */  Joint.Type.SPINE_NAVAL,
                /*	 2  NECK			-> */  Joint.Type.NECK,
                /*	 3  HEAD			-> */  Joint.Type.HEAD,
                /*	 4  SHOULDER LEFT	-> */  Joint.Type.SHOULDER_LEFT,
                /*	 5  ELBOW LEFT		-> */  Joint.Type.ELBOW_LEFT,
                /*	 6  WRIST LEFT		-> */  Joint.Type.WRIST_LEFT,
                /*	 7  HANDTIP LEFT	-> */  Joint.Type.HANDTIP_LEFT,
                /*	 8  SHOULDER RIGHT	-> */  Joint.Type.SHOULDER_RIGHT,
                /*	 9  ELBOW RIGHT		-> */  Joint.Type.ELBOW_RIGHT,
                /*	10	WRIST RIGHT		-> */  Joint.Type.WRIST_RIGHT,
                /*	11	HAND TIP RIGHT	-> */  Joint.Type.HANDTIP_RIGHT,
                /*	12	HIP LEFT		-> */  Joint.Type.HIP_LEFT,
                /*	13	KNEE LEFT		-> */  Joint.Type.KNEE_LEFT,
                /*	14	ANKLE LEFT		-> */  Joint.Type.ANKLE_LEFT,
                /*	15	FOOT LEFT		-> */  Joint.Type.FOOT_LEFT,
                /*	16	HIP RIGHT		-> */  Joint.Type.HIP_RIGHT,
                /*	17	KNEE RIGHT		-> */  Joint.Type.KNEE_RIGHT,
                /*	18	ANKLE RIGHT		-> */  Joint.Type.ANKLE_RIGHT,
                /*	19	FOOT RIGHT		-> */  Joint.Type.FOOT_RIGHT,
                /*	20	SPINE SHOULDER	-> */  Joint.Type.SPINE_CHEST,
                /*	21	HAND TIP LEFT	-> */  Joint.Type.HANDTIP_LEFT,
                /*	22	THUMB LEFT		-> */  Joint.Type.THUMB_LEFT,
                /*	23	HAND TIP RIGHT	-> */  Joint.Type.HANDTIP_RIGHT,
                /*	24	THUMB RIGHT		-> */  Joint.Type.THUMB_RIGHT,
            };

            return LOOKUP_TABLE[(int)kinectJointType];
        }


        //// extension method to convert KincetV2 data
        //public static Joint.Type ToJointType(this KinectFace.FacePointType pointType) {
        //    Joint.Type[] LOOKUP_TABLE = {
        //        /*  0 EyeLeft           -> */  Joint.Type.EYE_LEFT,
        //        /*  1 EyeRight          -> */  Joint.Type.EYE_RIGHT,
        //        /*  2 Nose              -> */  Joint.Type.NOSE,
        //        /*  3 MouthCornerLeft   -> */  Joint.Type.NONE,
        //        /*  4 MouthCornerRight  -> */  Joint.Type.NONE,
        //    };

        //    return (pointType == KinectFace.FacePointType.None) ? Joint.Type.NONE : LOOKUP_TABLE[(int)pointType];
        //}

        public static Joint.TrackingState ToJointTrackedState(this Kinect.TrackingState trackungState) {
            switch(trackungState) { 
                case Kinect.TrackingState.NotTracked: return Joint.TrackingState.NOT_TRACKED;
                case Kinect.TrackingState.Tracked   : return Joint.TrackingState.TRACKED;
                case Kinect.TrackingState.Inferred  : return Joint.TrackingState.INFERRED;
            }
            throw new NotImplementedException();
        }


        public static Quaternion toQuaternion(this Windows.Kinect.Vector4 kinectVec4) {
            return new Quaternion(kinectVec4.X, kinectVec4.Y, kinectVec4.Z, kinectVec4.W);
        }



    }

} //! namespace ryabomar.KinectV2 