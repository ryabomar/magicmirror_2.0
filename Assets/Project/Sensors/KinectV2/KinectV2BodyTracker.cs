﻿using System;
using System.Collections.Generic;
using UnityEngine;

using Windows.Kinect;
using Microsoft.Kinect.Face;

namespace ryabomar.KinectV2 {

    /// <summary>
    /// wrapper for Kinect V2 body tracking
    ///     // TODO: face tracking is broken again
    /// </summary>
    public class KinectV2BodyTracker : MonoBehaviour, IBodyTracker {

        public BodyTrackingEvent BodyAppeared_Event    { get; set; } = new BodyTrackingEvent();
        public BodyTrackingEvent BodyDisappeared_Event { get; set; } = new BodyTrackingEvent();
        public BodyTrackingEvent BodyUpdated_Event     { get; set; } = new BodyTrackingEvent();

        Dictionary<ulong, BodyData> _bodyDatas = new Dictionary<ulong, BodyData>();

        KinectSensor _sensor;
        Windows.Kinect.Body[] _kinectBodies;
        BodyFrameReader  _bodyFrameReader;

        class FaceReader {
            FaceFrameSource source;
            FaceFrameReader reader;
            ulong id;

            public FaceReader(ulong id, KinectSensor sensor){
                source = FaceFrameSource.Create(sensor, id, FaceFrameFeatures.RotationOrientation);
                if(source == null){
                    Debug.Log("[Kinect_v2:BodyTracker] Could not create face reader source");
                }
                
                reader = source?.OpenReader();
                if(reader == null){
                    Debug.Log("[Kinect_v2:BodyTracker] Could not open face reader");
                }
            }

            ~FaceReader(){
                reader.Dispose();
            }

            public Quaternion GetHeadOrientation(){
                if(source != null && reader != null){
                    using(var frame = reader.AcquireLatestFrame()){
                        if(frame != null){
                            var faceOrientation =  frame.FaceFrameResult.FaceRotationQuaternion;
                            return new Quaternion(faceOrientation.X, faceOrientation.Y, faceOrientation.Z, faceOrientation.W);
                        }
                    }
                }
                return Quaternion.identity;
            }
        }
        

        Dictionary<ulong, FaceReader> _faceReaders = new Dictionary<ulong, FaceReader>();

        public void OnEnable() {
            _sensor = KinectSensor.GetDefault();
            if (_sensor == null) { // || !_sensor.IsAvailable
                throw new SensorNotFound("[Kinect_v2:BodyTracker] Could not get default sensor");
            }

            {
                _kinectBodies = new Windows.Kinect.Body[_sensor.BodyFrameSource.BodyCount];

                _bodyFrameReader = _sensor.BodyFrameSource.OpenReader();
                if (_bodyFrameReader == null) {
                    throw new SensorInitializationFailed("[Kinect_v2:BodyTracker] Could not open body reader");
                }

                _sensor.Open();
                if (!_sensor.IsOpen) {
                    throw new SensorInitializationFailed("[Kinect_v2:BodyTracker] Could not open sensor");
                }
            }
            
            _bodyFrameReader.FrameArrived += _onFrameArrived;
            Debug.Log("[Kinect_v2:BodyTracker] enabled");
        }


        public void OnDisable() {

            _bodyDatas?.Clear();

            if(_bodyFrameReader != null){
                _bodyFrameReader.FrameArrived -= _onFrameArrived;
            }

            {
                _faceReaders.Clear();

                _bodyFrameReader?.Dispose();     // close body reader
                _sensor?.Close();                // close sensor
            }

            Debug.Log("[Kinect_v2:BodyTracker] disabled");
        }


        public List<BodyData> GetLastFrameBodies() {
            return new List<BodyData>(_bodyDatas.Values); 
        }


        List<ulong> _FindNotupdated(List<ulong> updatedIndecies){
            List<ulong> notUpdated = new List<ulong>();
            
            foreach(ulong id in _bodyDatas.Keys){
                if(!updatedIndecies.Contains(id)){
                    notUpdated.Add(id);
                }
            }

            return notUpdated;
        }


        void _ProcessKinectBodies(ref Body[] kinectBodies) {
            
            //List<ulong> newIds = new List<ulong>();
            HashSet<ulong> arrivedIds = new HashSet<ulong>();

            // convert kinect Body into BodyData
            foreach (var kinectBody in kinectBodies) {
                if(kinectBody == null) continue;                // skip null
                if(kinectBody.IsTracked == false) continue;     // skip untracked

                arrivedIds.Add(kinectBody.TrackingId);

                bool isNewBody = !_bodyDatas.ContainsKey(kinectBody.TrackingId);


                FaceReader faceReader;
                if(isNewBody){
                    faceReader = new FaceReader(kinectBody.TrackingId, _sensor);
                    _faceReaders[kinectBody.TrackingId] = faceReader;
                } else {
                    faceReader = _faceReaders[kinectBody.TrackingId];
                }

                BodyData bodyData = Utils.ConstructBodyData(kinectBody, faceReader.GetHeadOrientation());
                _bodyDatas[kinectBody.TrackingId] = bodyData;


                if (isNewBody) {
                    BodyAppeared_Event.Invoke(bodyData);        // inform listeners about new body
                    Debug.Log("[Kinect_v2:BodyTracker] new body found: id = " + bodyData.id);
                } else {
                    //Debug.Log("[Kinect_v2:BodyTracker] body updated: id = " + bodyData.id);
                    BodyUpdated_Event.Invoke(bodyData);         // inform listeners about update
                }
            }

            // find disappeared bodies
            List<ulong> disappearedIds = new List<ulong>();

            foreach(ulong id in _bodyDatas.Keys){
                if(!arrivedIds.Contains(id)){
                    disappearedIds.Add(id);
                }
            }

            // process disappeared bodies
            foreach(ulong id in disappearedIds){
                BodyDisappeared_Event.Invoke(_bodyDatas[id]);
                _bodyDatas.Remove(id);
                _faceReaders.Remove(id);
            }
        }


        void _onFrameArrived(object sender, BodyFrameArrivedEventArgs args) {
            
            // update kinect bodies
            using(var frame = args.FrameReference.AcquireFrame()) {
                frame.GetAndRefreshBodyData(_kinectBodies);             // update Kinect bodies
                _ProcessKinectBodies(ref _kinectBodies);
            }
        }
        

        void Update() {}
    }

} //! namespace ryabomar.KinectV2