﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Windows.Kinect;


namespace ryabomar.KinectV2 {


    public class DataBaker : IDisposable {

        public Texture colorCamera_Texture      { get => _colorCameratTexture; }
        public Texture pointsPositions_Texture  { get => _pointsPositions_Texture; }
        public Texture pointsUVs_Texture        { get => _pointsColors_Texture; }
        //{ get => _pointsUVs_Texture; }
        public Texture bodyIndecies_Texture     { get => _bodyIndecies_Texture; }
        public Texture depthData_Texture        { get => _depthData_Texture; }


        public DataBaker(Vector2Int depthCameraResolution, Vector2Int colorCameraResolution) {           
            _InitBuffers(depthCameraResolution.x * depthCameraResolution.y);
            _InitTextures(depthCameraResolution, colorCameraResolution);
            _InitShader(depthCameraResolution, colorCameraResolution);
        }


        public void BakeColorCameraView(byte[] data) {
            _colorCameratTexture.LoadRawTextureData(data);
            _colorCameratTexture.Apply();
        }

        public void BakePointsPositions(CameraSpacePoint[] points) {
            _pointsPositionsBuffer?.SetData(points);
            _computeShader?.Dispatch(_pointsPositionsBaker_kernel, 64, 1, 1);
        }


        public void BakePointsUVs(ColorSpacePoint[] points) {
            unsafe { // a little bit dirty....
                fixed(ColorSpacePoint* ptr = points) {
                    IntPtr intPtr = (IntPtr)ptr;
                    _pointsUVs_Texture.LoadRawTextureData(intPtr, sizeof(ColorSpacePoint) * points.Length);
                }
            }
            _pointsUVs_Texture.Apply();
        }


        public void BakeBodyIndecies(byte[] data) {
            _bodyIndecies_Texture.LoadRawTextureData(data);
            _bodyIndecies_Texture.Apply();
        }


        public void BakeDepthData(ushort[] depthBuffer) {
            unsafe { // a little bit dirty....
                fixed (ushort* ptr = depthBuffer){ 
                    IntPtr intPtr = (IntPtr)ptr;
                    _depthData_Texture.LoadRawTextureData(intPtr, sizeof(ushort) * depthBuffer.Length);
                }
            }
            _depthData_Texture.Apply();
        }


        public void BakePointColors(ColorSpacePoint[] uvs){
            _pointsUVsBuffer?.SetData(uvs);

            _computeShader?.Dispatch(_pointsColorsBaker_kernel, 64, 1, 1);
        }


        public virtual void Dispose() {
            _ReleaseShader();
            _ReleaseBuffers();
            _ReleaseTextures();
            _disposed = true;
        }

        // != PUBLIC  ^ ======================================
        // == PRIVATE v ======================================

        Texture2D _colorCameratTexture;
        RenderTexture _pointsPositions_Texture;
        RenderTexture _pointsColors_Texture;
        Texture2D _pointsUVs_Texture;
        Texture2D _bodyIndecies_Texture;
        Texture2D _depthData_Texture;

        ComputeBuffer _pointsPositionsBuffer;
        ComputeBuffer _pointsUVsBuffer;
        ComputeShader _computeShader;

        int _pointsPositionsBaker_kernel;
        int _pointsColorsBaker_kernel;

        bool _disposed = false;


        void _InitTextures(Vector2Int irCameraResolution, Vector2Int colorCameraResolution) {
            { // depth data // one 16-bit
                _depthData_Texture = new Texture2D(irCameraResolution.x, irCameraResolution.y, TextureFormat.R16, false);
            }

            { // body indecies // one 8 bit
                _bodyIndecies_Texture = new Texture2D(irCameraResolution.x, irCameraResolution.y, TextureFormat.R8, false);
            }

            { // positions // 4 floats
                _pointsPositions_Texture = new RenderTexture(irCameraResolution.x, irCameraResolution.y, 0, RenderTextureFormat.ARGBFloat);
                _pointsPositions_Texture.enableRandomWrite = true;
                _pointsPositions_Texture.Create();
            }

            { // points colors
                _pointsColors_Texture = new RenderTexture(irCameraResolution.x, irCameraResolution.y, 0, RenderTextureFormat.ARGBFloat);
                _pointsColors_Texture.enableRandomWrite = true;
                _pointsColors_Texture.Create();
            }

            { // UVs // 2 floats
                _pointsUVs_Texture = new Texture2D(irCameraResolution.x, irCameraResolution.y, TextureFormat.RGFloat, false);
            }

            { // color camera view // RGBA
                _colorCameratTexture = new Texture2D(colorCameraResolution.x, colorCameraResolution.y, TextureFormat.RGBA32, false);
            }

        }

        void _ReleaseTextures() {
            //_pointsPositions_Texture?.Release();

            _pointsPositions_Texture = null;
            _pointsUVs_Texture       = null;
            _bodyIndecies_Texture    = null;
            _depthData_Texture       = null;

            _pointsColors_Texture = null;
        }


        void _InitBuffers(int nPoints) {
            _pointsPositionsBuffer = new ComputeBuffer(nPoints, sizeof(float) * 3); // X Y Z
            _pointsUVsBuffer = new ComputeBuffer(nPoints, sizeof(float) * 2);       // X Y
        }


        void _ReleaseBuffers() {
            _pointsPositionsBuffer?.Release();
            _pointsPositionsBuffer = null;

            _pointsUVsBuffer?.Release();
            _pointsUVsBuffer = null;
        }


        void _InitShader(Vector2Int depthCameraResolution, Vector2Int colorCameraResolution) {
            string shaderName = "DataBaker_ComputeShader";
            try {
                _computeShader = Resources.Load<ComputeShader>(shaderName);
            } catch (Exception e) {
                Dispose();
                throw new Exception("Could not load resource: " + shaderName, e);
            }

            _pointsPositionsBaker_kernel = _computeShader.FindKernel("BakePositions");
            _pointsColorsBaker_kernel    = _computeShader.FindKernel("BakeColors");

            _computeShader.SetBuffer(_pointsPositionsBaker_kernel, "pointsPositions_buffer", _pointsPositionsBuffer);
            _computeShader.SetTexture(_pointsPositionsBaker_kernel, "pointsPositions_Texture", _pointsPositions_Texture, 0);

            _computeShader.SetBuffer(_pointsColorsBaker_kernel, "pointsUVs_buffer", _pointsUVsBuffer);
            _computeShader.SetTexture(_pointsColorsBaker_kernel, "pointsColors_Texture", _pointsColors_Texture, 0);
            _computeShader.SetTexture(_pointsColorsBaker_kernel, "colorcameratexture", _colorCameratTexture, 0);

            _computeShader.SetInt("nPointsHorizontal", depthCameraResolution.x);
            _computeShader.SetInt("nPointsVertical", depthCameraResolution.y);

            _computeShader.SetInt("colorWidth",  colorCameraResolution.x);
            _computeShader.SetInt("colorHeight", colorCameraResolution.y);
        }


        void _ReleaseShader() {
            _computeShader = null; // ???? Dispose??
            _pointsPositionsBaker_kernel = -1;
            _pointsColorsBaker_kernel    = -1;
        }



        ~DataBaker() {
            if(!_disposed) {
                Dispose();
            }
        }
    }

} //! namespace ryabomar.KinectV2
