﻿using System.Collections;
using System.Collections.Generic; 
using UnityEngine;

using Windows.Kinect;
using Microsoft.Kinect.Face;

using System;

namespace ryabomar.KinectV2 {


    /// <summary>
    /// wrapper for Kinect V2 camera sensor
    /// </summary>
    public class KinectV2CameraSensor : MonoBehaviour, ICameraSensor {

        public Texture color_Texture          { get => _baker?.colorCamera_Texture; }
        public Texture bodyIndex_Texture      { get => _baker?.bodyIndecies_Texture; }
        public Texture pointsPos_Texture      { get => _baker?.pointsPositions_Texture; }
        public Texture pointsColors_Texture   { get => _baker?.pointsUVs_Texture; }
        public Texture depthData_Texture      { get => _baker?.depthData_Texture; }


        public Vector2Int colorResolution { get => _colorResolution; }
        public Vector2Int depthResolution { get => _depthResolution; }


        Vector2Int _colorResolution;
        Vector2Int _depthResolution;

        
        // ============ intermediate buffers vvvv
        ColorSpacePoint[]  _pointsUV;
        CameraSpacePoint[] _pointsPos;

        byte[]   _colorBuff;
        ushort[] _depthBuff;
        byte[]   _bodyIndexBuff;
        // !========== intermediate buffers ^^^^

        DataBaker _baker;

        KinectSensor _sensor;
        MultiSourceFrameReader _multiReader;


        public void OnEnable() { 
            _sensor = KinectSensor.GetDefault();

            // TODO: proper check for sensor connected; vvv this vvv does not work
            if(_sensor == null) { // || !_sensor.IsAvailable
                throw new SensorNotFound("[Kinect_v2:Sensor] Could not get default sensor");
            }

            _colorResolution = new Vector2Int(_sensor.ColorFrameSource.FrameDescription.Width, _sensor.ColorFrameSource.FrameDescription.Height);
            _depthResolution = new Vector2Int(_sensor.DepthFrameSource.FrameDescription.Width, _sensor.DepthFrameSource.FrameDescription.Height);
            

            {// initialize buffers
                _baker         = new DataBaker(_depthResolution, _colorResolution);
                _colorBuff     = new byte[4 * colorResolution.x * colorResolution.y]; //RGBA == 4 * byte
                _depthBuff     = new ushort[depthResolution.x * depthResolution.y];
                _bodyIndexBuff = new byte[depthResolution.x * depthResolution.y];
                _pointsUV      = new ColorSpacePoint[depthResolution.x * depthResolution.y];
                _pointsPos     = new CameraSpacePoint[depthResolution.x * depthResolution.y];
            }


            { // initialize reader
                var frameSourceTypes = FrameSourceTypes.None;

                { // activate only required readers
                    frameSourceTypes |= FrameSourceTypes.Color;
                    frameSourceTypes |= FrameSourceTypes.Depth;
                    frameSourceTypes |= FrameSourceTypes.Infrared;
                    frameSourceTypes |= FrameSourceTypes.BodyIndex;
                }

                _multiReader = _sensor.OpenMultiSourceFrameReader(frameSourceTypes);
                if(_multiReader == null) {
                    throw new SensorInitializationFailed("[Kinect_v2:Sensor] Could not open reader");
                }
            }

            _sensor.Open();

            if (!_sensor.IsOpen) {
                throw new SensorInitializationFailed("[Kinect_v2:Sensor] Could not open sensor");
            }

            if(_multiReader != null) { 
                _multiReader.MultiSourceFrameArrived += _OnFrameArrived;
            }
            Debug.Log("[Kinect_v2:Sensor] enabled");
        }


        public void OnDisable() { 
            _baker?.Dispose();
            _baker = null;

            _multiReader?.Dispose();
            _multiReader = null;

            _sensor?.Close();
            _sensor = null;

            if(_multiReader != null) {
                _multiReader.MultiSourceFrameArrived -= _OnFrameArrived;
            }

            Debug.Log("[Kinect_v2:Sensor] disposed");
        }


        void _OnFrameArrived(object sender, MultiSourceFrameArrivedEventArgs args) {
            
            var reference = args.FrameReference.AcquireFrame(); 

            // color frame
            using(var frame = reference.ColorFrameReference.AcquireFrame()) {
                frame.CopyConvertedFrameDataToArray(_colorBuff, ColorImageFormat.Rgba);
                _baker.BakeColorCameraView(_colorBuff);
            }


            // depth frame
            using(var frame = reference.DepthFrameReference.AcquireFrame()) {
                if(frame != null) {
                    
                    frame.CopyFrameDataToArray(_depthBuff);
                    
                    // calculate points positions and UVs
                    var mapper = _sensor.CoordinateMapper;

                    mapper.MapDepthFrameToColorSpace(_depthBuff, _pointsUV);
                    mapper.MapDepthFrameToCameraSpace(_depthBuff, _pointsPos);

                    _baker.BakeDepthData(_depthBuff);
                    //_baker.BakePointsUVs(_pointsUV);
                    _baker.BakePointsPositions(_pointsPos);
                    _baker.BakePointColors(_pointsUV);
                }
            }


            // body index frame
            using(var frame = reference.BodyIndexFrameReference.AcquireFrame()) {
                frame.CopyFrameDataToArray(_bodyIndexBuff);
                _baker.BakeBodyIndecies(_bodyIndexBuff);
            }
        }


        Vector3 _ToWorldSpacePos(DepthSpacePoint point) {
            var mapper = _sensor.CoordinateMapper;
            int col = (int)(point.X * 512);
            int row = (int)(point.Y * 424);

            ushort depth = _depthBuff[col + row * 424];

            CameraSpacePoint csp = mapper.MapDepthPointToCameraSpace(point, depth);

            return new Vector3(csp.X, csp.Y, csp.Z);
        }

    } //! class CameraSensor

} //! namespace ryabomar.KinectV2