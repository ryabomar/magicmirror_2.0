﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ryabomar {    

    /// <summary>
    /// piece of clothing that can be worn on a mannequin
    /// </summary>
    public class Outfit : MonoBehaviour {
        
        [Flags] public enum Category {
            NONE             = 0,
            HEADGEAR         = 1,
            FEETWEAR         = 2,
            GLOVES           = 4,
            UNDERWEAR_BOTTOM = 8,
            UNDERWEAR_TOP    = 16,
            TOPWEAR_BOTTOM   = 32,
            TOPWEAR_TOP      = 64,
            EVERYTHING       = HEADGEAR | FEETWEAR | GLOVES | UNDERWEAR_BOTTOM | UNDERWEAR_TOP | TOPWEAR_BOTTOM | TOPWEAR_TOP
        }


        [Serializable]  public enum BodyType {
            MALE_L,
            MALE_S, 
            FEMALE_L,
            FEMALE_S,
            CHILD, 
        }


        [SerializeField] public uint id;
        [SerializeField] public Category category;
        [SerializeField] public BodyType bodyType;
        [SerializeField] public Texture thumbnail;

        [SerializeField] GameObject skeleton;
        [SerializeField] List<GameObject> bodyMeshes   = new List<GameObject>();
        [SerializeField] List<GameObject> outfitMeshes = new List<GameObject>();
        List<GameObject> maskMeshes = new List<GameObject>();


        void Awake(){
            _Initialize();
        }
        

        public void LinkSkeleton(SkinnedMeshRenderer other){
            outfitMeshes.ForEach((obj) => { _CopySkeletonBones(other.gameObject, obj); });
            bodyMeshes.ForEach  ((obj) => { _CopySkeletonBones(other.gameObject, obj); });
            maskMeshes.ForEach  ((obj) => { _CopySkeletonBones(other.gameObject, obj); });
        }


        void _Initialize(){
            _FindSkeleton();
            _FindBodyMeshes(); // before _FindOutfitMeshes!!!
            _FindOutfitMeshes();
            _makeMaskObjects(); // before _SetupShaders!!!
            _SetupShaders();

            bodyMeshes.ForEach  ((obj) => { obj.SetActive(false); });
            maskMeshes.ForEach  ((obj) => { obj.SetActive(false); });
        }


        void _SetupShaders() {
            foreach(GameObject obj in bodyMeshes){
                obj.GetComponent<SkinnedMeshRenderer>().updateWhenOffscreen = true;
                obj.SetActive(false);
                //ReplaceShader(obj.GetComponent<SkinnedMeshRenderer>(), Shader.Find("stencil/_2_BodyShader"));
            }


            foreach(GameObject obj in outfitMeshes){
                obj.GetComponent<SkinnedMeshRenderer>().updateWhenOffscreen = true;
                //ReplaceShader(obj.GetComponent<SkinnedMeshRenderer>(), Shader.Find("stencil/_3_OutfitUnlitShader"));
            }


            foreach(GameObject obj in maskMeshes){
                obj.GetComponent<SkinnedMeshRenderer>().updateWhenOffscreen = true;
                obj.SetActive(false);
                //ReplaceShader(obj.GetComponent<SkinnedMeshRenderer>(), Shader.Find("stencil/_1_MaskingShader"));
            }
        }


        void _FindSkeleton(){
            if(skeleton == null){ skeleton = this.transform.Find("Game_engine")?.gameObject; }
            if(skeleton == null) throw new Exception("can't load skeleton: " + gameObject.name);
        }


        void _FindOutfitMeshes(){
            foreach(Transform child in this.transform){
                if(child.gameObject == this.skeleton)        continue; 
                if(bodyMeshes.Contains(child.gameObject))    continue;
                if(!outfitMeshes.Contains(child.gameObject)) { outfitMeshes.Add(child.gameObject); }
            }
        }


        void _FindBodyMeshes(){
            string bodyMeshName = gameObject.name.ToLower().Replace("(clone)", "") + "Mesh";
            string eyeMeshName = "high-polyMesh";
            
            GameObject bodyMesh = transform.Find(bodyMeshName)?.gameObject; 
            if(bodyMesh != null && !bodyMeshes.Contains(bodyMesh)) { bodyMeshes.Add(bodyMesh); }

            GameObject eyeMesh = transform.Find(eyeMeshName)?.gameObject;
            if(eyeMesh != null && !bodyMeshes.Contains(eyeMesh)) { bodyMeshes.Add(eyeMesh); }
        }
           

        
        /// <summary>
        /// copies bones from one SkinnedMeshRenderer to another
        /// bone structures have to be the same
        /// </summary>
        /// <param name="sourceObject"></param>
        /// <param name="destObject"></param>
        static void _CopySkeletonBones(GameObject sourceObject, GameObject destObject){
            //https://forum.unity.com/threads/transfer-the-rig-of-a-skinned-mesh-renderer-to-a-new-smr-with-code.499008/
            SkinnedMeshRenderer source = sourceObject.GetComponent<SkinnedMeshRenderer>();
            SkinnedMeshRenderer dest   = destObject.GetComponent<SkinnedMeshRenderer>();

            Dictionary<string, Transform> boneMap = new Dictionary<string, Transform>();
            
            foreach (Transform bone in source.bones)
            {
                boneMap[bone.name] = bone;
            }
    
            Transform[] boneArray = dest.bones;
            for (int idx = 0; idx < boneArray.Length; ++idx){
                string boneName = boneArray[idx].name;
                if (false == boneMap.TryGetValue(boneName, out boneArray[idx]))
                {
                    Debug.LogError("failed to get bone: " + boneName);
                    Debug.Break();
                }
            }
            dest.bones = boneArray;
        }


        void _makeMaskObjects(){
            foreach(GameObject obj in bodyMeshes){
                GameObject maskObj = GameObject.Instantiate(obj, transform);
                maskObj.SetActive(true);
                maskObj.name = "mask__" + maskObj.name;
                maskMeshes.Add(maskObj);
            }
        }


        /// <summary> 
        /// replace shader in given renderer
        /// !!!!
        ///     in order to avoid conflicts with other instances of the same object
        ///     instead of just replacing shader in material creates a new material 
        ///     with given shader and all values of previous material
        /// !!!!
        /// </summary>
        /// <param name="renderer">renderer in which shader will be changed</param>
        /// <param name="shader">source shader</param>
        void ReplaceShader(Renderer renderer, Shader shader){
            Material newMaterial = new Material(shader);
            newMaterial.CopyPropertiesFromMaterial(renderer.material);
            renderer.material = newMaterial;
            renderer.material.shader = shader;
            renderer.material.SetInt("_StencilMask", (int)category);
        }

    } //! class Outfit

} //! namespace ryabomar