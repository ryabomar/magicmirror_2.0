﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ryabomar {

    /// <summary>
    /// collection of outfits
    /// </summary>
    public class OutfitCollection : MonoBehaviour {
        [SerializeField] Showroom male_l_showroom;
        [SerializeField] Showroom male_s_showroom;
        [SerializeField] Showroom female_l_showroom;
        [SerializeField] Showroom female_s_showroom;
        [SerializeField] Showroom child_showroom;


        uint _lastGivenId = 0;
        uint GenerateId() {
            return ++_lastGivenId;
        }

        Dictionary<uint, Outfit>[] _outfits = new Dictionary<uint, Outfit>[Enum.GetNames(typeof(Outfit.BodyType)).Length];
        
        
        void Awake(){
            for(int i = 0; i < _outfits.Length; i++){ _outfits[i] = new Dictionary<uint, Outfit>(); }

            // populate colection
            _LoadResources("OutfitPrefabs/male_l",   Outfit.BodyType.MALE_L);
            _LoadResources("OutfitPrefabs/male_s",   Outfit.BodyType.MALE_S);
            _LoadResources("OutfitPrefabs/female_l", Outfit.BodyType.FEMALE_L);
            _LoadResources("OutfitPrefabs/female_s", Outfit.BodyType.FEMALE_S);
            _LoadResources("OutfitPrefabs/child",    Outfit.BodyType.CHILD);
            
        }
        

        void Start(){
            male_l_showroom.DeactivateCameras();
            male_s_showroom.DeactivateCameras();
            female_l_showroom.DeactivateCameras();
            female_s_showroom.DeactivateCameras();
            child_showroom.DeactivateCameras();
        }

        /// <summary>
        /// get outfits that matches the given parameters
        /// </summary>
        /// <param name="bodyType"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        public List<Outfit> QueryOutfits(Outfit.BodyType bodyType, Outfit.Category category){
            var result = new List<Outfit>();
            foreach(KeyValuePair<uint, Outfit> entry in _outfits[(int)bodyType]){
                if((entry.Value.category & category) != Outfit.Category.NONE){
                    result.Add(entry.Value);
                } 
            }
            return result;
        }


        /// <summary>
        /// get outfits that matches the given parameters
        /// </summary>
        /// <param name="category"></param>
        /// <returns></returns>
        public List<Outfit> QueryOutfits(Outfit.Category category){
            var result = new List<Outfit>();
            foreach(Outfit.BodyType bodyType in Enum.GetValues(typeof(Outfit.BodyType))){
                result.AddRange(QueryOutfits(bodyType, category));
            }
            return result;
        }


        /// <summary>
        /// get outfit by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Outfit GetOutfit(uint id){
            foreach(Outfit.BodyType bodyType in Enum.GetValues(typeof(Outfit.BodyType))){
                if(_outfits[(int)bodyType].TryGetValue(id, out Outfit outfit)){
                    if(outfit != null) return outfit;
                }
            }
            return null;
        }


        void _LoadResources(string resourcePath, Outfit.BodyType bodyType){
            foreach(var outfitPrefab in Resources.LoadAll<Outfit>(resourcePath)){
                outfitPrefab.id = GenerateId();
                outfitPrefab.bodyType = bodyType;
                outfitPrefab.thumbnail = _ChoseShowroom(bodyType).MakeThumbnail(outfitPrefab);
                _outfits[(int)bodyType][outfitPrefab.id] = outfitPrefab;
            }
        }


        Showroom _ChoseShowroom(Outfit.BodyType bodyType){
            switch(bodyType){
                case Outfit.BodyType.MALE_L:    return male_l_showroom;
                case Outfit.BodyType.MALE_S:    return male_s_showroom;
                case Outfit.BodyType.FEMALE_L:  return female_l_showroom;
                case Outfit.BodyType.FEMALE_S:  return female_s_showroom;
                case Outfit.BodyType.CHILD:     return child_showroom;
            } return null;
        }
    }
} //! namespace ryabomar
