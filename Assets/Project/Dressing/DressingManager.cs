﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ryabomar {

    /// <summary>
    /// controlls process of dressing
    /// </summary>
    public class DressingManager : MonoBehaviour {

        [Header("References")]
        [SerializeField] UserSwitcherHandle userSwitcherHandle;
        [SerializeField] MannequinSpawner   mannequinSpawner;
        [SerializeField] public UserSettingsManager settingsManager;

        [Header("Outfit collection")]
        [SerializeField] OutfitCollection outfitCollection;


        [SerializeField] public UserBodyTypeEvent userBodyTypeChanged        = new UserBodyTypeEvent();
        [SerializeField] public CategoryEvent     activeUserOutfitSetChanged = new CategoryEvent();

        public Outfit.BodyType lastBodyType { get => mannequinSpawner.lastSpawnedType; }


        void Start(){
            if(settingsManager.HasProperty(StringSettingsProperty.DRESSING__LAST_USER_BODY_TYPE)){
                string lastBodyTypeStr = settingsManager.GetPropertyValue(StringSettingsProperty.DRESSING__LAST_USER_BODY_TYPE);
                
                Enum.TryParse(lastBodyTypeStr, out Outfit.BodyType lastUserBodyType);
                mannequinSpawner.ChangeNextBodyType(lastUserBodyType);
            }

            userSwitcherHandle.activeBodySwitched.AddListener((bodyData) => {
                Mannequin mannequin = mannequinSpawner.GetActiveUserManniquin();
                if(mannequin != null){
                    activeUserOutfitSetChanged.Invoke(mannequin.equippedCategories);
                }
            });


            SetBodyVisible(settingsManager.GetPropertyValue(BoolSettingsProperty.DEBUG__USER_BODY_VISIBLE));
        }


        /// <summary>
        /// equip an outfit on active mannequin
        /// </summary>
        /// <param name="outfit"></param>
        public void EquipOnActiveUser(uint outfitId) {
            Outfit outfit = outfitCollection.GetOutfit(outfitId);
            if(outfit != null){
                mannequinSpawner.EquipOnActiveUserMannequin(outfit);

                Mannequin mannequin = mannequinSpawner.GetActiveUserManniquin();
                if(mannequin != null){
                    activeUserOutfitSetChanged.Invoke(mannequin.equippedCategories);
                }

            }
        }


        /// <summary>
        /// unequip outfit from active mannequin of given category
        /// </summary>
        /// <param name="category"></param>
        public void UnequipFromActiveUser(Outfit.Category category){
            Mannequin mannequin = mannequinSpawner.GetActiveUserManniquin();
            if(mannequin != null){
                mannequin.Unequip(category);
                activeUserOutfitSetChanged.Invoke(mannequin.equippedCategories);
            }
        }


        /// <summary>
        /// adjust BodyAvatar of currently active user
        /// </summary>
        public void AdjustBody(){
            mannequinSpawner.AdjustActiveUserBody();
        }


        /// <summary>
        /// change tody type of BodyAvatar of currently active user
        /// </summary>
        /// <param name="type"></param>
        public void ChangeBodyType(Outfit.BodyType type){
            // if(){
            //     userBodyTypeChanged.Invoke(type);
            // }
            mannequinSpawner.ChangeActiveUserBodyType(type);

            userBodyTypeChanged.Invoke(type);

            settingsManager.SetProperty(StringSettingsProperty.DRESSING__LAST_USER_BODY_TYPE, type.ToString());

            Debug.Log("[DressingManager] body type changet to " + type.ToString());
        }


        public void SetBodyVisible(bool value){
            mannequinSpawner.SetBodyVisible(value);
            settingsManager.SetProperty(BoolSettingsProperty.DEBUG__USER_BODY_VISIBLE, value);
        }


        public void SetMannequinScale(Vector3 scale){
            mannequinSpawner?.SetMannequinScale(scale);
        }

    } //! class DressingRoom

} //! namespace ryabomar
