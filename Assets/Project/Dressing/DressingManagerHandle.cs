﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar {

    /// <summary>
    /// Handle for class DressingRoom
    /// </summary>
    [CreateAssetMenu(fileName = "DressingManagerHandle", menuName = "MagicMirror/DressingManagerHandle")]
    public class DressingManagerHandle : ScriptableObject {

        //public UserBodyTypeEvent userBodyTypeChangedEvent = new UserBodyTypeEvent();

        [SerializeField] public UserBodyTypeEvent userBodyTypeChanged        => _dressingManager?.userBodyTypeChanged;
        [SerializeField] public CategoryEvent     activeUserOutfitSetChanged => _dressingManager?.activeUserOutfitSetChanged;

        public Outfit.BodyType lastBodyType { get => _dressingManager.lastBodyType; }
        
        DressingManager _dressingManager => FindObjectOfType<DressingManager>();
        

        /// <summary>
        /// equip an outfit on active mannequin
        /// </summary>
        /// <param name="outfit"></param>
        public void EquipOnActiveUser(uint outfitId) {
            _dressingManager?.EquipOnActiveUser(outfitId);
        }


        /// <summary>
        /// unequip outfit from active mannequin of given category
        /// </summary>
        /// <param name="category"></param>
        public void UnequipFromActiveUser(Outfit.Category category){
            _dressingManager?.UnequipFromActiveUser(category);
        }


        public void AdjustActiveUserBody(){
            _dressingManager?.AdjustBody();
        }
        

        public void ChangeActiveUserBodyType(Outfit.BodyType type){
            _dressingManager?.ChangeBodyType(type);
        }


        public void SetBodyVisible(bool value){
            _dressingManager?.SetBodyVisible(value);
        }


        public void SetMannequinScale(Vector3 scale){
            _dressingManager?.SetMannequinScale(scale);
        }

    } //! class DressingRoomHandle
} //! namespace ryabomar


