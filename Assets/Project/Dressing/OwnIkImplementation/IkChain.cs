﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ryabomar.OwnIk {
    
    /// <summary> Chain of elements to apply inverse kinematic </summary>
    public class IkChain {
        
        private class IkLink {
            public Transform transform;
            public Quaternion initRotation;
            public Vector3 directionToChild;
            public float length;

            public IkLink(Transform transform, Quaternion initLocalOrientation, Vector3 directionToChild, float length){
                this.transform = transform;
                this.initRotation = initLocalOrientation;
                this.directionToChild = directionToChild;
                this.length = length;
            }
        }
        
        List<IkLink> _links = new List<IkLink>();

        // constructor prevents creating chain of length 0
        IkLink _root     => _links[_links.Count - 1];
        IkLink _effector => _links[0];

        float _length = 0.0f; // total chain length


        /// <summary> Constructor </summary>
        /// <param name="effector">end of the chain; will strive to reach target</param>
        /// <param name="root">root; won't move</param>
        public IkChain(Transform effector, Transform root){
            if(root == null || effector == null) { throw new ArgumentNullException(); }
            
            {// check if root is related to effector
                bool isAncestor = false;
                Transform tr = effector.transform;
                while(tr != null){
                    if(tr == root) {
                        isAncestor = true;
                        break;
                    }
                    tr = tr.parent;
                }

                if(!isAncestor) throw new ArgumentException();
            }

            _length = 0.0f;
            
            { // add effector link
                IkLink effectorLink = new IkLink(
                    effector.transform,
                    effector.transform.localRotation,
                    Vector3.zero,
                    0.0f
                );
                _links.Add(effectorLink);
            }

            { // add intermediate links
                Transform childLinkTransform = effector;
                Transform linkTransform = effector.transform.parent;

                while (linkTransform != root) {

                    IkLink newLink = new IkLink(
                        linkTransform, 
                        linkTransform.localRotation, 
                        childLinkTransform.localPosition.normalized,
                        (childLinkTransform.position - linkTransform.position).magnitude
                    );
                    
                    _length += newLink.length;
                    
                    _links.Add(newLink);

                    childLinkTransform = linkTransform;
                    linkTransform = linkTransform.parent;
                }
            }

            { // add root
                IkLink lastLink = _links[_links.Count - 1];

                IkLink rootLink = new IkLink(
                    root.transform,
                    root.transform.localRotation,
                    lastLink.transform.localPosition.normalized,
                    (lastLink.transform.position - root.transform.position).magnitude
                );

                _length += rootLink.length;

                _links.Add(rootLink);
            }
            
            //Debug.Log("IK chain: from " + _root.transform.name + " to " + _effector.transform.name);
        }


        /// <summary>
        /// update IK using coordinates as targets in world space
        /// </summary>
        /// <param name="targets"></param>
        public void SetTargetsAndUpdateIk(Vector3[] targets){
            Vector3 effectorTarget = targets[0];

            if((_root.transform.position - effectorTarget).sqrMagnitude >= (_length*_length)){
                StratchTo(effectorTarget);
            } else {
                _FABRIK(targets, 10, 0.001f);
            }
            
        }


        /// <summary>
        /// sets effector global orientation
        /// </summary>
        /// <param name="globalOrientation"></param>
        public void SetEffectorRotation(Quaternion orientation){
            // _effector.transform.localRotation = _effector.initRotation;
            // _effector.transform.localRotation *= orientation;
            //TODO

        }



        /// <summary>
        /// recalculate link lengths
        ///  requires to do so after manniquin adjustment
        /// </summary>
        public void RecalculateLength(){
            _length = 0;
            for(int i = 1; i < _links.Count; i++){
                _links[i].length = (_links[i - 1].transform.position - _links[i].transform.position).magnitude;
                _length += _links[i].length;
            }
        }

         
        /// <summary> IK algorithm implementation </summary>
        /// <param name="targets">array of targets starting from effector</param>
        /// <param name="maxItarations">maximum namber of allowed iterations</param>
        /// <param name="precision">precision threshold</param>
        /// <returns>number of executed iterations</returns>
        uint _FABRIK(Vector3[] targets, uint maxItarations, float precision) {
            int nLinks = _links.Count;

            Vector3[] calculatedLinkPositions = new Vector3[nLinks];

            // copy node positions
            for(int i = 0; i < nLinks; i++){
                calculatedLinkPositions[i] = _links[i].transform.position;
            }

            uint iteration = 1;
            for(; iteration <= maxItarations; iteration++){
                {// forward part
                    // move effector to its target
                    calculatedLinkPositions[0] = targets[0];

                    for(int i = 1; i < nLinks; i++){
                        Vector3 d;

                        if(iteration == 1 && i < targets.Length) {
                            // use hints on first iteration
                            d = calculatedLinkPositions[i - 1] - targets[i];
                        } else {
                            d = calculatedLinkPositions[i - 1] - calculatedLinkPositions[i];
                        }

                        calculatedLinkPositions[i] = calculatedLinkPositions[i - 1] - d.normalized * _links[i].length;
                    }
                }
                
                {// backward part
                    calculatedLinkPositions[_links.Count - 1] = _links[_links.Count - 1].transform.position;
                    
                    for(int i = _links.Count - 2; i >= 0; i--){
                        Vector3 d = calculatedLinkPositions[i] - calculatedLinkPositions[i + 1];
                        calculatedLinkPositions[i] = calculatedLinkPositions[i + 1] + d.normalized * _links[i + 1].length;

                    }
                }

                // check distance to target
                if((calculatedLinkPositions[0] - _links[0].transform.position).sqrMagnitude <= (precision * precision)) {
                    break; // close enought
                }
            }

            // apply positions
            for(int i = _links.Count - 1; i >= 1; i--){
                // restor initial rotation
                _links[i].transform.localRotation = _links[i].initRotation;

                // figure aout how to rotate 
                Vector3 dirToChild    = _links[i].directionToChild;
                Vector3 newDirToChild = _links[i].transform.InverseTransformDirection(calculatedLinkPositions[i-1] - calculatedLinkPositions[i]); // transform to local space
                Quaternion deltaRotation = Quaternion.FromToRotation(dirToChild, newDirToChild);

                // apply rotation
                _links[i].transform.localRotation *= deltaRotation;
            }
            
            return iteration - 1; // negitiate one extra increment from for loop
        }



        /// <summary> Stratch whole chain along line </summary>
        /// <param name="target">target to reach</param>
        void StratchTo(Vector3 target){
            for(int i = _links.Count - 1; i >= 1; i--){
                _links[i].transform.localRotation = _links[i].initRotation;
                
                Vector3 dirToChild = _links[i].directionToChild;
                Vector3 DirTarget  = _links[i].transform.InverseTransformDirection(target - _links[i].transform.position);
                Quaternion deltaRotation = Quaternion.FromToRotation(dirToChild, DirTarget);
                
                _links[i].transform.localRotation *= deltaRotation;
            }
        }
    } //! class IkChain

} //! namespace ryabomar.OwnIk