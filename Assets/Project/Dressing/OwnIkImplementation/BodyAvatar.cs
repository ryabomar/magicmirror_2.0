﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ryabomar;


namespace ryabomar.OwnIk {




    /// <summary> sort of Unity Avatar replacement </summary>
    public class BodyAvatar {
        public Dictionary<HumanBodyBones, Transform> bones { get; private set; }

        public float headLerp = 0.8f;
        public float lerp = 0.5f;

        IkChain _leftHandIkCHain;
        IkChain _rightHandIkCHain;
        IkChain _leftLegIkCHain;
        IkChain _rightLegIkCHain;
        IkChain _spineIkChain;


        Quaternion _initRotation_hips;
        Quaternion _initRotation_chest;
        Quaternion _initRotation_upperChest;

        Quaternion _initHeadRotation;


        /// <summary> constructor </summary>
        public BodyAvatar(Dictionary<HumanBodyBones, Transform> bones) {
            this.bones = bones;

            _initRotation_hips       = bones[HumanBodyBones.Hips].rotation;
            _initRotation_chest      = bones[HumanBodyBones.Chest].rotation;
            _initRotation_upperChest = bones[HumanBodyBones.UpperChest].rotation;

            _initHeadRotation = bones[HumanBodyBones.Head].rotation;

            // create IK chains
            _leftHandIkCHain  = new IkChain(bones[HumanBodyBones.LeftHand],  bones[HumanBodyBones.LeftShoulder]);
            _rightHandIkCHain = new IkChain(bones[HumanBodyBones.RightHand], bones[HumanBodyBones.RightShoulder]);
            _leftLegIkCHain   = new IkChain(bones[HumanBodyBones.LeftToes],  bones[HumanBodyBones.LeftUpperLeg]);
            _rightLegIkCHain  = new IkChain(bones[HumanBodyBones.RightToes], bones[HumanBodyBones.RightUpperLeg]);
            _spineIkChain     = new IkChain(bones[HumanBodyBones.Neck],      bones[HumanBodyBones.Spine]);
        }


        /// <summary>animate movement using data from sensor </summary>
        public void UpdateIK(BodyData bodyData){
            _MovePelvis(bodyData);

            _spineIkChain.SetTargetsAndUpdateIk(new Vector3[]{
                bodyData.joints[(int)Joint.Type.NECK].position,
                bodyData.joints[(int)Joint.Type.SPINE_CHEST].position,
                bodyData.joints[(int)Joint.Type.SPINE_NAVAL].position,
            });
            _RotateSpineBones(bodyData);

            _leftHandIkCHain.SetTargetsAndUpdateIk(new Vector3[]{
                bodyData.joints[(int)Joint.Type.WRIST_RIGHT].position,
                bodyData.joints[(int)Joint.Type.ELBOW_RIGHT].position,
                bodyData.joints[(int)Joint.Type.SHOULDER_RIGHT].position,
                //bodyData.joints[(int)Joint.Type.CLAVICLE_RIGHT].position,
            });  
            _leftHandIkCHain.SetEffectorRotation(bodyData.joints[(int)Joint.Type.WRIST_RIGHT].orientation);

            _rightHandIkCHain.SetTargetsAndUpdateIk(new Vector3[]{
                bodyData.joints[(int)Joint.Type.WRIST_LEFT].position,
                bodyData.joints[(int)Joint.Type.ELBOW_LEFT].position,
                bodyData.joints[(int)Joint.Type.SHOULDER_LEFT].position,
                //bodyData.joints[(int)Joint.Type.CLAVICLE_LEFT].position,
            }); 
            _rightHandIkCHain.SetEffectorRotation(bodyData.joints[(int)Joint.Type.WRIST_LEFT].orientation);

            _leftLegIkCHain.SetTargetsAndUpdateIk(new Vector3[]{
                bodyData.joints[(int)Joint.Type.FOOT_RIGHT].position,
                bodyData.joints[(int)Joint.Type.ANKLE_RIGHT].position,
                bodyData.joints[(int)Joint.Type.KNEE_RIGHT].position,
                bodyData.joints[(int)Joint.Type.HIP_RIGHT].position,
            });   


            _rightLegIkCHain.SetTargetsAndUpdateIk(new Vector3[]{
                bodyData.joints[(int)Joint.Type.FOOT_LEFT].position,
                bodyData.joints[(int)Joint.Type.ANKLE_LEFT].position,
                bodyData.joints[(int)Joint.Type.KNEE_LEFT].position,
                bodyData.joints[(int)Joint.Type.HIP_LEFT].position,
            });  
            
            // apply head orientation
            bones[HumanBodyBones.Head].transform.rotation = 
            Quaternion.Lerp(
                Quaternion.AngleAxis(180, Vector3.up) * bodyData.joints[(int)Joint.Type.HEAD].orientation,
                bones[HumanBodyBones.Head].transform.rotation,
                headLerp
            );
        }


        /// <summary> make avatar from given root object and relative path map </summary>
        public static BodyAvatar MakeUsingBoneMap(GameObject rootObject, Dictionary<HumanBodyBones, string> boneMap){

            Dictionary<HumanBodyBones, Transform> bones = new Dictionary<HumanBodyBones, Transform>();

            foreach(var entry in boneMap){
                HumanBodyBones boneEnun = entry.Key;
                string pathToObj        = entry.Value;
                
                Transform boneTransform = rootObject.transform.Find(pathToObj);
                if(boneTransform == null) { throw new ArgumentOutOfRangeException(); }// cant find

                bones[boneEnun] = boneTransform;
            }


            return new BodyAvatar(bones);
        }


        /// <summary> adjust avatar to fit users body sizes </summary>
        public void Adjust(BodyData bodyData) {
            { // adjust spine
                var joints = bodyData.joints;//kinectBody.joints;
                //bones[HumanBodyBones.Hips]
                //_animator.GetBoneTransform(HumanBodyBones.Hips);
                float avatarSpineLength = 0.0f;
                float userSpineLength   = 0.0f;

                avatarSpineLength += (bones[HumanBodyBones.Spine].position      - bones[HumanBodyBones.Hips].position).magnitude;         
                avatarSpineLength += (bones[HumanBodyBones.Chest].position      - bones[HumanBodyBones.Spine].position).magnitude;        
                avatarSpineLength += (bones[HumanBodyBones.UpperChest].position - bones[HumanBodyBones.Chest].position).magnitude;        
                avatarSpineLength += (bones[HumanBodyBones.Neck].position       - bones[HumanBodyBones.UpperChest].position).magnitude;   

                userSpineLength += (joints[(int)Joint.Type.SPINE_NAVAL].position - joints[(int)Joint.Type.PELVIS].position).magnitude;
                userSpineLength += (joints[(int)Joint.Type.SPINE_CHEST].position - joints[(int)Joint.Type.SPINE_NAVAL].position).magnitude;
                userSpineLength += (joints[(int)Joint.Type.NECK].position        - joints[(int)Joint.Type.SPINE_CHEST].position).magnitude;

                //_animator.GetBoneTransform(HumanBodyBones.Hips).localPosition *= userSpineLength / avatarSpineLength; // not needed
                bones[HumanBodyBones.Spine].localPosition      *= userSpineLength / avatarSpineLength;
                bones[HumanBodyBones.Chest].localPosition      *= userSpineLength / avatarSpineLength;
                bones[HumanBodyBones.UpperChest].localPosition *= userSpineLength / avatarSpineLength;
                bones[HumanBodyBones.Neck].localPosition       *= userSpineLength / avatarSpineLength;
            }


            { // adjust legs
                var joints = bodyData.joints;
                float avatarLegLength = 0.0f;
                float userLegLength   = 0.0f;

                { // left leg
                    float avatarLeftUpperLegLength = (bones[HumanBodyBones.LeftLowerLeg].position - bones[HumanBodyBones.LeftUpperLeg].position).magnitude;
                    float avatarLeftLowerLegLength = (bones[HumanBodyBones.LeftFoot].position     - bones[HumanBodyBones.LeftLowerLeg].position).magnitude;
                    
                    avatarLegLength += avatarLeftUpperLegLength + avatarLeftLowerLegLength; // + avatarLeftClavicleLength

                    float userLeftUpperLegLength   = (joints[(int)Joint.Type.KNEE_LEFT].position    - joints[(int)Joint.Type.HIP_LEFT].position).magnitude;
                    float userLeftLowerLegLength   = (joints[(int)Joint.Type.ANKLE_LEFT].position   - joints[(int)Joint.Type.KNEE_LEFT].position).magnitude;

                    userLegLength += userLeftUpperLegLength + userLeftLowerLegLength; // userLeftClavicleLength
                }


                { // right leg
                    float avatarRightUpperLegLength = (bones[HumanBodyBones.RightLowerLeg].position - bones[HumanBodyBones.RightUpperLeg].position).magnitude;
                    float avatarRightLowerLegLength = (bones[HumanBodyBones.RightFoot].position     - bones[HumanBodyBones.RightLowerLeg].position).magnitude;
                    
                    avatarLegLength += avatarRightUpperLegLength + avatarRightLowerLegLength; // + avatarLeftClavicleLength

                    float userRightUpperLegLength   = (joints[(int)Joint.Type.KNEE_RIGHT].position    - joints[(int)Joint.Type.HIP_RIGHT].position).magnitude;
                    float userRightLowerLegLength   = (joints[(int)Joint.Type.ANKLE_RIGHT].position   - joints[(int)Joint.Type.KNEE_RIGHT].position).magnitude;

                    userLegLength += userRightUpperLegLength + userRightLowerLegLength; // userLeftClavicleLength
                }


                userLegLength   /= 2.0f;
                avatarLegLength /= 2.0f;

                // rescale bones
                bones[HumanBodyBones.LeftUpperLeg].localPosition  *= userLegLength / avatarLegLength;
                bones[HumanBodyBones.LeftLowerLeg].localPosition  *= userLegLength / avatarLegLength;
                bones[HumanBodyBones.LeftFoot].localPosition      *= userLegLength / avatarLegLength;

                bones[HumanBodyBones.RightUpperLeg].localPosition *= userLegLength / avatarLegLength;
                bones[HumanBodyBones.RightLowerLeg].localPosition *= userLegLength / avatarLegLength;
                bones[HumanBodyBones.RightFoot].localPosition     *= userLegLength / avatarLegLength;
            }


            { // adjust arms
                var joints = bodyData.joints;
                float avatarArmLength = 0.0f;
                float userArmLength   = 0.0f;

                { // left arm
                    //float avatarLeftClavicleLength = (_animator.GetBoneTransform(HumanBodyBones.LeftUpperArm).position - _animator.GetBoneTransform(HumanBodyBones.LeftShoulder).position).magnitude;
                    float avatarLeftUpperArmLength = (bones[HumanBodyBones.LeftLowerArm].position - bones[HumanBodyBones.LeftUpperArm].position).magnitude;
                    float avatarLeftLowerArmLength = (bones[HumanBodyBones.LeftHand].position     - bones[HumanBodyBones.LeftLowerArm].position).magnitude;
                    
                    avatarArmLength += avatarLeftUpperArmLength + avatarLeftLowerArmLength; // + avatarLeftClavicleLength

                    //float userLeftClavicleLength   = (joints[(int)Joint.Type.SHOULDER_LEFT].position - joints[(int)Joint.Type.CLAVICLE_LEFT].position).magnitude;
                    float userLeftUpperArmLength   = (joints[(int)Joint.Type.ELBOW_LEFT].position    - joints[(int)Joint.Type.SHOULDER_LEFT].position).magnitude;
                    float userLeftLowerArmLength   = (joints[(int)Joint.Type.WRIST_LEFT].position    - joints[(int)Joint.Type.ELBOW_LEFT].position).magnitude;

                    userArmLength += userLeftUpperArmLength + userLeftLowerArmLength; // userLeftClavicleLength
                }


                { // right arm
                    //float avatarRightClavicleLength = (_animator.GetBoneTransform(HumanBodyBones.RightUpperArm).position - _animator.GetBoneTransform(HumanBodyBones.RightShoulder).position).magnitude;
                    float avatarRightUpperArmLength = (bones[HumanBodyBones.RightLowerArm].position - bones[HumanBodyBones.RightUpperArm].position).magnitude;
                    float avatarRightLowerArmLength = (bones[HumanBodyBones.RightHand].position     - bones[HumanBodyBones.RightLowerArm].position).magnitude;
                    
                    avatarArmLength += avatarRightUpperArmLength + avatarRightLowerArmLength; // + avatarRightClavicleLength

                    //float userRightClavicleLength   = (joints[(int)Joint.Type.SHOULDER_RIGHT].position - joints[(int)Joint.Type.CLAVICLE_RIGHT].position).magnitude;
                    float userRightUpperArmLength   = (joints[(int)Joint.Type.ELBOW_RIGHT].position    - joints[(int)Joint.Type.SHOULDER_RIGHT].position).magnitude;
                    float userRightLowerArmLength   = (joints[(int)Joint.Type.WRIST_RIGHT].position    - joints[(int)Joint.Type.ELBOW_RIGHT].position).magnitude;

                    userArmLength += userRightUpperArmLength + userRightLowerArmLength; // userRightClavicleLength
                }

                userArmLength   /= 2.0f;
                avatarArmLength /= 2.0f;

                // rescale bones
                bones[HumanBodyBones.LeftUpperArm].localPosition  *= userArmLength / avatarArmLength;
                bones[HumanBodyBones.LeftLowerArm].localPosition  *= userArmLength / avatarArmLength;
                bones[HumanBodyBones.LeftHand].localPosition      *= userArmLength / avatarArmLength;

                bones[HumanBodyBones.RightUpperArm].localPosition *= userArmLength / avatarArmLength;
                bones[HumanBodyBones.RightLowerArm].localPosition *= userArmLength / avatarArmLength;
                bones[HumanBodyBones.RightHand].localPosition     *= userArmLength / avatarArmLength;
            }
        
            { // recalculate IK lengths
                _leftHandIkCHain.RecalculateLength();
                _rightHandIkCHain.RecalculateLength();
                _leftLegIkCHain.RecalculateLength();
                _rightLegIkCHain.RecalculateLength();
                _spineIkChain.RecalculateLength();
            }
        }
        

        void _MovePelvis(BodyData bodyData) {
            bones[HumanBodyBones.Hips].position = bodyData.joints[(int)Joint.Type.PELVIS].position;
            bones[HumanBodyBones.Hips].rotation = _CalcPelvisOrientation(bodyData);
        }

        Quaternion _CalcPelvisOrientation(BodyData bodyData){
            var joints = bodyData.joints;
            Vector3 spineBasePos = joints[(int)Joint.Type.PELVIS].position;
            Vector3 leftHipPos   = joints[(int)Joint.Type.HIP_LEFT].position;
            Vector3 rightHipPos  = joints[(int)Joint.Type.HIP_RIGHT].position;
            Vector3 spineMidPos  = joints[(int)Joint.Type.SPINE_NAVAL].position;

            Vector3 upDir        = (spineMidPos - spineBasePos).normalized;
            Vector3 rightDir     = ((rightHipPos - spineBasePos) * 2 - (leftHipPos - spineBasePos)).normalized; // average vector
            Vector3 forwardDir   = Vector3.Cross(upDir, rightDir.normalized);

            if(!forwardDir.Equals(Vector3.zero)){
                //bones[HumanBodyBones.Hips].rotation = Quaternion.LookRotation(forwardDir, upDir) * _initRotation_hips;
            }

            return Quaternion.LookRotation(forwardDir, upDir) * _initRotation_hips;
        }


        void _RotateSpineBones(BodyData bodyData) {
            var joints = bodyData.joints;
            
            { // hips
                Vector3 spineBasePos = joints[(int)Joint.Type.PELVIS].position;
                Vector3 leftHipPos   = joints[(int)Joint.Type.HIP_LEFT].position;
                Vector3 rightHipPos  = joints[(int)Joint.Type.HIP_RIGHT].position;
                Vector3 spineMidPos  = joints[(int)Joint.Type.SPINE_NAVAL].position;

                Vector3 upDir        = (spineMidPos - spineBasePos).normalized;
                Vector3 rightDir     = ((rightHipPos - spineBasePos) * 2 - (leftHipPos - spineBasePos)).normalized; // average vector
                Vector3 forwardDir   = Vector3.Cross(upDir, rightDir.normalized);

                if(!forwardDir.Equals(Vector3.zero)){
                    bones[HumanBodyBones.Hips].rotation = Quaternion.LookRotation(forwardDir, upDir) * _initRotation_hips;
                }
            }

            { // Upper chest
                Vector3 spineShoulderPos = joints[(int)Joint.Type.SPINE_CHEST].position;
                Vector3 leftShoulderPos  = joints[(int)Joint.Type.SHOULDER_LEFT].position;
                Vector3 rightShoulderPos = joints[(int)Joint.Type.SHOULDER_RIGHT].position;
                Vector3 spineMidPos      = joints[(int)Joint.Type.SPINE_NAVAL].position;

                Vector3 upDir        = (spineShoulderPos - spineMidPos).normalized;
                Vector3 rightDir     = ((rightShoulderPos - spineShoulderPos) * 2 - (leftShoulderPos - spineShoulderPos)).normalized;
                Vector3 forwardDir   = -Vector3.Cross(rightDir.normalized, upDir);

                if(!forwardDir.Equals(Vector3.zero)){
                    bones[HumanBodyBones.UpperChest].rotation = Quaternion.LookRotation(forwardDir, upDir) * _initRotation_upperChest;
                }
            }

            // { // upper chest

            // }
        }


    } //! public class BodyAvatar

}//! namespace ryabomar.OwnIk
