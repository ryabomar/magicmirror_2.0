﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar.OwnIk {

    /// <summary>
    /// MonoBehaviour component to operate with BodyAvatar
    /// Accepts BodyData to use joint positions as IK targets 
    /// </summary>
    public class BodyAvatarComponent : MonoBehaviour {
        public BodyAvatar bodyAvatar;
        BodyData _lastBodyData;
        
        void Start(){
            /// currently only makehuman is supported
            bodyAvatar = Utils.MakeBoduAvatarUsingMakeHumanBoneMap(gameObject);
        }


        void LateUpdate(){
            if(_lastBodyData != null){
                bodyAvatar?.UpdateIK(_lastBodyData);
            }
        }


        public void OnBodyDataUpdated(BodyData bodyData){
            _lastBodyData = bodyData;
        }
    } //! class BodyAvatarComponent

} //! namespace ryabomar.OwnIk
