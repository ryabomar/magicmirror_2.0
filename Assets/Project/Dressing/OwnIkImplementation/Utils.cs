﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ryabomar.OwnIk {

public static class Utils {

    // lookup table for MakeHuman skeleton
    public static Dictionary<HumanBodyBones, string> boneMap_Makehuman { get; private set; }= new Dictionary<HumanBodyBones, string>{
            // spine
            { HumanBodyBones.Hips,          "Game_engine/Root/pelvis" },
            { HumanBodyBones.Spine,         "Game_engine/Root/pelvis/spine_01"},
            { HumanBodyBones.Chest,         "Game_engine/Root/pelvis/spine_01/spine_02"},
            { HumanBodyBones.UpperChest,    "Game_engine/Root/pelvis/spine_01/spine_02/spine_03"},
            { HumanBodyBones.Neck,          "Game_engine/Root/pelvis/spine_01/spine_02/spine_03/neck_01"},
            { HumanBodyBones.Head,          "Game_engine/Root/pelvis/spine_01/spine_02/spine_03/neck_01/head"},

            // left hand
            { HumanBodyBones.LeftShoulder,  "Game_engine/Root/pelvis/spine_01/spine_02/spine_03/clavicle_l"},
            { HumanBodyBones.LeftUpperArm,  "Game_engine/Root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l"},
            { HumanBodyBones.LeftLowerArm,  "Game_engine/Root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l"},
            { HumanBodyBones.LeftHand,      "Game_engine/Root/pelvis/spine_01/spine_02/spine_03/clavicle_l/upperarm_l/lowerarm_l/hand_l"},

            // right hand
            { HumanBodyBones.RightShoulder, "Game_engine/Root/pelvis/spine_01/spine_02/spine_03/clavicle_r"},
            { HumanBodyBones.RightUpperArm, "Game_engine/Root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r"},
            { HumanBodyBones.RightLowerArm, "Game_engine/Root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r"},
            { HumanBodyBones.RightHand,     "Game_engine/Root/pelvis/spine_01/spine_02/spine_03/clavicle_r/upperarm_r/lowerarm_r/hand_r"},

            // left leg
            { HumanBodyBones.LeftUpperLeg,  "Game_engine/Root/pelvis/thigh_l"},
            { HumanBodyBones.LeftLowerLeg,  "Game_engine/Root/pelvis/thigh_l/calf_l"},
            { HumanBodyBones.LeftFoot,      "Game_engine/Root/pelvis/thigh_l/calf_l/foot_l"},
            { HumanBodyBones.LeftToes,      "Game_engine/Root/pelvis/thigh_l/calf_l/foot_l/ball_l"},

            // right leg
            { HumanBodyBones.RightUpperLeg,  "Game_engine/Root/pelvis/thigh_r"},
            { HumanBodyBones.RightLowerLeg,  "Game_engine/Root/pelvis/thigh_r/calf_r"},
            { HumanBodyBones.RightFoot,      "Game_engine/Root/pelvis/thigh_r/calf_r/foot_r"},
            { HumanBodyBones.RightToes,      "Game_engine/Root/pelvis/thigh_r/calf_r/foot_r/ball_r"},
        };


    /// <summary> make avatar from given makehuman campatable object </summary>
    public static BodyAvatar MakeBoduAvatarUsingMakeHumanBoneMap(GameObject makeHumanRootObject){
        return BodyAvatar.MakeUsingBoneMap(makeHumanRootObject, boneMap_Makehuman);
    }

}

} //! namespace ryabomar.OwnIk