﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace ryabomar.UI {
    public class BodyTypeSwitcherWidget : MonoBehaviour {
        [SerializeField] DressingManager dressingManager;
        [SerializeField] UserSettingsManager settingsManager;

        [SerializeField] Toggle toggle_MALE_L;
        [SerializeField] Toggle toggle_MALE_S;
        [SerializeField] Toggle toggle_FEMALE_L;
        [SerializeField] Toggle toggle_FEMALE_S;
        [SerializeField] Toggle toggle_CHILD; 


        public void Start(){
            if(settingsManager.HasProperty(StringSettingsProperty.DRESSING__LAST_USER_BODY_TYPE)){
                string lastBodyTypeStr = settingsManager.GetPropertyValue(StringSettingsProperty.DRESSING__LAST_USER_BODY_TYPE);
                
                Enum.TryParse(lastBodyTypeStr, out Outfit.BodyType lastUserBodyType);

                switch (lastUserBodyType){
                    case Outfit.BodyType.MALE_L:     toggle_MALE_L.isOn = true; break;
                    case Outfit.BodyType.MALE_S:     toggle_MALE_S.isOn = true; break;
                    case Outfit.BodyType.FEMALE_L:   toggle_FEMALE_L.isOn = true; break;
                    case Outfit.BodyType.FEMALE_S:   toggle_FEMALE_S.isOn = true; break;
                    case Outfit.BodyType.CHILD:      toggle_CHILD.isOn = true; break;
                }
            }

            toggle_MALE_L.onValueChanged.AddListener  (value => { if(value) dressingManager.ChangeBodyType(Outfit.BodyType.MALE_L); });
            toggle_MALE_S.onValueChanged.AddListener  (value => { if(value) dressingManager.ChangeBodyType(Outfit.BodyType.MALE_S); });
            toggle_FEMALE_L.onValueChanged.AddListener(value => { if(value) dressingManager.ChangeBodyType(Outfit.BodyType.FEMALE_L); });
            toggle_FEMALE_S.onValueChanged.AddListener(value => { if(value) dressingManager.ChangeBodyType(Outfit.BodyType.FEMALE_S); });
            toggle_CHILD.onValueChanged.AddListener   (value => { if(value) dressingManager.ChangeBodyType(Outfit.BodyType.CHILD); });
        }

    }
} //! namespace ryabomar.UI 