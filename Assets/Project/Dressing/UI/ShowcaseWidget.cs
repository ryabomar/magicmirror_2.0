﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ryabomar.UI {
    public class ShowcaseWidget : MonoBehaviour {
        [SerializeField] OutfitCollection collection;
        [SerializeField] DressingManager dressingManager;
        
        [SerializeField] Outfit.Category categoryToDisplay;

        [SerializeField] Button buttonTemplate;
        [SerializeField] Button buttonUnequip;
        [SerializeField] GameObject buttonsContainer;

        class _Entry {
            public Button button;
            public Outfit outfit;
        }

        List<_Entry> _entries = new List<_Entry>();


        void Start(){
            buttonTemplate.gameObject.SetActive(false); // hide button template

            // make button for each outfit in collection
            foreach(Outfit outfit in collection.QueryOutfits(categoryToDisplay)){

                GameObject buttonInstance = Instantiate(buttonTemplate.gameObject, buttonsContainer.transform);
                buttonInstance.name = "Button__equip_" + outfit.name;
                buttonInstance.SetActive(true);

                Button button = buttonInstance.GetComponent<Button>();
            
                // save ref to image
                _entries.Add(new _Entry{ button = button, outfit = outfit});

                button.onClick.AddListener(()=> {
                    Outfit _outfit = outfit;
                    dressingManager.EquipOnActiveUser(outfit.id);
                });

                Image thumbnailImage = buttonInstance.transform.Find("Image")?.GetComponent<Image>();
                thumbnailImage.material = new Material(thumbnailImage.material);
                thumbnailImage.material.mainTexture = outfit.thumbnail;
            }

            _SwitchTo(dressingManager.lastBodyType);

            dressingManager.userBodyTypeChanged.AddListener(_SwitchTo);

            buttonUnequip.onClick.AddListener(()=>{dressingManager.UnequipFromActiveUser(categoryToDisplay);});

        }


        void _SwitchTo(Outfit.BodyType bodyType){
            foreach(_Entry entry in _entries){
                entry.button.gameObject.SetActive(entry.outfit.bodyType == bodyType);
            }
        }

        void _Closed(){
            Debug.Log("closed");
        }


        void _Opened(){
            Debug.Log("opened");
        }
    }
} //! namespace ryabomar.UI 