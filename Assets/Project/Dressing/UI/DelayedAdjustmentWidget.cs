﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ryabomar.UI {
    public class DelayedAdjustmentWidget : MonoBehaviour {
        [SerializeField] DressingManagerHandle dressingManagerHandle;
        [SerializeField] Slider slider;
        [SerializeField] Text text;
        [SerializeField][Range(0.0f, 10.0f)] float delay = 2.0f;

        float _timeLeft;


        void OnEnable(){
            _timeLeft = delay;
            slider.value = 1.0f;
            text.text = _timeLeft.ToString("0.0") + "s";
        }



        void Update(){
            _timeLeft -= Time.deltaTime;
            slider.value = _timeLeft / delay;
            text.text = _timeLeft.ToString("0.0") + "s";
            if(_timeLeft <= 0.0f){
                dressingManagerHandle.AdjustActiveUserBody();
                gameObject.SetActive(false);
            }
        }

    }
} //! namespace ryabomar.UI {