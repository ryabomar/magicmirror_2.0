﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;


namespace ryabomar.UI {

    [Serializable] public class CategorySwitchingEvent : UnityEvent<Outfit.Category>{}

    public class CategorySwitcherWidget : MonoBehaviour {
        [SerializeField] CategorySwitchingEvent categorySwitchingEvent = new CategorySwitchingEvent();

        public void SwitchTo(Outfit.Category category){
            categorySwitchingEvent.Invoke(category);
        }

        public void SwitchToNONE()              { SwitchTo(Outfit.Category.NONE); }
        public void SwitchToHEADGEAR()          { SwitchTo(Outfit.Category.HEADGEAR); }
        public void SwitchToFEETWEAR()          { SwitchTo(Outfit.Category.FEETWEAR); }
        public void SwitchToGLOVES()            { SwitchTo(Outfit.Category.GLOVES); }
        public void SwitchToUNDERWEAR_BOTTOM()  { SwitchTo(Outfit.Category.UNDERWEAR_BOTTOM); }
        public void SwitchToUNDERWEAR_TOP()     { SwitchTo(Outfit.Category.UNDERWEAR_TOP); }
        public void SwitchToTOPWEAR_BOTTOM()    { SwitchTo(Outfit.Category.TOPWEAR_BOTTOM); }
        public void SwitchToTOPWEAR_TOP()       { SwitchTo(Outfit.Category.TOPWEAR_TOP); }
        public void SwitchToEVERYTHING()        { SwitchTo(Outfit.Category.EVERYTHING); }


    } //! class CategorySwitcherWidget

} //! namespace ryabomar.UI