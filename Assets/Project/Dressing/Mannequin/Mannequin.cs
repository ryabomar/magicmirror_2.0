﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ryabomar.OwnIk;

namespace ryabomar {

    /// <summary>
    /// Visualization of a body with abbility to wear clothing
    /// </summary>
    public class Mannequin : MonoBehaviour, IBodyVisualization {
        /// one of 5 available types
        public Outfit.BodyType type;
        public BodyAvatar bodyAvatar => _bodyAvatar;
        public BodyData lastBodyData => _bodyData;

        /// same id as BodyData
        public ulong bodyId { get; set; }
        public Outfit.Category equippedCategories {
            get {
                Outfit.Category equipped = Outfit.Category.NONE;
                foreach(Outfit outfit in _equippedOutfits){
                    equipped |= outfit.category;
                }
                return equipped;
            }
        }


        //GameObject _bodyInstance;
        BodyAvatar _bodyAvatar;

        // slots for outfits
        List<Outfit> _equippedOutfits = new List<Outfit>();


        BodyData _bodyData = null;

        public GameObject bodyMesh => transform.Find("bodyMesh")?.gameObject;
        public GameObject eyesMesh => transform.Find("high-polyMesh")?.gameObject;


        void Start() {
            // bodyMesh     = transform.Find("bodyMesh")?.gameObject;
            // eyesMesh     = transform.Find("high-polyMesh")?.gameObject;
            // GameObject armatureRoot = _bodyInstance.transform.Find("GameEngine")?.gameObject;

            _bodyAvatar = Utils.MakeBoduAvatarUsingMakeHumanBoneMap(this.gameObject);

            bodyMesh.GetComponent<SkinnedMeshRenderer>().updateWhenOffscreen = true;
            eyesMesh.GetComponent<SkinnedMeshRenderer>().updateWhenOffscreen = true;
        }



        /// <summary>
        /// update body movement
        /// </summary>
        /// <param name="bodyData">data from sensor</param>
        public void UpdateData(BodyData bodyData){
            _bodyData = bodyData;
        }


        /// <summary>
        /// destroy this mannequin
        /// </summary>
        public void Destroy(BodyData bodyData){
            if(bodyId == bodyData.id) {
                Debug.Log("[Mannequin #" + bodyId + "] Destroyed");
                GameObject.Destroy(this.gameObject);
            }
        }

        /// <summary>
        /// unequip outfit from active mannequin of given category
        /// </summary>
        /// <param name="category"></param>
        public void Unequip(Outfit.Category category){
            HashSet<uint> outfitIdsToRemove = new HashSet<uint>();

            foreach(Outfit outfit in _equippedOutfits){
                if((outfit.category & category) != Outfit.Category.NONE){
                    outfitIdsToRemove.Add(outfit.id);
                    outfit.gameObject.SetActive(false);
                    Destroy(outfit.gameObject);
                }
            }

            _equippedOutfits.RemoveAll((outfit) => outfitIdsToRemove.Contains(outfit.id));
        }


        /// <summary>
        /// adjust limbs size to fit user body
        /// </summary>
        public void AdjustBody() {
            if(_bodyData != null && _bodyAvatar != null) {
                Debug.Log("[Mannequin] Adjusting user body #" + _bodyData.id);
                _bodyAvatar.Adjust(_bodyData);
            }
        }


        /// <summary>
        /// equip given outfit
        /// </summary>
        /// <param name="outfit"></param>
        public void Equip(Outfit outfit){
            Unequip(outfit.category);

            GameObject newOutfitObj = Instantiate(outfit.gameObject, transform);
            newOutfitObj.name = outfit.name;
            newOutfitObj.layer = LayerMask.NameToLayer("outfit");
            //_equippedOutfits.Add()
            Outfit newOutfit = newOutfitObj.GetComponent<Outfit>();
            newOutfit.LinkSkeleton(transform.Find("bodyMesh").GetComponent<SkinnedMeshRenderer>());

            _equippedOutfits.Add(newOutfit);
        }


        // public void Unequip(Outfit.Category category){

        // }


        void LateUpdate(){
            if(_bodyData != null){
                _bodyAvatar?.UpdateIK(_bodyData);
            }
        }

    }
} //! namespace ryabomar


/*



*/