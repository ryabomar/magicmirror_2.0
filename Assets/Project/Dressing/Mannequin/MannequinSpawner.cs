﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar {

    /// <summary>
    /// control spawning of Manniquins; One Manniquin for each user
    /// </summary>
    public class MannequinSpawner : MonoBehaviour {
        [SerializeField] SensorSwitcherHandle sensorSwitcherHandle;
        [SerializeField] UserSwitcherHandle userSwitcherHandle;

        public GameObject MaleSBodyMannequin;
        public GameObject MaleLBodyMannequin;
        public GameObject FemaleSBodyMannequin;
        public GameObject FemaleLBodyMannequin;
        public GameObject ChildBodyMannequin;

        
        public Outfit.BodyType lastSpawnedType { get; private set; } = Outfit.BodyType.FEMALE_S;
        bool _isBodyVisible;

        Vector3 _mannequinScale = Vector3.one;

        void Start() {
            sensorSwitcherHandle.newBodyAppeared.AddListener(SpawnMannequin);
        }


        public void ChangeNextBodyType(Outfit.BodyType newtBodyType){
            lastSpawnedType = newtBodyType;
        }

        GameObject _GetBodyPrefab(Outfit.BodyType type){
            switch(type){
                case Outfit.BodyType.MALE_L:   return MaleLBodyMannequin;
                case Outfit.BodyType.MALE_S:   return MaleSBodyMannequin;
                case Outfit.BodyType.FEMALE_S: return FemaleSBodyMannequin;
                case Outfit.BodyType.FEMALE_L: return FemaleLBodyMannequin;
                case Outfit.BodyType.CHILD:    return ChildBodyMannequin;
                default: return null;
            }
        }



        public void SpawnMannequin(BodyData bodyData){
            SpawnMannequin(bodyData, lastSpawnedType);
        }


        public void SpawnMannequin(BodyData bodyData, Outfit.BodyType userBodyType) {
            GameObject go = Instantiate(_GetBodyPrefab(userBodyType), this.transform);
            go.name = "Mannequin#" + bodyData.id;
            Mannequin mannequin = go.GetComponent<Mannequin>();
            mannequin.bodyId = bodyData.id;
            mannequin.type = userBodyType;
            mannequin.transform.localScale = _mannequinScale;

            mannequin.bodyMesh.GetComponent<SkinnedMeshRenderer>().enabled = _isBodyVisible;
            mannequin.eyesMesh.GetComponent<SkinnedMeshRenderer>().enabled = _isBodyVisible;

            // subscribe on update and destroy
            sensorSwitcherHandle.AddBodyListeners(bodyData.id, mannequin.UpdateData, mannequin.Destroy);
            sensorSwitcherHandle.bodyDisappeared.AddListener(mannequin.Destroy);

            lastSpawnedType = userBodyType;
        }



        public Mannequin FindManniquin(ulong id) {
            foreach(Transform child in this.transform) {
                Mannequin mannequin = child.GetComponent<Mannequin>();
                if(mannequin != null && mannequin.bodyId == id) {
                    return mannequin;
                }
            }

            return null; // not found
        }


        public void EquipOnActiveUserMannequin(Outfit outfit){
            GetActiveUserManniquin()?.Equip(outfit);
        }


        public Mannequin GetActiveUserManniquin(){
            return FindManniquin(userSwitcherHandle.activeBodyId);
        }


        public void AdjustActiveUserBody(){
            GetActiveUserManniquin()?.AdjustBody();
        }


        public bool ChangeActiveUserBodyType(Outfit.BodyType userBodyType){
            bool result = false;

            Mannequin mannequin = GetActiveUserManniquin();
            
            if( mannequin != null &&
                mannequin.type != userBodyType &&
                mannequin.lastBodyData != null)
                {

                BodyData lastbodyData = mannequin.lastBodyData; // copy last data

                Destroy(mannequin.gameObject); // destroy old
                SpawnMannequin(lastbodyData, userBodyType); // spawn new one with desired body with bodyData from old mannequin
                
                result =  true;
            }

            lastSpawnedType = userBodyType;
            return result;
        }


        public void SetBodyVisible(bool value){
            _isBodyVisible = value;
            foreach(Transform child in transform){
                Mannequin mannequin = child.GetComponent<Mannequin>();
                if(mannequin != null){
                    mannequin.bodyMesh.GetComponent<SkinnedMeshRenderer>().enabled = value;
                    mannequin.eyesMesh.GetComponent<SkinnedMeshRenderer>().enabled = value;
                }
            }
        }


        public void SetMannequinScale(Vector3 scale){
            _mannequinScale = scale;

            foreach(Transform child in transform){
                Mannequin mannequin = child.GetComponent<Mannequin>();
                if(mannequin != null){
                    mannequin.transform.localScale = _mannequinScale;
                }
            }
        }

    } //! class MannequinSpawner

} //! namespace ryabomar


