﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ryabomar {
    [Serializable] public class CategoryEvent     : UnityEvent<Outfit.Category> {}
    [Serializable] public class UserBodyTypeEvent : UnityEvent<Outfit.BodyType> { } 


} //! namespace ryabomar 