﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ryabomar {


    public class InputController : MonoBehaviour {

        [SerializeField] UserSettingsManager settingsManager;

        bool _IsDebugEnable = false;
        float _inputDelay = 0.4f;
        float _lastInput;

        List<GameObject> _debugObjects = new List<GameObject>();

        void Start(){
            _IsDebugEnable = settingsManager.GetPropertyValue(BoolSettingsProperty.DEBUG__ENABLED);
            _lastInput = Time.realtimeSinceStartup;

            foreach(GameObject obj in GameObject.FindGameObjectsWithTag("DEBUG")){
                _debugObjects.Add(obj);
                obj.SetActive(_IsDebugEnable);
            }
        }


        void Update(){
            if(Input.GetKey(KeyCode.F12)){
                if(Time.realtimeSinceStartup > _lastInput + _inputDelay){
                    _lastInput = Time.realtimeSinceStartup;
                    
                    _IsDebugEnable = !_IsDebugEnable;
                    foreach(GameObject obj in _debugObjects){
                        Debug.Log(obj.name);
                        obj.SetActive(_IsDebugEnable);
                    }

                    settingsManager.SetProperty(BoolSettingsProperty.DEBUG__ENABLED, _IsDebugEnable);
                }
            }
        }
        
    }

} //! namespace ryabomar 