﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar {
    [CreateAssetMenu(fileName = "ApplicationManagerHandle", menuName = "MagicMirror/ApplicationManagerHandle")]
    public class ApplicationManagerHandle : ScriptableObject {

        ApplicationManager _applicationManager => FindObjectOfType<ApplicationManager>();

        public void SwitchToScreenPositionConfigurationScene(){
            _applicationManager?.SwitchToScreenPositionConfigurationScene();
        }


        public void SwitchBackFromScreenPositionConfigurationScen(){
            _applicationManager?.SwitchBackFromScreenPositionConfigurationScen();
        }

    }
} //! namespace ryabomar {
