﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
//using EditorSceneManager = UnityEditor.SceneManagement.EditorSceneManager;

namespace ryabomar {
    // TODO: BIG CLEANUP!! MESS!!


    /// <summary>
    /// controls application initialization
    ///     (only loads scenes for now)
    /// </summary>
    public class ApplicationManager : MonoBehaviour {

        /// <summary> some event related to scenes loading; holds % of application initialized and a name of scene </summary>
        [Serializable] public class LoadingSceneEvent : UnityEvent<float, string>
        {}

        [SerializeField] LoadingSceneEvent loadingStarts_event    = new LoadingSceneEvent();

        /// <summary> been called when scene loading begins</summary>
        [SerializeField] LoadingSceneEvent sceneLoadingBegins_event = new LoadingSceneEvent();

        /// <summary> been called when scene loading ends</summary>
        [SerializeField] LoadingSceneEvent sceneLoadingEnds_event   = new LoadingSceneEvent();

        /// <summary> been called when all scenes had been loaded</summary>
        [SerializeField] LoadingSceneEvent loadingComplete_event    = new LoadingSceneEvent();


        [SerializeField] UserSettingsManager settingsManager;

        [SerializeField] Camera initSceneCamera;


        enum SceneEnum { 
            Init                = 0,
            Core                = 1,
            Debug               = 2,
            Dressing            = 3,
            Environment         = 4,
            SensorPositioning   = 5,
        }



        void Start() {
            
            loadingStarts_event.AddListener((progress, msg) => {initSceneCamera.gameObject.SetActive(true);});
            loadingComplete_event.AddListener((progress, msg) => {initSceneCamera.gameObject.SetActive(false);});

            SceneManager.SetActiveScene(gameObject.scene);
            settingsManager?.Init();
            _LoadEssentialScenes();
        }


        void _LoadEssentialScenes(){
            loadingStarts_event.Invoke(0, "");
            StartCoroutine(_LoadScenesAsync(new List<(int, string)>{
                // --- list of scenes to load ---
                // (id in build, "scene name")
                (1, "Core"),
                (2, "Debug"),
                (3, "Dressing"),
                //(4, "Environment"),
            }, LoadSceneMode.Additive));
        }


        IEnumerator _LoadScenesAsync(List<(int, string)> sceneIdxs, LoadSceneMode mode) {
            
            for(int i = 0; i < sceneIdxs.Count; i++){
                (int sceneIdx, string sceneName) = sceneIdxs[i];

                sceneLoadingBegins_event.Invoke((float)i / sceneIdxs.Count, "loading " + sceneName + "...");

                AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneIdx, mode);

                while(!asyncOperation.isDone){
                    yield return null;
                }

                //yield return new WaitForSeconds(1.0f);
                sceneLoadingEnds_event.Invoke((float)(i + 1.0f) / sceneIdxs.Count, "done");
                //yield return new WaitForSeconds(1.0f);
            }

            loadingComplete_event.Invoke(1.0f, "");
        }



        void OnApplicationQuit(){
            // force PlayerPrefs save on exit
            settingsManager.SaveProperties();
        }


        public void SwitchToScreenPositionConfigurationScene(){
            #pragma warning disable CS0618 // suppress warning
            // unload some scenes
            SceneManager.UnloadScene((int)SceneEnum.Core);
            SceneManager.UnloadScene((int)SceneEnum.Debug);
            SceneManager.UnloadScene((int)SceneEnum.Dressing);
            #pragma warning restore CS0618
            // load
            SceneManager.LoadScene((int)SceneEnum.SensorPositioning, LoadSceneMode.Additive);

            Debug.Log("to");
        }


        public void SwitchBackFromScreenPositionConfigurationScen(){
            SceneManager.UnloadSceneAsync((int)SceneEnum.SensorPositioning);

            _LoadEssentialScenes();
        }
    }



} //!namespace ryabomar 