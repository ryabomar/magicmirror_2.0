﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ryabomar {
    
    /// <summary>
    /// visualize application initialization in UI
    /// </summary>
    public class LoadingScreen : MonoBehaviour {
        
        [SerializeField] Slider progressBar;
        [SerializeField] Text   progressText;
        [SerializeField] Text   loadingLog;


        void Start(){
            ClearLog();
        }

        
        public void OnLoadingStarts(float progress, string msg) {
            gameObject.SetActive(true);
            progressBar.gameObject.SetActive(true);
            progressText.gameObject.SetActive(true);
            loadingLog.gameObject.SetActive(true);
            _SetProgress(progress);
            ClearLog();
        }



        /// <summary> delegate </summary>
        public void OnLoadingBegins(float progress, string msg) {
            _SetProgress(progress);
            _AppendToLog(msg);
        }


        // <summary> delegate </summary>
        public void OnLoadingEnds(float progress, string msg) {
            _SetProgress(progress);
            _AppendToLog(msg, false);
        }


        // <summary> delegate </summary>
        public void OnLoadingComplete(float progress, string msg) {

            StartCoroutine(_WaitAndHideSelf());
        }


        public void ClearLog() {
            loadingLog.text = "";
        }


        void _SetProgress(float value){
            progressBar.value = value;
            progressText.text = (value * 100).ToString("0.") + "%";        
        }

        void _AppendToLog(string txt, bool newLine = true){
            loadingLog.text = loadingLog.text + (newLine ? "\n" : " ") + txt;
        }


        IEnumerator _WaitAndHideSelf(){
            yield return new WaitForSeconds(1.0f);
            gameObject.SetActive(false);
            progressBar.gameObject.SetActive(false);
            progressText.gameObject.SetActive(false);
            loadingLog.gameObject.SetActive(false);
        }

    } //! class LoadingScreen 

} //! namespace ryabomar