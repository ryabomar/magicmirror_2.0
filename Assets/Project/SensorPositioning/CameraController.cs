﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar.SensorPositioning {

/// <summary>
/// For screen positioning scene only
///     camera orbit and zoom
/// </summary>
public class CameraController : MonoBehaviour {
        [SerializeField] GameObject pivot;

        Camera _camera;

        Vector3 _lastMousePos = new Vector3();

        void Start(){
            _camera = GetComponent<Camera>();
        }


        void Update(){
            if (Input.GetMouseButton(1)){
                _RotatePivot();
            } else {
                _lastMousePos = Input.mousePosition;
            }
            _Zoom();
        }


        void _RotatePivot(){
            Vector3 mousePos = Input.mousePosition;
            Vector3 mouseDelta = _lastMousePos - mousePos;

            pivot.transform.localEulerAngles = new Vector3(mouseDelta.y + pivot.transform.localEulerAngles.x, mouseDelta.x + pivot.transform.localEulerAngles.y, 0);

            _lastMousePos = mousePos;
        }



        void _Zoom(){
            float fow = _camera.fieldOfView;
            fow += Input.mouseScrollDelta.y;
            _camera.fieldOfView = Mathf.Clamp(fow, 20, 120);
        }
    }
} //! namespace ryabomar.SensorPositioning
