﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ryabomar.SensorPositioning {

    /// <summary>
    /// screen position and orientation configurator
    /// </summary>
    public class ScreenPositionConfigurator : MonoBehaviour {
        [SerializeField] UserSettingsManager settingsManager;
        [SerializeField] Camera cam;
        
        [SerializeField] GameObject moveScreen;
        [SerializeField] GameObject rotateScreen;

        [SerializeField] Material selected;
        [SerializeField] Material unselected;

        enum ARROW_TAG {NONE, ARROW_MOVE, ARROW_MOVE_VERT, ARROW_ROTATE}
        ARROW_TAG _selectedTag = ARROW_TAG.NONE;

        Vector3 _initPosition;
        Vector3 _initOrient;


        Vector3 _lastMousePos = new Vector3();

        void Start(){
            _initPosition = moveScreen.transform.localPosition;
            _initOrient = rotateScreen.transform.localEulerAngles;

            LoadScreenData();
        }


        void Update(){

            { // mouse hover
                Ray ray = cam.ScreenPointToRay(Input.mousePosition);
                if(Physics.Raycast(ray, out RaycastHit hit)){
                    Transform selected = hit.transform;

                    foreach(ARROW_TAG tag in Enum.GetValues(typeof(ARROW_TAG))){
                        if(tag == ARROW_TAG.NONE) continue;

                        if(selected.CompareTag(tag.ToString())){
                            if(_selectedTag != ARROW_TAG.NONE && _selectedTag != tag){
                                _Deselect(_selectedTag);
                            }

                            _selectedTag = tag;

                            _Select(tag);
                        }
                    }
                } else {
                    if(_selectedTag != ARROW_TAG.NONE){
                        _Deselect(_selectedTag);
                    }
                    //_selectedTag = ARROW_TAG.NONE;
                }
            }
            
            if(Input.GetMouseButton(0)){
                switch(_selectedTag){
                    case ARROW_TAG.ARROW_MOVE:      _MoveScreen();      break;
                    case ARROW_TAG.ARROW_MOVE_VERT: _MoveVertScreen();  break;
                    case ARROW_TAG.ARROW_ROTATE:    _RotateScreen();    break;
                }
            }

            _lastMousePos = Input.mousePosition;
        }



        void _Select(ARROW_TAG tag){
            foreach(GameObject obj in GameObject.FindGameObjectsWithTag(tag.ToString())){
                MeshRenderer meshRenderer = obj.GetComponent<MeshRenderer>();
                if(meshRenderer != null){
                    meshRenderer.material = selected;
                }
            }
        }


        void _Deselect(ARROW_TAG tag){
            foreach(GameObject obj in GameObject.FindGameObjectsWithTag(tag.ToString())){
                MeshRenderer meshRenderer = obj.GetComponent<MeshRenderer>();
                if(meshRenderer != null){
                    meshRenderer.material = unselected;
                }
            }
        }


        void _MoveScreen(){
            Vector3 min = new Vector3(-6, -6, -6);
            Vector3 max = new Vector3(6, 6, 6);

            Vector3 delta = Input.mousePosition - _lastMousePos;
            delta *= Time.deltaTime;

            Vector3 pos = moveScreen.transform.localPosition;
            pos.x = Mathf.Clamp(pos.x + delta.x, min.x, max.x);
            pos.z = Mathf.Clamp(pos.z + delta.y, min.z, max.z);

            moveScreen.transform.localPosition = pos;
        }


        void _MoveVertScreen(){
            Vector3 min = new Vector3(-6, -6, -6);
            Vector3 max = new Vector3(6, 6, 6);

            Vector3 delta = Input.mousePosition - _lastMousePos;
            delta *= Time.deltaTime;

            Vector3 pos = moveScreen.transform.localPosition;
            pos.y = Mathf.Clamp(pos.y + delta.y, min.y, max.y);

            moveScreen.transform.localPosition = pos;
        }


        void _RotateScreen(){
            Vector3 delta = Input.mousePosition - _lastMousePos;
            rotateScreen.transform.localEulerAngles = new Vector3(rotateScreen.transform.localEulerAngles.x, delta.x + rotateScreen.transform.localEulerAngles.y, 0);

        }
        


        public void ResetScreen(){
            moveScreen.transform.localPosition = _initPosition;
            rotateScreen.transform.localEulerAngles = _initOrient;
        }


        public void SaveScreenData(){
            settingsManager.SetProperty(FloatSettingsProperty.PROJECTION_FRAME_POS_X, moveScreen.transform.localPosition.x);
            settingsManager.SetProperty(FloatSettingsProperty.PROJECTION_FRAME_POS_Y, moveScreen.transform.localPosition.y);
            settingsManager.SetProperty(FloatSettingsProperty.PROJECTION_FRAME_POS_Z, moveScreen.transform.localPosition.z);

            settingsManager.SetProperty(FloatSettingsProperty.PROJECTION_FRAME_EULER_X, rotateScreen.transform.localEulerAngles.x);
            settingsManager.SetProperty(FloatSettingsProperty.PROJECTION_FRAME_EULER_Y, rotateScreen.transform.localEulerAngles.y);
            settingsManager.SetProperty(FloatSettingsProperty.PROJECTION_FRAME_EULER_Z, rotateScreen.transform.localEulerAngles.z);

            settingsManager.SaveProperties();
        }


        public void LoadScreenData(){
            float pos_x = settingsManager.GetPropertyValue(FloatSettingsProperty.PROJECTION_FRAME_POS_X);
            float pos_y = settingsManager.GetPropertyValue(FloatSettingsProperty.PROJECTION_FRAME_POS_Y);
            float pos_z = settingsManager.GetPropertyValue(FloatSettingsProperty.PROJECTION_FRAME_POS_Z);

            float euler_x = settingsManager.GetPropertyValue(FloatSettingsProperty.PROJECTION_FRAME_EULER_X);
            float euler_y = settingsManager.GetPropertyValue(FloatSettingsProperty.PROJECTION_FRAME_EULER_Y);
            float euler_z = settingsManager.GetPropertyValue(FloatSettingsProperty.PROJECTION_FRAME_EULER_Z);

            moveScreen.transform.localPosition = new Vector3(pos_x, pos_y, pos_z);
            rotateScreen.transform.localEulerAngles = new Vector3(euler_x, euler_y, euler_z);
        }
    }

} //! namespace ryabomar.SensorPositioning
