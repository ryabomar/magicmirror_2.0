# (mini) code guidlene

##UnityEditor

	- names - mostly CameleCase  
	- UI elements names bigins with type: `Toggle_someProperty`, `Button_reset`
	- objects without visual representation:  `[Object]`  
	- objects responsible for spawning other objecst: `=Object=` 


##Code Style

	- Class brackets  
	```c#
		class MyClass {
			///
		}
	```
	
	- 2 lines between methods  
	```c#
		//...
			void foo() {
				//...
			}
			
			
			int bar(int bazz) {
				//...
			}
		//...
	```
	
	- names of a method starts with verbs and with Capital letter
	```c#
		public void DoMyWork() { ... }
		public int GetSomeNumber() { ... }
	```
	
	- names of variables are noun with lower case
	```c#
		int counter = 5;
	```
	
	- private members names starts with `_`
	```c#
		private void _DoMyWork() { ... } 
		private float _x;
	```
	
	
	- For intercommunication between objects on different scenes, use ScriptableObject classes; Their names ends with "Handle"  
	```c#
		class MyClass : MonoBehaviour {}			// on scene
		class MyClassHandle : ScriptableObject {}	// in project
	```
	
	- after closing bracket, leave a comment saying what scope has ended 
	```c#
		class MyClass {

		} //! class MyClass
	```
		