# MagicMirror_2.0

The updated version of MagicMirror application

## Setup

Copy all '.dll' from [https://drive.google.com/file/d/14tDZ_t0nKuZ6GTuSzoBFhR7LrXB_V8hG/view?usp=sharing] into Assets/Plugins/


Azure Kinect body tracking sdk
https://www.microsoft.com/en-us/download/details.aspx?id=102901

Azure Kinect Sensor SKD
https://docs.microsoft.com/en-us/azure/kinect-dk/body-sdk-download


In unity editor open scene Assets/Project/Init/Init

## Project structure

- Assets/Project/
    - Core/
        > Essential code

    - SensorWrapper/	
        > BodyTracking and Sensor wrapper Interfaces  
        
    - Sensors/	        
        > Implementation of BodyTracking and Sensor interfaces 
        
        - FakeDebug/	
            > Fake sensor and body tracker; provides constant data  
            
        - Kinect2/
            > All code related to Kinect2  
            
        - Azure/
            > All code related to Kinect Azure  
            
    - Init/ 	
        > Application initialization
    
    - Debug/  	
        > Tools for debugging (wireframe body, ui toggles, etc)  
    
    - Dressing/	
        > Outfit system  


## The project is divided into scenes:

	- Init                         Initialization and applications, other scenes are loaded from it
	- Core                         Sensor switcher, OffAxis projection, Point Cloud
	- Debug                        Various debugging tools, Wireframe bodies, UI to enable and debug these features
	- Dressing                     All about clothes, avatars, inverse kinematics, clothing selection UI
	- SensorPositioning            Screen position setter (relatively to the sensor). The data is used for the OffAxis projection frame

All intercommunication between scenes are done thought ScriptableObjects. Such classes have names ended with '*Hadle'.